if [ $# -lt 1 ]
then
  echo "no package name provided, exiting ..."
  exit 1
fi

packagename=$1
disttarzfile=${packagename}.tar.Z
distlistfile=${packagename}.lst
distunpacklogfile=${packagename}.log

if [ "${OSTYPE}" = "cygwin" ]
then
  zcat ${disttarzfile} | tar -T ${distlistfile} -xvf - > ${distunpacklogfile} 2>&1
else
  zcat ${disttarzfile} | tar -xvf -  > ${distunpacklogfile} 2>&1
fi

returncode=$?
if [ $? -ne 0 ]
then
    echo "error in tar, exiting ..."
    exit $returncode
fi

executables=`grep -E -i "(\.dll$|\.exe$|\.dll\.config$|\.exe\.config$)" ${distlistfile}`
for executablefile in $executables
do
	if [ -s ${executablefile} ]
	then
		chmod +x "${executablefile}"
		if [ $? -ne 0 ]
		then
			echo "unable to set executable permission: ${executablefile}, exiting ..."
			exit 1
		fi
	fi
done

grep -q "end of file" ${distunpacklogfile}
if [ $? -eq 0 ]
then
    #report an error if end of file message was found in the log file
	echo "end of file in tar found, exiting ..."
    exit 1
fi

exit 0
