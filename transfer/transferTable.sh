XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_bcp_errors.fn

#parse the command line arguments
# 1 = table
# 2 = sourcedb
# 3 = source server
# 4 = source login
# 5 = source passwd
# 6 = dest db
# 7 = dest server
# 8 = dest user
# 9 = dest passwd

if [ $# -ne 9 ]
then
    echo "Usage: transferTable.sh table srcdb srcserver srclogin srcpasswd destdb destserver destlogin destpasswd"
    exit 1
fi

table=$1
srcdb=$2
srcserver=$3
srcuser=$4
srcpasswd=$5
destdb=$6
destserver=$7
destuser=$8
destpasswd=$9


. $XLS/src/scripts/transfer/transfertable2.sh ${table} ${srcdb} ${srcserver} ${srcuser} ${srcpasswd} ${table} ${destdb} ${destserver} ${destuser} ${destpasswd}

