#parse the command line arguments
# 1 = source server
# 2 = dest server
# 3 = user id
# 4 = temp file

shname=transferusers.sh
if [ $# -lt 3 ]
then
    echo "Usage: ${shname} srcserver destserver userid [accountid] [tempfile]"
    exit 1
fi

srcserver=${1}
destserver=${2}
userid=${3}
if [ $# -gt 4 ]
then
	tempfile=${5}
else
	tempfile="data.out"
fi

usersql="SELECT users.* from users where id=${userid}"
delusersql="delete users where id=${userid}"
psettingsql="select prepbookusersetting.* from prepbookusersetting where userid=${userid}"
delsetting="delete prepbookusersetting where userid=${userid}"
psectionsql="select prepbooksection.* from prepbooksection where userid=${userid}"
delsection="delete prepbooksection where userid=${userid}"
puserssql="select prepbookusers.* from prepbookusers where userid=${userid}"
delpuser="delete prepbookusers where userid=${userid}"
userflagsql="select users_flag.* from users_flag where userid=${userid}"
deluserflag="delete users_flag where userid=${userid}"

rm -f ${tempfile}

echo "${shname}: Transfer of prepbookusersetting table from xls (${srcserver}) to prepbookusersetting table in xls (${destserver})"

#echo "${shname}: Saving data from source users..."

bcp "${usersql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error saving source data to file"
    exit 1
fi

isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${delusersql}"

#echo "${shname}: Loading data into destination..."

bcp xls..users in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error loading destination data from file"
    exit 1
fi

#echo "${shname}: Saving data from source prepbookusersetting..."

bcp "${psettingsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error saving source data to file"
    exit 1
fi

isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${delsetting}"

#echo "${shname}: Loading data into destination..."

bcp xls..prepbookusersetting in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error loading destination data from file"
    exit 1
fi

#echo "${shname}: Saving data from source prepbooksection..."

bcp "${psectionsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error saving source data to file"
    exit 1
fi

isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${delsection}"

#echo "${shname}: Loading data into destination..."

bcp xls..prepbooksection in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error loading destination data from file"
    exit 1
fi

#echo "${shname}: Saving data from source prepbookusers..."

bcp "${puserssql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error saving source data to file"
    exit 1
fi

isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${delpuser}"

#echo "${shname}: Loading data into destination..."

bcp xls..prepbookusers in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error loading destination data from file"
    exit 1
fi

#echo "${shname}: Saving data from source users_flag..."

bcp "${userflagsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error saving source data to file"
    exit 1
fi

isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${deluserflag}"

#echo "${shname}: Loading data into destination..."

bcp xls..users_flag in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error loading destination data from file"
    exit 1
fi

if [ $# -gt 3 ]
then
	accountid=${4}

	accountflagsql="select accounts_flag.* from accounts_flag where accountid=${accountid}"
	delaccountflag="delete accounts_flag where accountid=${accountid}"
	accountsql="select account.* from account where id=${accountid}"
	delaccount="delete account where id=${accountid}"
	pbacctinfosql="select pbaccountinfo.* from pbaccountinfo where account=${accountid}"
	pbcoversql="select pbcoversheettemplate.* from pbcoversheettemplate where account=${accountid}"
	delpbacctinfo="delete pbaccountinfo where account=${accountid}"
	delpbcover="delete pbcoversheettemplate where account=${accountid}"
	pricingsql="select pricing.* from pricing where account=${accountid}"
	delpricing="delete pricing where account=${accountid}"
	pricingstrsql="select pricing_strings.* from pricing_strings where account=${accountid}"
	delpricingstr="delete pricing_strings where account=${accountid}"
	accountpermstrsql="select account_perm.* from account_perm where account=${accountid}"
	delaccountpermstr="delete account_perm where account=${accountid}"

	bcp "${accountsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${delaccount}"

	#echo "${shname}: Loading data into destination..."

	bcp xls..account in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

	bcp "${accountflagsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${delaccountflag}"

	#echo "${shname}: Loading data into destination..."

	bcp xls..accounts_flag in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

	bcp "${pbacctinfosql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${delpbacctinfo}"

	#echo "${shname}: Loading data into destination..."

	bcp xls..pbaccountinfo in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

	bcp "${pbcoversql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${delpbcover}"

	#echo "${shname}: Loading data into destination..."

	bcp xls..pbcoversheettemplate in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

	bcp "${pricingsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${delpricing}"

	#echo "${shname}: Loading data into destination..."

	bcp xls..pricing in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

	bcp "${pricingstrsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${delpricingstr}"

	#echo "${shname}: Loading data into destination..."

	bcp xls..pricing_strings in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

	bcp "${accountpermstrsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U xlsdbo /P xlsdbo /S ${destserver} /Q"${delaccountpermstr}"

	#echo "${shname}: Loading data into destination..."

	bcp xls..account_perm in ${tempfile} /E /b1000 /S${destserver} /Uxlsdbo /Pxlsdbo /c /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi
fi

exit 0
