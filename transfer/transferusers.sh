#parse the command line arguments
# 1 = source server
# 2 = dest server
# 3 = dest server password
# 4 = user id
# 5 = account id

shname=transferusers.sh
if [ $# -lt 4 ]
then
    echo "Usage: ${shname} srcserver destserver destpass userid [accountid]"
    exit 1
fi

srcserver=${1}
destserver=${2}
destpass=${3}
destuser=xlsdbo
userid=${4}

tempfile="$userid.out"
function main() {

psettingsql="select prepbookusersetting.* from prepbookusersetting where userid=${userid}"
delsetting="delete prepbookusersetting where userid=${userid}"
psectionsql="select prepbooksection.* from prepbooksection where userid=${userid}"
delsection="delete prepbooksection where userid=${userid}"
puserssql="select prepbookusers.* from prepbookusers where userid=${userid}"
delpuser="delete prepbookusers where userid=${userid}"
userflagsql="select users_flag.* from users_flag where userid=${userid}"
deluserflag="delete users_flag where userid=${userid}"

rm -f ${tempfile}

echo "${shname}: Transfer of prepbookusersetting table from xls (${srcserver}) to prepbookusersetting table in xls (${destserver})"

echo "${shname}: Saving data from source prepbookusersetting..."

bcp "${psettingsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /CRAW /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error saving source data to file"
    exit 1
fi

isql /U ${destuser} /P ${destpass} /S ${destserver} /b /Q"${delsetting}"
if [ $? -ne 0 ]
then
	echo "error running ${delsetting}, exiting..."
	exit 1
fi

echo "${shname}: Loading data into destination..."

bcp prepbookusersetting in ${tempfile} /E /b1000 /U ${destuser} /P ${destpass} /S ${destserver} /c /CRAW /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error loading destination data from file"
    exit 1
fi

echo "${shname}: Saving data from source prepbooksection..."

bcp "${psectionsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /CRAW /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error saving source data to file"
    exit 1
fi

isql /U ${destuser} /P ${destpass} /S ${destserver} /b /Q"${delsection}"
if [ $? -ne 0 ]
then
	echo "error running ${delsection}, exiting..."
	exit 1
fi

echo "${shname}: Loading data into destination..."

bcp prepbooksection in ${tempfile} /E /b1000 /U ${destuser} /P ${destpass} /S ${destserver} /c /CRAW /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error loading destination data from file"
    exit 1
fi

echo "${shname}: Saving data from source prepbookusers..."

bcp "${puserssql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /CRAW /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error saving source data to file"
    exit 1
fi

isql /U ${destuser} /P ${destpass} /S ${destserver} /b /Q"${delpuser}"
if [ $? -ne 0 ]
then
	echo "error running ${delpuser}, exiting..."
	exit 1
fi

echo "${shname}: Loading data into destination..."

bcp prepbookusers in ${tempfile} /E /b1000 /U ${destuser} /P ${destpass} /S ${destserver}  /c /CRAW /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error loading destination data from file"
    exit 1
fi

echo "${shname}: Saving data from source users_flag..."

bcp "${userflagsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /CRAW /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error saving source data to file"
    exit 1
fi

isql /U ${destuser} /P ${destpass} /S ${destserver} /b /Q"${deluserflag}"
if [ $? -ne 0 ]
then
	echo "error running ${deluserflag}, exiting..."
	exit 1
fi

echo "${shname}: Loading data into destination..."

bcp users_flag in ${tempfile} /E /b1000 /U ${destuser} /P ${destpass} /S ${destserver} /c /CRAW /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error loading destination data from file"
    exit 1
fi
}

function main_account() {

	accountflagsql="select accounts_flag.* from accounts_flag where accountid=${accountid}"
	delaccountflag="delete accounts_flag where accountid=${accountid}"
	pbcoversheetsql="select * from pbcoversheettemplate where account=${accountid}"
	delpbcoversheet="delete pbcoversheettemplate where account=${accountid}"
	pbacctinfosql="select pbaccountinfo.* from pbaccountinfo where account=${accountid}"
	delpbacctinfo="delete pbaccountinfo where account=${accountid}"
	pricingsql="select pricing.* from pricing where account=${accountid}"
	delpricing="delete pricing where account=${accountid}"
	pricingstrsql="select pricing_strings.* from pricing_strings where account=${accountid}"
	delpricingstr="delete pricing_strings where account=${accountid}"
	accountpermstrsql="select account_perm.* from account_perm where account=${accountid}"
	delaccountpermstr="delete account_perm where account=${accountid}"

#	echo "${shname}: Saving data from source account ..."
#	bcp "${accountsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /CRAW /t"\001" /r"\002"
#	if [ $? != 0 ]
#	then
#	    echo "${shname}: Error saving source data to file"
#	    exit 1
#	fi

#	isql /U ${destuser} /P ${destpass} /S ${destserver} /b /Q"${delaccount}"
#	if [ $? -ne 0 ]
#	then
#		echo "error running ${delaccount}, exiting..."
#		exit 1
#	fi

#	echo "${shname}: Loading data into destination..."

#	bcp account in ${tempfile} /E /b1000 /U ${destuser} /P ${destpass} /S ${destserver} /c /CRAW /t"\001" /r"\002"
#	if [ $? != 0 ]
#	then
#	    echo "${shname}: Error loading destination data from file"
#	    exit 1
#	fi

	echo "${shname}: Saving data from source account flags ..."
	bcp "${accountflagsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U xls /P xls /U ${destuser} /P ${destpass} /S ${destserver} /b /Q"${delaccountflag}"
	if [ $? -ne 0 ]
	then
		echo "error running ${delaccountflag}, exiting..."
		exit 1
	fi

	echo "${shname}: Loading data into destination..."

	bcp accounts_flag in ${tempfile} /E /b1000 /U ${destuser} /P ${destpass} /S ${destserver} /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

	echo "${shname}: Saving data from source pbaccountinfo ..."
	bcp "${pbacctinfosql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U ${destuser} /P ${destpass} /S ${destserver} /b /Q"${delpbacctinfo}"
	if [ $? -ne 0 ]
	then
		echo "error running ${delpbacctinfo}, exiting..."
		exit 1
	fi

	echo "${shname}: Loading data into destination..."

	bcp pbaccountinfo in ${tempfile} /E /b1000 /U ${destuser} /P ${destpass} /S ${destserver} /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

	echo "${shname}: Saving data from pricing ..."
	bcp "${pricingsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U ${destuser} /P ${destpass} /S ${destserver} /b /Q"${delpricing}"
	if [ $? -ne 0 ]
	then
		echo "error running ${delpricing}, exiting..."
		exit 1
	fi

	echo "${shname}: Loading data into destination..."

	bcp pricing in ${tempfile} /E /b1000 /U ${destuser} /P ${destpass} /S ${destserver}  /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

	echo "${shname}: Saving data from pricing_strings ..."
	bcp "${pricingstrsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U ${destuser} /P ${destpass} /S ${destserver} /b /Q"${delpricingstr}"
	if [ $? -ne 0 ]
	then
		echo "error running ${delpricingstr}, exiting..."
		exit 1
	fi

	echo "${shname}: Loading data into destination..."

	bcp pricing_strings in ${tempfile} /E /b1000 /U ${destuser} /P ${destpass} /S ${destserver}  /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

	echo "${shname}: Saving data from account_perm ..."
	bcp "${accountpermstrsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U ${destuser} /P ${destpass} /S ${destserver} /b /Q"${delaccountpermstr}"
	if [ $? -ne 0 ]
	then
		echo "error running ${delaccountpermstr}, exiting..."
		exit 1
	fi

	echo "${shname}: Loading data into destination..."

	bcp account_perm in ${tempfile} /E /b1000 /U ${destuser} /P ${destpass} /S ${destserver} /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

	echo "${shname}: Saving data from source pbcoversheettemplate ..."
	bcp "${pbcoversheetsql}" queryout ${tempfile} /S${srcserver} /Uxls /Pxls /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error saving source data to file"
	    exit 1
	fi

	isql /U ${destuser} /P ${destpass} /S ${destserver} /b /Q"${delpbcoversheet}"
	if [ $? -ne 0 ]
	then
		echo "error running ${delpbacctinfo}, exiting..."
		exit 1
	fi

	echo "${shname}: Loading data into destination..."

	bcp pbcoversheettemplate in ${tempfile} /b1000 /U ${destuser} /P ${destpass} /S ${destserver} /c /CRAW /t"\001" /r"\002"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error loading destination data from file"
	    exit 1
	fi

}

logfile=${userid}.log
main #> $logfile 2>&1
if [ $# -gt 4 ]
then
	accountid=${5}
	main_account #> $logfile 2>&1
fi

exit 0
