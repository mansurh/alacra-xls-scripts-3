shname=fromData7toData9.sh
if [ $# -lt 2 ]
then
    echo "Usage: ${shname} table tempfile"
    exit 1
fi

table=${1}
tempfile=${2}

./transferdataonly.sh ${table} xls data7 xls xlsold ${table} xls data9 xls xls ${tempfile}
