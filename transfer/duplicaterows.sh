# duplicaterows.sh - copies rows in a table from one ID to another
#parse the command line arguments
# 1 = server
# 2 = user
# 3 = passwd
# 4 = table name
# 5 = key column name
# 6 = old ID
# 7 = new ID
shname=duplicaterows.sh
if [ $# -lt 7 ]
then
    echo "Usage: ${shname} server login passwd tableName keyColName oldID newID"
    exit 1
fi

server=${1}
user=${2}
passwd=${3}
table=${4}
key=${5}
oldid=${6}
newid=${7}


# make temp file names
spfile=spcolquery_${table}.out
if [ -e ${spfile} ]
then
	rm -f ${spfile}
fi
flagfile=flag_${table}.out
if [ -e ${flagfile} ]
then
	rm -f ${flagfile}
fi


# get column list from table
spcolquery="sp_columns ${table}"
isql /Q "${spcolquery}" /S ${server} /U ${user} /P ${passwd} -b -n -h-1 -w4096 > ${spfile}
if [ $? != 0 ]
then
    echo "${shname}: Error running query [${spcolquery}] on ${server}"
    exit 1
fi

# use awk to essentially select 4th token in each line, substituting the new key value for the name of the key column
# this makes the select list that we can use for INSERT
selectlist=$(awk -v K=${key} -v NEWKEY=${newid} -v FLAGFILE=${flagfile} -f $XLS/src/scripts/transfer/duplicaterows.awk ${spfile})

#echo "selectlist:"
#echo "${selectlist}"

# examine the flag file left by awk to see if any columns matched the key column name -- generate error if not
flagcount=0
if [ -e ${flagfile} ]
then
	flagcount=$(cat ${flagfile})
fi

if [ ${flagcount} -lt 1 ]
then
    echo "${shname}: Error, no columns matched the key column name ${key}"
	exit 1
fi

rm ${flagfile}

# delete any existing rows matching the target ID
deletequery="DELETE FROM ${table} WHERE ${key} = ${newid}"

echo "${deletequery}"
isql /Q "${deletequery}" /S ${server} /U ${user} /P ${asswd} -b -n -h-1 -w4096
if [ $? != 0 ]
then
    echo "${shname}: Error running query [${deletequery}] on ${server}"
    exit 1
fi

# clone the rows from the source into the target
fullquery="INSERT ${table} SELECT ${selectlist} FROM ${table} WHERE ${key} = ${oldid}"
#echo "fullquery:"
#echo "${fullquery}"

echo "${fullquery}"
isql /Q "${fullquery}" /S ${server} /U ${user} /P ${passwd} -b -n -h-1 -w4096
if [ $? != 0 ]
then
    echo "${shname}: Error running query [${fullquery}] on ${server}"
    exit 1
fi

rm ${spfile}

exit 0
