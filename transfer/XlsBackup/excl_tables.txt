# This is a list of tables to exclude from the test db copy
# Notes: Tables that are NOT commented out are EXCLUDED from the copy
# i.e. Put a # in front of all tables you want copied
#
logins
logins_internal
pbusage
prepbook
prepbooktoc
shoppingcart
shoppingcartlog
usage
usageSpecial
usage_guest
SecurityEvent
editBufferSession
editIndustry
editTableMap
alaxls_log
alaxls_log_copy
trace_sql
stat_monthly_log
alacralog_stat_monthly
login_stat_monthly
pbclickthroughlog_stat_monthly
pbusage_stat_monthly
usage_stat_monthly
lwid_perm_load
usage_ncflog
usage_ncflog_tmp
session_archive
users_audit
account_audit
# replicate tables
cc_event
cc_order
cc_order_email
cc_orderitem
afPageClick
registration_log
creditcardlog
whitepaper_subscribers
pbfileuploadlog
