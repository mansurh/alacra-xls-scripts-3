# XlsBackupLow.sh
# Parse the command line arguments.
#	1 = source server
#	2 = source user
#	3 = source psw
#	4 = dest server
#	5 = dest user
#	6 = dest psw
#	7 = noget (optional)
#	8 = updatetype (optional - daily|weekly)

shname=XlsBackupLow.sh
if [ $# -lt 6 ]
then
	echo "Usage: ${shname} sourceserver sourceuser sourcepsw destserver destuser destpsw"
	exit 1
fi

# Save the runstring parameters in local variables
sourceserver=$1
sourceuser=$2
sourcepsw=$3
destserver=$4
destuser=$5
destpsw=$6

# optional 'noget' as 7th param skips source safe step
if [ $# -gt 6 ]
then
	noget=$7
else
	noget=N
fi

updatetype="daily"
# optional updatetype as 3rd param for daily vs weekly update
if [ $# -gt 7 ]
then
	updatetype=$8
fi

backupserver=""
if [ $# -gt 8 ]
then
	backupserver=$8
fi

SCRIPTDIR=$XLS/src/scripts/transfer/XlsBackup

# get the latest exclusions
# create paths with slashes both ways
ourpath="$XLSDATA/XlsBackup"
cd $ourpath
rm *.bak
backslashpath=$(sed -e"s/\//\\\\/g" << HERE
${ourpath}
HERE
)

#echo "ourpath=${ourpath}"
#echo "backslashpath=${backslashpath}"

# source safe options
ssproject="\$/xls/src/scripts/transfer/XlsBackup"
# set the 'ignore prompts' flag (useful to remove for debugging)
iflag=-I-N
#iflag=
sslogin=batchuser
sspsw=batchuser

# Get the latest procedure exclusions
procexclfile=${ourpath}/excl_procedures.txt
if [ "${noget}" != "noget" ]
then
	echo "${shname}: Getting latest ${procexclfile} at `date`"
	rm -f ${procexclfile}
	ss Get ${ssproject}/excl_procedures.txt -GL${backslashpath} -GWR -GTM -GCC -R ${iflag} -Y${sslogon},${sspsw}
	if [ $? -ne 0 ]
	then
		echo "${shname}: Source Safe Get failed (1) at `date`"
		exit 1
	fi
	if [ ! -e ${procexclfile} ]
	then
		echo "${shname}: ${procexclfile} does not exist after Source Safe Get at `date`"
		exit 1
	fi
	chmod 777 ${procexclfile}
	echo ""
else
	cp ${SCRIPTDIR}/excl_procedures.txt ${procexclfile}
fi

# Get the latest table exclusions
tableexclfile=${ourpath}/excl_tables.txt
if [ "${noget}" != "noget" ]
then
	echo "${shname}: Getting latest ${tableexclfile} at `date`"
	rm -f ${tableexclfile}
	ss Get ${ssproject}/excl_tables.txt -GL${backslashpath} -GWR -GTM -GCC -R ${iflag} -Y${sslogon},${sspsw}
	if [ $? -ne 0 ]
	then
		echo "${shname}: Source Safe Get failed (2) at `date`"
		exit 1
	fi
	if [ ! -e ${tableexclfile} ]
	then
		echo "${shname}: ${tableexclfile} does not exist after Source Safe Get at `date`"
		exit 1
	fi
	chmod 777 ${tableexclfile}
	rm -f ${ourpath}/vssver.scc
	echo ""
else
	cp ${SCRIPTDIR}/excl_tables.txt ${tableexclfile}
fi

# Get the latest table exclusions for daily
if [ ${updatetype} == "daily" ]
then
  tableexcldailyfile=${ourpath}/excl_daily_tables.txt
  if [ "${noget}" != "noget" ]
  then
	echo "${shname}: Getting latest ${tableexcldailyfile} at `date`"
	rm -f ${tableexcldailyfile}
	ss Get ${ssproject}/excl_daily_tables.txt -GL${backslashpath} -GWR -GTM -GCC -R ${iflag} -Y${sslogon},${sspsw}
	if [ $? -ne 0 ]
	then
		echo "${shname}: Source Safe Get failed (2) at `date`"
		exit 1
	fi
	if [ ! -e ${tableexcldailyfile} ]
	then
		echo "${shname}: ${tableexcldailyfile} does not exist after Source Safe Get at `date`"
		exit 1
	fi
	chmod 777 ${tableexcldailyfile}
	rm -f ${ourpath}/vssver.scc
	echo ""
  else
	cp ${SCRIPTDIR}/excl_daily_tables.txt ${tableexcldailyfile}
  fi

  cat ${tableexcldailyfile} >> ${tableexclfile}
fi

incrdumpfilelist=${ourpath}/dump_incr_tables.txt
if [ "${noget}" != "noget" ]
then
	echo "${shname}: Getting latest ${incrdumpfilelist} at `date`"
	rm -f ${incrdumpfilelist}
	ss Get ${ssproject}/dump_incr_tables.txt -GL${backslashpath} -GWR -GTM -GCC -R ${iflag} -Y${sslogon},${sspsw}
	if [ $? -ne 0 ]
	then
		echo "${shname}: Source Safe Get failed (2) at `date`"
		exit 1
	fi
	if [ ! -e ${incrdumpfilelist} ]
	then
		echo "${shname}: ${incrdumpfilelist} does not exist after Source Safe Get at `date`"
		exit 1
	fi
	chmod 777 ${incrdumpfilelist}
	rm -f ${ourpath}/vssver.scc
	echo ""
else
	cp ${SCRIPTDIR}/dump_incr_tables.txt ${incrdumpfilelist}
fi

# Perform the selective copy, using the exclusion files
$XLS/src/scripts/transfer/copydatabase.sh xls ${sourceserver} ${sourceuser} ${sourcepsw} xls ${destserver} ${destuser} ${destpsw} ${ourpath} ${tableexclfile} ${procexclfile} ${incrdumpfilelist}
returncode=$?

if [ $returncode != 0 ]
then
    exit $returncode
fi

echo "Done with copydatabase.sh " `date`

cd $ourpath

if [ ! -z ${backupserver} ]
then
	echo "Backing up xls db"
	rm xls.bak
	isql /S${destserver} /U${destuser} /P${destpsw} < ${SCRIPTDIR}/backup_xls_db.sql
	todaysfile=xls`date +%u`.bak
	mv xls.bak ${todaysfile}
	echo "user filepickup filepickup" > command.ftp
	echo "binary " >> command.ftp
	echo "cd /xlsdata/xlsbackup" >> command.ftp
	echo "put ${todaysfile} "  >> command.ftp
	echo "quit"  >> command.ftp
	ftp -i -n -s:command.ftp ${backupserver}
	if [ $? -ne 0 ]
	then
		echo "error in ftp, exiting ..."
		rm ${todaysfile}
		exit 1
	fi
	rm ${todaysfile}
else
	tarfile=xls.bak.`date +%d`.tgz
	rm -f ${tarfile}
	tar cvfz ${tarfile} xls
	if [ $? -ne 0 ]; then echo "Error in tarring xls data into ${tarfile}, exiting ..."; exit 1; fi;
fi

exit 0
