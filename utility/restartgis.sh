function stop_gis {
  net stop GenericIndexServer2
}
function start_gis {
  net start GenericIndexServer2
}

today=`date +%Y%m%d`
logfile="gisrestart${today}.log"
date >> $logfile
stop_gis >> $logfile 2>&1
sleep 3
start_gis >> $logfile 2>&1
date >> $logfile