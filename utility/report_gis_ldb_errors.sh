scriptdir=$XLS/src/scripts/utility
logfile=GISErrorLog`date +%Y%m%d`.log

if [ "x${OSTYPE}" = "xcygwin" ]
then
	cd $WINDIR/system32
else
	cd $windir/system32
fi

rm -f temp.out
grep "Cannot find landing page xml for ID" ${logfile} | awk -f ${scriptdir}/process_gis_log.awk > gis_process.tmp

if [ -s gis_process.tmp ]
then
	sort temp.out  >> gis_process.tmp

	blat gis_process.tmp -s "GIS Landing Page DB erors on ${COMPUTERNAME}" -t "dbinfo@alacra.com, ron.waksman@alacra.com"
fi

exit 0