function ShortenUrl {
	param
	(
		[string]$url
	)

	$bitlyurl = ''
	$errText = ''
	$urlEncodedUrl = [System.Web.HttpUtility]::UrlEncode($url)
	Try {
		$bitlyreq = [System.Net.WebRequest]::Create([string]::Format("http://api.bit.ly/v3/shorten?login=stpulse&apiKey=R_296ffa40aed8251b2d17c1c862091011&longUrl={0}&format=json", $urlEncodedUrl))
		$bitlyreq.Method = "GET"
		$bitlyresp = $bitlyreq.GetResponse()
		UsingObj ($bitlysr = New-Object System.IO.StreamReader($bitlyresp.GetResponseStream())) {
			$bitlyresptxt = $bitlysr.ReadToEnd()
			$urlpos = $bitlyresptxt.IndexOf('"url":')
			if ($urlpos -ne -1) {
				$urlvalpos = $bitlyresptxt.IndexOf('"', $urlpos + 5)
				$bitlyurl = $bitlyresptxt.Substring($urlvalpos + 1, $bitlyresptxt.IndexOf('"', $urlvalpos + 1) - ($urlvalpos + 1))
				$bitlyurl = $bitlyurl.Replace('\/', '/')
			} else {
				$errText = $bitlyresptxt
			}
		}
	} Catch {
		[System.Diagnostics.EventLog]::WriteEntry("SubscriptionDeliveryService", [string]::Format("Bit.ly Error: {0}", $ex.ToString()), [System.Diagnostics.EventLogEntryType]::Error) | Out-Null
	}

	if ([string]::IsNullOrEmpty($bitlyurl)) {
		$tinyurlserver = ""
		$backupserver = ""
		$tinyurlport = ""
		[Alacra.Util.General]::GetXLSAppServerInfo("tinyurlretriever", [ref] $tinyurlserver, [ref] $backupserver, [ref] $tinyurlport) | Out-Null
		$tinyurl = ""
		if (-not [string]::IsNullOrEmpty($tinyurlserver)) {
			$tinyurlreq = [System.Net.WebRequest]::Create([string]::Format("http://{0}:{1}/?fullurl={2}", $tinyurlserver, $tinyurlport, $urlEncodedUrl))
			$tinyurlreq.Method = "GET"
			$tinyurlresp = $tinyurlreq.GetResponse()
			$tinyurlsr = New-Object System.IO.StreamReader($tinyurlresp.GetResponseStream())
			$tinyurl = $tinyurlsr.ReadToEnd()
		}
		if (-not [string]::IsNullOrEmpty($tinyurl)) {
			$bitlyurl = [string]::Format("http://ws.alacra.com/{0}", $tinyurl)
		} else {
			[System.Diagnostics.EventLog]::WriteEntry("SubscriptionDeliveryService", [string]::Format("TinyUrlRetriever returned zero-length ({0}) (bit.ly:{1})", $tinyurlserver, $errText), [System.Diagnostics.EventLogEntryType]::Error) | Out-Null
		}
	}
	#Trace.TraceInformation("shortened url: {0} -> {1}", urlEncodedUrl, bitlyurl)
	return [string]$bitlyurl
}
