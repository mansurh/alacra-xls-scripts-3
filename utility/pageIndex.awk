BEGIN {
	IFS=" ";
	totc=0;
	mynr = 1;
}
{
	if ($1 == "Page") {
#		print "Page found";
		pns[$2] = mynr;
		totc++;
	}

	i=index($0, ":");
	if (i > 0)
	{
		print substr($0, i+2);
		mynr++;
	}
}
END {
	print "PAGEINDEX", totc >> "index." FILENAME
	for (i=1; i <= totc; i++)
	{
		print i ":" pns[i] >> "index." FILENAME
	}
}
