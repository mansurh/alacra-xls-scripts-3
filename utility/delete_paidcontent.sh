. $XLS/src/scripts/loading/dba/get_registry_value.fn

dbname=xlsdbo
server=`get_xls_registry_value $dbname server`
userid=`get_xls_registry_value $dbname user`
psw=`get_xls_registry_value $dbname password`

isql /S${server} /U${userid} /P${psw} << HERE
delete shoppingcart_paidcontent where expires < getdate()
go
update usage
set price = list_price-discount_amount
from usage u, usagediscount d 
where u.userid=d.userid and u.shoppingcartid = d.shoppingcartid and u.price = u.list_price
and u.access_time > dateadd(day, -7, getdate())
go
HERE