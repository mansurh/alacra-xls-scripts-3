if [ $# != 1 ]
then
        echo "Usage: dbserver.sh dbname"
        exit 1
fi
dbname=$1
export dbname

var=`registry -r -p -k "HKEY_LOCAL_MACHINE\\Software\\xls\\Data Servers\\\\${dbname}" -n Server`
if [ $? != 0 ]
then
	var=`registry -r -p -k "HKEY_LOCAL_MACHINE\\Software\\xls\\Data Servers\\\\${dbname}" -n server`

	if [ $? != 0 ]
	then
		exit 1
	fi
fi

echo "${var}\c"
