
use strict;
use Win32::TieRegistry;
use Win32::Internet;

sub getRegistryVal {
	my $keyname = shift;
	my $valname = shift;
	$Registry->Delimiter("/");
	my $HKLMKey = $Registry->{"LMachine/"};
	my $RequestedKey = $HKLMKey->{$keyname};
	$RequestedKey->{"/$valname"};
}

sub enumerateRegistryKeys {
	my $keyname = shift;
	$Registry->Delimiter("/");
	my $HKLMKey = $Registry->{"LMachine/"};
	my $RequestedKey = $HKLMKey->{$keyname};
	grep( m#^[^/]#, keys( %{$RequestedKey} ) );
}

my $Connection = new Win32::Internet();

my $fullservername = getRegistryVal( "SOFTWARE/XLS/Temp Files/", "ThisServer" );
my @servernamepieces = split /\./, $fullservername;
my $servername = $servernamepieces[0];
#print $servername."\n";

my $Session;
$Connection->FTP($Session, "data11", "administrator", "newpass");
eval { $Session->Mkdir("/f/cfg/files/$servername"); };
$Session->Cd("/f/cfgfiles/$servername");
$Session->Binary();
#print $Session->GetResponse();
my @keys = enumerateRegistryKeys("SOFTWARE/XLS/Inverted Files/");
foreach my $e (@keys) {
	my $cfgfilepath = getRegistryVal( "SOFTWARE/XLS/Inverted Files/".$e, "Path" );
	$cfgfilepath =~ s#\\#/#g;
	if( -e $cfgfilepath ) {
		my @fileparts = split /\//, $cfgfilepath;
		my $filename = @fileparts[$#fileparts];
		$Session->Put("$cfgfilepath", $filename);
		print $Session->GetResponse();
	}
}
$Session->Close();

1;

