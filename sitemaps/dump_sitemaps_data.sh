
XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

main() {
 database=sitemapdb
 server=`get_xls_registry_value ${database} Server`
 logon=`get_xls_registry_value ${database} User`
 password=`get_xls_registry_value ${database} Password`

 if [ "$server" = "" ]; then echo "No Server for ${database} available, exiting ..."; return 0; fi;

 bcp "select regkey, part, archived, convert(varchar,max(lastmod),100) from sitemaps where server in (select name from resourceserver where type=6 and serveruse=1) and regkey ='dnb2store'  group by regkey, part, archived " queryout tmp.dat /c /U${logon} /P${password} /S${server}
 if [ $? -ne 0 ]
 then
   echo "error in bcp for dnb data, exiting ..."
   return 1
 fi
 mv tmp.dat sitemapindex_dnb2store.dat
 chmod a+r sitemapindex_dnb2store.dat

 bcp "select regkey, part, archived, convert(varchar,max(lastmod),100) from sitemaps where server in (select name from resourceserver where type=6 and serveruse=1) and regkey in (select regkey from active_apps) and regkey!='dnb2store' group by regkey, part, archived " queryout tmp.dat /c /U${logon} /P${password} /S${server}
 if [ $? -ne 0 ]
 then
   echo "error in bcp for all data, exiting ..."
   return 1
 fi
 mv tmp.dat sitemapindex.dat
 chmod a+r sitemapindex.dat

}

#set -x

pvtdir=`get_xls_registry_value_by_name "Pvt Files" "" "PathFile"`
mkdir -p $pvtdir
cd $pvtdir
if [ $? -ne 0 ]
then
   echo "$pvtdir not found, exiting ..."
   exit 1
fi

logfile=sitemaps`date +%m%d`.log
main >  ${logfile} 2>&1
if [ $? -ne 0 ]
then
  blat ${logfile} -s "Sitemap data rebuild failed on $COMPUTERNAME" -t administrators@alacra.com
  exit 1
fi