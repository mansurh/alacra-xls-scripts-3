use Win32::OLE;
use HTTP::Date;
use OLE;

unless (@ARGV >= 5) {
	die("usage: createDnb2Sitemaps SERVER DATABASE USER PASSWORD SRVNAME defaultdate{YYYY-MM-DD format} MAX ");
}
$SERVER=$ARGV[0];
$DATABASE=$ARGV[1];;
$USER=$ARGV[2];
$PASSWORD=$ARGV[3];
my $SRVNAME=$ARGV[4];
my $DEF_DATE=$ARGV[5];
if (length($DEF_DATE) > 0 && (length($DEF_DATE) != 10 || index($DEF_DATE,"-")== -1)) {
	die "default date format incorrect - must be YYYY-MM-DD, got $DEF_DATE \n"; } 

my $MAX_RECORDS=$ARGV[6];

my $freq;
my $counter=1;
$fieldNum=2;

if ($MAX_RECORDS ne "") {
	$TOP_STRING="top ".$MAX_RECORDS; }
$QUERY = sprintf("select %s w.dunsNo, reportDate from worldbase w, invIndexSortFull i where i.dunsNo=w.dunsNo and i.tier in (1,2,3,4,5) order by i.sort asc", $TOP_STRING); 
#$QUERY = sprintf("select %s w.dunsNo, reportDate from worldbase w, invIndexSortFull i where i.dunsNo=w.dunsNo and i.dunsNo<100000000 order by i.sort asc", $TOP_STRING); 
#$QUERY = sprintf("select %s w.dunsNo, reportDate from worldbase w, invIndexSortFull i where i.dunsNo=w.dunsNo and i.dunsNo=634445209 order by i.sort asc", $TOP_STRING); 
print ("$QUERY \n");

$DELQUERY = sprintf("delete sitemaps where dbase='dnb2' and regkey='dnb2store' and server = '%s'\n", $SRVNAME);

#exit 0;

my $SITEMAPFILE;
unless (open(SITEMAPFILE, ">sitemap.sql")){
	die "cannot open sitemap.sql file";
}

my $OUTFILE;
&openFile();

#Create connection strings
my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!";
#30 minutes should be enough
$conn->{'CommandTimeout'} = 1800;
my $connect="DRIVER={SQL SERVER};PWD=$PASSWORD;UID=$USER;SERVER=$SERVER;DATABASE=$DATABASE";

#Connect
$conn->open($connect);

#Execute query
my $sql=$QUERY;

my $rs;

unless($rs=$conn->Execute($sql)){
  die("error executing the SQL statement");
}

$MAX_BATCH=45000;
$total_count=0;
$batch_count=0;
#Get values
$currentDateTime = HTTP::Date::time2iso();
$myfldcnt = $rs->Fields->Count;
if ($myfldcnt eq $fieldNum ){
	print SITEMAPFILE ("$DELQUERY\n");
	while ( !$rs->EOF){
		$total_count++;
		$batch_count++;
		if ($batch_count > $MAX_BATCH) {
			&closeFile(1, $batch_count-1);
			$batch_count=1;
			$counter++;
			&openFile();
		}
		$id = $rs->Fields(0)->value;
		$date = $rs->Fields(1)->value;
#testing value		
#$date="19990307";
		$parsedt = HTTP::Date::parse_date($date);
# 		if bad date		
#print("$date : $parsedt  : $currentDateTime\n");
		if (!$parsedt  || substr($date,0,4) lt "2000" || $parsedt>$currentDateTime ) 
			{ $date=$DEF_DATE; }
		else {
			$date=sprintf("%s-%s-%s", substr($date,0,4),substr($date,4,2),substr($date,6));		}
#		print("$date\n");
		# use translate spaces to underscores
#		$name =~ tr/ /_/;
		&writeSet($id, $date);
		$rs->MoveNext;
	} 
	print("total records: $total_count\n");
} else {
	print ("number of fields is not equal to $fieldNum\n"); 
	exit 1;
}

&closeFile(0, $batch_count-1);
$rs->Close;
$conn->Close;

sub closeFile{
	local ($gzip, $batch) = @_;
	print OUTFILE ("</urlset>\n");
	close (OUTFILE);
	$file=sprintf("dnb2_dnb2store_%03d.smx", $counter);
	if ($gzip eq 1 ) {
		system("gzip -f $file");
	}
	print SITEMAPFILE ("insert into sitemaps(dbase, regkey, server, part, archived, indextime, lastmod, doc_count, sitemap_size, metadata_size)\n");
	print SITEMAPFILE ("values('dnb2', 'dnb2store', '$SRVNAME', $counter, $gzip, getdate(), getdate(), $batch, 0, 0)\n ");
}

sub openFile {
	$file=sprintf("dnb2_dnb2store_%03d.smx", $counter);
	unless (open(OUTFILE, ">$file")){
		die "cannot open $file file";
	}
print ("$file\n");
	print OUTFILE ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	print OUTFILE ("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
}

sub writeSet {
	local ($id, $date) = @_ ;
	print OUTFILE ("<url>");
	$paddedId="000000".$id;
	$zeroId=substr($paddedId,-9);
	print OUTFILE ("<loc>http://dnb.alacrastore.com/storecontent/dnb2/$zeroId</loc>");
	if ($date ne "") {
		print OUTFILE ("<lastmod>$date</lastmod>"); }
	print OUTFILE ("<changefreq>yearly</changefreq>");
	print OUTFILE ("</url>\n");
}
