XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

if [ $# -lt 2 ]
then
	echo "usage: $0 xlsserver database [type (all|public|private)]"
	exit 1
fi

xlsserver=$1
xlsdatabase=$2
type="public"
if [ $# -gt 2 ]
then
  type=$3
fi

xlsuser=`get_xls_registry_value ${xlsdatabase} User`
xlspass=`get_xls_registry_value ${xlsdatabase} Password`

database=sitemapdb
siteserver=`get_xls_registry_value ${database} Server`
siteuser=`get_xls_registry_value ${database} User`
sitepass=`get_xls_registry_value ${database} Password`

run() {
  $XLS/src/scripts/loading/dba/startupdate.sh search ${COMPUTERNAME} search
  check_return_code $? "error in startupdate.sh, exiting ..." $?

  # where is the index located?
  currentloc=`reginvidx get co`

  scratch=${currentloc%/*}
  indexdir=${scratch}

  cd $indexdir
  mkdir -p new
  cd new
  check_return_code $? "error in cd to ${indexdir}/new, exiting ..." $?

  rm -f *smx*
  let today=`date +%d`

  #create private company snapshots in the first week of the month only
  if [ $today -le 7 ] || [ $type == "private" ] || [ $type == "all" ]
  then
	perl $XLS/src/scripts/sitemaps/createCompanySitemaps.pl ${xlsserver} ${xlsdatabase} ${xlsuser} ${xlspass} $COMPUTERNAME private
	check_return_code $? "error in perl createCompanySitemaps.pl for private companies, exiting ..." $?

	#sanity check (make sure you have at least 10 files for private companies)
	if [ `ls *smx* |wc -l` -lt 10 ]
	then
		echo "createCompanySitemaps.pl created less than 10 private company sitemaps, exiting ..."
		exit 1
	fi

	test -s sitemap.sql
	check_return_code $? "sitemap.sql is empty, exiting ..." $?

	isql /S${siteserver} /U${siteuser} /P${sitepass} /b < sitemap.sql
	check_return_code $? "error running sitemap.sql for public companies, exiting ..." $?

	#remove sitemap index files that have 100 or more in the name (all private company sitemaps)
	rm -f ${indexdir}/search_co_${COMPUTERNAME}_1??.smx*
	mv search_co_${COMPUTERNAME}_1??.smx* ${indexdir}/
	check_return_code $? "error moving private company sitemaps to $indexdir, exiting ..." $?
  fi

  if [ $type == "private" ]
  then
	echo "don't need to create public sitemap files, exiting ..."
	$XLS/src/scripts/loading/dba/endupdate.sh search ${COMPUTERNAME} search
	exit 0
  fi

  rm -f *smx*

  perl $XLS/src/scripts/sitemaps/createCompanySitemaps.pl ${xlsserver} ${xlsdatabase} ${xlsuser} ${xlspass} $COMPUTERNAME public
  check_return_code $? "error in perl createCompanySitemaps.pl for public companies, exiting ..." $?

  #sanity check (make sure you have at least 2 files for public companies)
  if [ `ls *smx* |wc -l` -lt 2 ]
  then
	echo "createCompanySitemaps.pl created less than 2 public company sitemaps, exiting ..."
	exit 1
  fi

  test -s sitemap.sql
  check_return_code $? "sitemap.sql is empty, exiting ..." $?

  isql /S${siteserver} /U${siteuser} /P${sitepass} /b < sitemap.sql
  check_return_code $? "error running sitemap.sql for public companies, exiting ..." $?

  #remove sitemap index files that have 0?? in the name (all public company sitemaps)
  rm -f ${indexdir}/search_co_${COMPUTERNAME}_0??.smx*
  mv search_co_${COMPUTERNAME}_0??.smx* ${indexdir}/
  check_return_code $? "error moving public company sitemaps to $indexdir, exiting ..." $?

  $XLS/src/scripts/loading/dba/endupdate.sh search ${COMPUTERNAME} search
  echo "complete"

}

mkdir -p $XLSDATA/index
logfilename=$XLSDATA/index/sitemapindex`date +%y%m%d`.log
`run > ${logfilename} 2>&1`
returncode=$?
if [ $returncode -ne 0 ]
then
	blat ${logfilename} -t administrators@alacra.com -s "Company Sitemapindex generation failed"
	exit ${returncode}
fi
