XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 2 ]
then
	echo "usage: $0 dbname srcserver"
	exit 1
fi
dbname=$1
srcserver=$2

filename=${dbname}.bak.`date +%d`

backupdir=$XLSDATA/${dbname}/sqlbackup

main() {
mkdir -p ${backupdir}
cd ${backupdir}
if [ $? -ne 0 ]
then
	echo "error cd to ${backupdir}, exiting ..."
	exit 2
fi

rm -f ${filename}*

echo "user filepickup filepickup" > command.ftp
echo "binary" >> command.ftp
echo "cd /xlsdata/${dbname}/sqlbackup" >> command.ftp
echo "get ${filename}.7z" >> command.ftp
echo "bye" >> command.ftp

ftp -i -n -s:command.ftp ${srcserver}
if [ $? -ne 0 ]
then
	echo "error in ftp ${filename}.7z file from ${srcserver}, exiting ..."
	exit 1
fi

p7zip -d ${filename}.7z
if [ $? -ne 0 ]
then
	echo "error in p7zip -d of ${filename}.7z file, exiting ..."
	exit 1
fi

if [ ! -s ${filename} ]
then
	echo "${filename} is empty, exiting ..."
	exit 1
fi

chmod a+wrx ${filename}
isql /b /U${user} /P${pass} /S${server} << HERE
USE master
go
ALTER DATABASE ${dbname} SET SINGLE_USER WITH ROLLBACK IMMEDIATE
RESTORE DATABASE ${dbname} FROM DISK="${backupdir}/${filename}" WITH REPLACE
ALTER DATABASE ${dbname} SET MULTI_USER
go
HERE

if [ $? -ne 0 ]
then
	echo "error in isql to restore database ${dbname}, exiting ..."
	exit 1
fi

rm -f ${filename}
echo "Restore done"
}

server=`get_xls_registry_value ${dbname} Server`
user=`get_xls_registry_value ${dbname} User`
pass=`get_xls_registry_value ${dbname} Password`

logfile=$XLSDATA/${dbname}/restore`date +%Y%m%d`.log
(main) > $logfile 2>&1
if [ $? -ne 0 ]
then
  blat ${logfile} -t administrators@alacra.com -s "Restore failed for $dbname on $server"
  exit 1
fi

