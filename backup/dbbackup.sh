XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 1 ]
then
	echo "usage: $0 dbname"
	exit 1
fi
dbname=$1
backupdir=f:/mssql/backup
if [ $# -gt 1 ]
then
  backupdir=$2
fi


main() {

cd ${backupdir}
if [ $? -ne 0 ]
then
	echo "error cd to ${backupdir}, exiting ..."
	exit 2
fi
rm -f ${dbname}.bak*

filename=${dbname}.bak.`date +%d`
isql /b /U${user} /P${pass} /S${server} << HERE
DBCC SHRINKFILE (${dbname}_log)
go
BACKUP DATABASE ${dbname} TO DISK="${backupdir}/${filename}"
HERE

if [ $? -ne 0 ]
then
	echo "error in isql to backup database ${dbname}, exiting ..."
	exit 1
fi
if [ ! -s ${filename} ]
then
	echo "${filename} is empty, exiting ..."
	exit 1
fi

p7zip ${filename}
if [ $? -ne 0 ]
then
	echo "error in p7zip of ${filename} file, exiting ..."
	exit 1
fi

chmod a+r ${filename}.7z
rm -f nul
echo "Backup complete"
}

server=`get_xls_registry_value ${dbname} Server`
user=`get_xls_registry_value ${dbname} User`
pass=`get_xls_registry_value ${dbname} Password`

mkdir -p $XLSDATA/${dbname}

logfile=$XLSDATA/${dbname}/backup`date +%Y%m%d`.log
(main) > $logfile 2>&1
if [ $? -ne 0 ]
then
  blat ${logfile} -t administrators@alacra.com -s "Backup failed for $dbname on $server"
  exit 1
fi
