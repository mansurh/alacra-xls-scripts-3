echo "File-B Backup Started on Data2" > logfile
date >> logfile

ntbackup backup "e:\\data" /b /hc:on /t normal /l "logfile" /e /tape:0

echo "File-B Backup on Data2 Stopped" >> logfile
date >> logfile

# Mail Results of backup to Administrators group
blat "logfile" -t "administrators@xls.com" -s "File-B Backup on Data2 Log"
