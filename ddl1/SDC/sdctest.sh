localdir="/usr/ftp/sdc"
export localdir
rm -f $localdir/sdc.tar
echo "Start time (`date`)" > sdclog.log
until [ -e $localdir/DONE.ALL ]
do
	sleep 15m
done
tar -cvzf $localdir/sdc.tar $localdir/*.d*
cp sdcdone.txt $localdir/sdcdone.txt
ftp -i -n ddl2.xls.com<sdc.ftp
if [ $? !=0 ]
then
	exit 1
fi
ftp -i -n ddl2.xls.com<sdcdone.ftp
echo "End Time (`date`)" >> sdclog.log
rm -f $localdir/DONE.ALL
rm -f $localdir/*.d*
exit 0

