# Script file to build the IP matching table for SDC M&A
# 1 = IPID Server
# 2 = IPID Server
# 3 = IPID Login
# 4 = IPID Password
# 5 = SDC M&A Server
# 6 = SDC M&A Login
# 7 = SDC M&A Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: $0 IPID_server IPID_database IPID_login IPID_password sdc_server sdc_login sdc_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
sdcserver=$5
sdclogin=$6
sdcpassword=$7

ipname=sdc

# Name of the temp file to use
TMPFILE1=ipid_sdcvcfirm1.tmp
TMPFILE2=ipid_sdcvcfirm2.tmp

# Name of the table to use
TABLENAME=ipid_sdcvcfirm

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the sdc data into a temporary file
# Get the most recent version of the company name for the matching
# process

isql -S${sdcserver} -U${sdclogin} -P${sdcpassword} -s"|" -n -w4000 -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select 'matchkey'=a.FIRM_ID, 'name'=a.FIRM_NAME, 'address1'=a.STREET1,
'address2'=a.STREET2, 'city'=a.CITY, 'state'=s.statelong, 'country'=n.Nationlong,
'url'=replace(a.website,'www.','http://www.') ,firm_type_text,seek_text
from disb_port_firm a, disb_nation_lookup n, disb_state_lookup s
where a.nation=n.nation and a.state=s.state 
HERE

# Step 3 - post-process the temp file
sed -f match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	matchkey float NULL,
	name varchar(90) NULL,
	address1 varchar(60) NULL,
	address2 varchar(60) NULL,
	city varchar(30) NULL,
	state varchar(30) NULL,
	country varchar(30) NULL,
	url varchar(60) NULL,
        type varchar(260) NULL,
        seek varchar(260) NULL
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(matchkey)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(state)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(country)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /c /t"|" /b100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} < $XLS/src/scripts/matching/ipid_sdcvcfirm.sql

# Step 8 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
