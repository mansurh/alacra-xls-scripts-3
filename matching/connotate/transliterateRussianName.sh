
XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

cSrv=`get_xls_registry_value concordance Server`
cUser=`get_xls_registry_value concordance user`
cPassword=`get_xls_registry_value concordance password`

isql /S${cSrv} /U${cUser} /P${cPassword} << HERE
ALTER TABLE ipid_$1 ADD english_name varchar(max)
go

update ipid_$1 set english_name=dbo.transliterateString('russian', name)
HERE


cBkup=`get_xls_registry_value concordance Backup`

isql /S${cBkup} /U${cUser} /P${cPassword} << HERE
ALTER TABLE ipid_$1 ADD english_name varchar(max)
go

update ipid_$1 set english_name=dbo.transliterateString('russian', name)
HERE


