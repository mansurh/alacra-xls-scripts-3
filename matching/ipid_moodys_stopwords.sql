UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, '1', 'ONE')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, '2', 'TWO')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, '3', 'THREE')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, '4', 'FOUR')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, '5', 'FIVE')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, '6', 'SIX')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, '7', 'SEVEN')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, '8', 'EIGHT')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, '9', 'NINE')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'AKTIENGESELLSCHAFT', 'AG')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'ALUMINUM', 'ALUMINIUM')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'BANCA', 'BANK')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'BANCO', 'BANK')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'BANCORPORATION', 'BANCORP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'BANQUE', 'BANK')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'BCA', 'BANK')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'BCO', 'BANK')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'BERHAD', 'BHD')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'BK', 'BANK')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'BROTHERS', 'BROS')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'CIA', 'CO')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'COMERCIALE', 'COMMERCIAL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'COMMERCIALE', 'COMMERCIAL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'COMPAGNIE', 'CO')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'COMPANHIA', 'CO')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'COMPANIA', 'CO')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'COMPANY', 'CO')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'CIE', 'CO')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'CORPORACION', 'CORP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'CORPORATION', 'CORP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'COS', 'COMPANIES')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'DEVL', 'DEVELOPMENT')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'DVLP', 'DEVELOPMENT')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'EQTY', 'EQUITY')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'EQT', 'EQUITY')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'FNCL', 'FINANCIAL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'FONDO', 'FUND')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'FONDS', 'FUND')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'GP', 'GROUP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'GROEP', 'GROUP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'GROUPE', 'GROUP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'GRP', 'GROUP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'GRUPO', 'GROUP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'GRUPPO', 'GROUP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'GRUPPEN', 'GROUP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'HLD', 'HOLDINGS')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'HLDS', 'HOLDINGS')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'HLDG', 'HOLDINGS')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'HLDGS', 'HOLDINGS')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'HOLDING', 'HOLDINGS')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'INCORPORATED', 'INC')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'INCORPORATION', 'INC')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'INTERNATL', 'INTERNATIONAL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'INTL', 'INTERNATIONAL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'INTNL', 'INTERNATIONAL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'INVT', 'INVESTMENTS')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'INVTS', 'INVESTMENTS')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'KK', 'COMPANYLTD')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'KONINKLIJKE', 'REASEGUROS')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'REASEGURO', 'ROYAL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'KONINKLIJK', 'ROYAL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'KONINK', 'ROYAL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'LIMITED', 'LTD')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'LIMITEDPARTNERSHIP', 'LP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'LTDPARTNERSHIP', 'LP')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'NATL', 'NATIONAL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'NEWYORK', 'NY')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'OY', 'PLC')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'OYJ', 'PLC')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'PERSEROTERBUKA', 'PT')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'PPY', 'PROPERTY')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'PTY', 'PROPERTY')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'PUBLICCOLTD', 'PCL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'PUBLICCOMPANYLIMITED', 'PCL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'PUBLICCOMPANYLTD', 'PCL')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'PUBLICLTDCOMPANY', 'PLC')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'PUBLICLTDCO', 'PLC')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'PUBLICLIMITEDCOMPANY', 'PLC')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'PUBLICLIMITEDCO', 'PLC')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'SANAYII', 'SANAYI')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'SRVCS', 'SERVICES')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'SVCS', 'SERVICES')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'TBK', 'TERBUKA')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'UTD', 'UNITED')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'UNTD', 'UNITED')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'VCT', 'VENTURECAPITALTRUST')
UPDATE ipid_moodys
SET normalized_name = REPLACE(normalized_name, 'VERSICH', 'VERSICHERUNG')
GO
