# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
# 4 = ICC Server
# 5 = ICC Login
# 6 = ICC Password
#if [ $# -lt 6 ]
#then
#	echo "Usage: update_direct_mapping_icc.sh xls_server xls_login xls_password icc_server icc_login icc_password"
#	exit 1
#fi

year=`date +%y`
month=`date +%m`
day=`date +%d`

updatedate="$year$month$day" 
scriptdir=$XLS/src/scripts/matching
logdir=$XLSDATA/icc

mkdir -p ${logdir}

# create unique log file name, based on date
logfilename=${logdir}/icc_direct_xls${updatedate}.log

echo "direct_mapping_icc.sh: Updating XLS direct_mapping table on updatedate=${updatedate}" > ${logfilename}

# Perform the update
${scriptdir}/update_direct_mapping_icc.sh >> ${logfilename} 2>&1
returncode=$?

# Mail the results to the administrators if update was no good
if [ ${returncode} != 0 ]
then
	echo "Bad return code from weekly update of direct_mapping table for icc - mailing log file" >> ${logfilename}
	blat ${logfilename} -t "administrators@alacra.com,simon.vileshin@alacra.com" -s "Update of direct_mapping table for icc  failed"
else
	blat ${logfilename} -t "simon.vileshin@alacra.com" -s "Update of direct_mapping table for icc succeded"
#	rm -f ${logfilename}
fi

exit $returncode

