# Script file to build the IP matching table for factiva
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = Factiva Server
# 6 = Factiva Login
# 7 = Factiva Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_factiva.sh ipid_server ipid_database ipid_login ipid_password factivaserver factivalogin factivapassword"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
factivaserver=$5
factivalogin=$6
factivapassword=$7

ipname=factiva

# Name of the temp file to use
TMPFILE1=factiva.csv
TMPFILE2=factiva.pip

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=$XLS/src/scripts/matching/ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}
                              

#Step 2 - get new data into bcp file
isql -S${factivaserver} -U${factivalogin} -P${factivapassword} -s"|" -w1000 -n  -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select fdsCode, listed, name, country from factiva..company
HERE

# Step 3 - post-process the temp file
sed -e "s/,/\^/g;s/|/,/g" < ${TMPFILE1} > temp1.tmp 
qcd2pipe -l -r < temp1.tmp > temp2.tmp
sed -e "s/\^/,/g;s/NULL//g" < temp2.tmp > ${TMPFILE2}

# Step 2 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}_prev
exec sp_rename '${TABLENAME}', '${TABLENAME}_prev'
go
HERE

# Step 3 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	FDSCode varchar(12) NULL,
	listed char(1) NULL,
	factiva_name varchar(128),
--	name varchar(64) NULL,
	country varchar(32) NULL
)
GO
create clustered index ${TABLENAME}_01 on ${TABLENAME}
	(FDSCode)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(country)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(factiva_name)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(listed)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 4 - reformat the data file - OLD FILE FORMAT
#tail +2 factiva.dat | sed -f factiva.sed > ${TMPFILE1}

# Step 6 - load in the data
echo bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /f${FORMAT_FILE} /b1000
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /f${FORMAT_FILE} /b1000
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - map the countries
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} < $XLS/src/scripts/matching/ipid_${ipname}_country.sql


# Step 8 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2} temp1.tmp temp2.tmp
