# Script file to generate the IP matching reports for Investext
# Created by Colin duSaire February 12, 2002 
# Added trim function and date restriction
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_itext.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=itext

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in Investext database */
PRINT "Potentially new companies in Investext"
select RTRIM(matchkey), RTRIM(ticker), RTRIM(name), RTRIM(last_report) 
from ipid_itext
where matchkey not in (select sourcekey from company_map where source=30)

/* Required Report - Companies which have disappeared from Investext database */
PRINT "Matched companies that have disappeared from Investext"
select "AlacraID"=x.id, "Name"=RTRIM(x.name), "Country"=RTRIM(c.name) 
from company x, company_map m, country c
where
x.id = m.xlsid and x.country*=c.id and m.source=30
and
m.sourcekey not in (select matchkey from ipid_itext)

/* Optional report - new information available in database */
PRINT "Mapped Investext Companies that may have changed Name"
select "AlacraID"=a.id, "Name"=RTRIM(a.name), "Country"=RTRIM(x.name), 
"Old Mapping"=b.sourcekey, "Old Last Date"=convert(char(15),c.last_report,106), 
"Possible New Mapping"=d.matchkey, "New Last Date"=convert(char(15),d.last_report,106)
from company a, company_map b, ipid_itext c, ipid_itext d, country x
where a.id=b.xlsid and b.source=30 and b.sourcekey=c.matchkey
and a.country=x.id and c.ticker=d.ticker and d.last_report > c.last_report
and not exists
(select * from company_map where d.matchkey=sourcekey and source=30)
order by x.name, d.last_report

HERE
