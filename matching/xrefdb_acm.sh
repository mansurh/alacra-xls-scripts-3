XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_arguments.fn
. $XLSUTILS/check_bcp_errors.fn
. $XLSUTILS/check_return_code.fn
. $XLSUTILS/get_registry_value.fn

check_arguments $# 1 "Usage: xrefdb_acm.sh acmsvr concordancesvr" 1

acmsvr=$1
concsvr=$2

concuser=`get_xls_registry_value concordance user`
concpass=`get_xls_registry_value concordance password`

DATADIR=$XLSDATA/matching/acm
mkdir -p ${DATADIR}
cd ${DATADIR}
check_return_code $? "Error switching to directory ${DATADIR}, exiting" 1

echo "build a list of companies covered for each collection..."
query="select distinct type, entry_text from index_entries where entry_name like 'xlsid%' and entry_text is not null and len(entry_text) > 0 order by type, entry_text"

TMPFILE=acm_matching.dat
echo ""
echo "bcp \"${query}\" queryout ${TMPFILE} -S ${acmsvr} -U acm -P acm -c -o acm_matching.errout"
echo ""
bcp "${query}" queryout ${TMPFILE} -S ${acmsvr} -U acm -P acm -c -o acm_matching.errout

check_return_code $? "Failed to bcp out companies list, exiting" 1
check_bcp_errors acm_matching.errout 1 "Failed to bcp out companies list, exiting" 1

# delete existing company matching table records
echo "delete existing acm_company_list records where ip >= 2000 and ip < 3000..."
isql -S ${concsvr} -U ${concuser} -P ${concpass} -Q " delete from acm_company_list where ip >= 2000 and ip < 3000 "
check_return_code $? "Failed to delete existing acm_company_list records, exiting" 1

echo "load the new list of companies..."
echo ""
echo "bcp acm_company_list in ${TMPFILE} -S ${concsvr} -U ${concuser} -P ${concpass} -c -o acm_matching.errout"
echo ""
bcp acm_company_list in ${TMPFILE} -S ${concsvr} -U ${concuser} -P ${concpass} -c -o acm_matching.errout
check_return_code $? "Failed to bcp in companies list, exiting" 1
check_bcp_errors acm_matching.errout 1 "Failed to bcp in companies list, exiting" 1

compcnt=`isql -S ${concsvr} -U ${concuser} -P ${concpass} -Q " select 'count:' + convert(varchar(16), count(*)) + ':end' from acm_company_list where ip >= 2000 and ip < 3000 "`
compcnt=`echo ${compcnt} | sed -e "s/.*count://g" | sed -e "s/:end.*//g"`
echo "finished - companies mapped: ${compcnt}"

ip_list=ip_list.dat
bcp "select distinct ip from acm_company_list where ip >= 2000 order by ip" queryout ${ip_list} -U concordance -P concordance -S ${concsvr} -c > junk

while read ip
do

    echo "updating company_map for ip: ${ip}"

    echo "delete all entries from company_map table for ${ip} that are not longer mapped in acm"
    isql -S ${concsvr} -U ${concuser} -P ${concpass} -Q "delete from company_map where source = ${ip} and xlsid not in (select distinct xlsid from acm_company_list where ip = ${ip})"
    check_return_code $? "Failed to delete previous acm entries from company map, exiting" 1

    echo "inserting any new acm entries for ${ip} into company_map table"
    isql -S ${concsvr} -U ${concuser} -P ${concpass} -Q "insert into company_map (xlsid, source, sourcekey, date, primary_key) select distinct xlsid, ip, xlsid, getdate(), null from acm_company_list where xlsid != 0 and xlsid is not null and ip = ${ip} and xlsid not in (select xlsid from company_map where source = ${ip})"
    check_return_code $? "Failed to insert new acm entries into company map, exiting" 1

done < ${ip_list}

rm ${TMPFILE}
rm acm_matching.errout

exit 0
