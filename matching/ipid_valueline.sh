# Script file to build the IP matching table for valueline
# 1 = IPID Server
# 2 = IPID Server
# 3 = IPID Login
# 4 = IPID Password
# 5 = valueline Server
# 6 = valueline Login
# 7 = valueline Password
ARGS=7
if [ $# -ne $ARGS ]
then
    echo "Usage: ipid_valueline.sh IPID_server IPID_database IPID_login IPID_password valueline_server valueline_login valueline_password"
    exit 1
fi

IPIDdataserver=$1
IPIDdatabase=$2
IPIDlogon=$3
IPIDpassword=$4
srcdataserver=$5
srclogon=$6
srcpassword=$7


LOADDIR=${XLS}/src/scripts/loading/valueline
TEMPDIR=${XLSDATA}/matching/valueline
mkdir -p ${TEMPDIR}


#
# Put new data in a batch file.
#

echo ""
echo "Building bulk load file from companies table from ${srcdataserver}"
echo ""

#isql -U ${srclogon} -P ${srcpassword} -S ${srcdataserver} -r -Q "set nocount on select distinct d1.prop_value + '|' + d2.prop_value + '|' + d3.prop_value + '|' + d4.prop_value from valueline.dbo.doc_props d1, valueline.dbo.doc_props d2, valueline.dbo.doc_props d3, valueline.dbo.doc_props d4 where d1.prop_name='cusip' and d2.prop_name='name' and d3.prop_name='cusip' and d4.prop_name='ticker' and d1.doc_id=d2.doc_id and d2.doc_id=d3.doc_id and d3.doc_id=d4.doc_id and d1.prop_value is not null" > ${TEMPDIR}/ipid_valueline.dat.temp1
bcp "select distinct d1.prop_value, d2.prop_value, d3.prop_value, d4.prop_value from valueline.dbo.doc_props d1, valueline.dbo.doc_props d2, valueline.dbo.doc_props d3, valueline.dbo.doc_props d4 where d1.prop_name='cusip' and d2.prop_name='name' and d3.prop_name='cusip' and d4.prop_name='ticker' and d1.doc_id=d2.doc_id and d2.doc_id=d3.doc_id and d3.doc_id=d4.doc_id and d1.prop_value is not null" queryout ${TEMPDIR}/ipid_valueline.dat.temp1 -U ${srclogon} -P ${srcpassword} -S ${srcdataserver} /c /t"|"

cat ${TEMPDIR}/ipid_valueline.dat.temp1 | sed -f ${LOADDIR}/ipid_valueline.sed > ${TEMPDIR}/ipid_valueline.dat.temp2

#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        echo ${line}
#
#    fi
#
#done < ${TEMPDIR}/ipid_valueline.dat.temp2 > ${TEMPDIR}/ipid_valueline.dat
cp ${TEMPDIR}/ipid_valueline.dat.temp2  ${TEMPDIR}/ipid_valueline.dat
#
# Drop old data from xls table ipid_valueline
#

echo ""
echo "Dropping old ipid_valueline data on ${IPIDdataserver}"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDdataserver} < ${LOADDIR}/create_ipid_valueline.sql


# drop the indices for the table
echo ""
echo "Dropping indices for ipid_valueline for faster loading"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDdataserver} < ${LOADDIR}/dropindex_ipid_valueline.sql


#
# Load all the new data...
#

DATFILE="${TEMPDIR}/ipid_valueline.dat"

# Load the ipid_valueline table
echo "Loading the ipid_valueline table" 

bcp ${IPIDdatabase}.dbo.ipid_valueline in ${DATFILE} /U${IPIDlogon} /P${IPIDpassword} /S${IPIDdataserver} /f${LOADDIR}/ipid_valueline.fmt /e${TEMPDIR}/ipid_valueline.log
if [ $? != 0 ]
then

    echo "Error loading ipid_valueline table"
    exit 1

fi

# Rebuild the indices for the table
echo ""
echo "Rebuilding indices for ipid_valueline for faster loading"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDdataserver} < ${LOADDIR}/createindex_ipid_valueline.sql

echo ""
echo "Updated table ipid_valueline for valueline OK."
echo ""

exit 0
