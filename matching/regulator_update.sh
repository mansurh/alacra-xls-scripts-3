#!/bin/sh 

XLSUTILS=$XLS/src/scripts/loading/dba

# Arguments:
# 1 = ipid to match
# 2 = ipid database
# 3 = ipid server
# 4 = ipid login
# 5 = ipid password
# 6 = deviation number

if [ $# -lt 5 ]
then
	echo "Usage: regulator_update.sh ipidname IPID_Database IPID_Server IPID_Login IPID_Password deviation"
	exit 1
fi

ipname=$1
IPIDdatabase=$2
IPIDserver=$3
IPIDlogin=$4
IPIDpassword=$5

deviation=20
if [ $# -gt 5 ]; then
	deviation=$6
fi



main() {

newTableExistance=`isql -b -h-1 -S ${IPIDserver} -U ${IPIDlogin}  -P ${IPIDpassword} -r -Q "SET NOCOUNT ON; SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'ipid_${ipname}_new') AND type in (N'U')"`
newTableExistance=`echo $newTableExistance | tr -d '\040\011\012\015'`

oldTableExistance=`isql -b -h-1 -S ${IPIDserver} -U ${IPIDlogin}  -P ${IPIDpassword} -r -Q "SET NOCOUNT ON; SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'ipid_${ipname}') AND type in (N'U')"`
oldTableExistance=`echo $oldTableExistance | tr -d '\040\011\012\015'`

bakTableExistance=`isql -b -h-1 -S ${IPIDserver} -U ${IPIDlogin}  -P ${IPIDpassword} -r -Q "SET NOCOUNT ON; SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'ipid_${ipname}_bak') AND type in (N'U')"`
bakTableExistance=`echo $bakTableExistance | tr -d '\040\011\012\015'`

if [[ $newTableExistance != "1" ]]
then
	echo "File wasnt loaded properly in connotateResponder"
	exit 1
fi

if [[ $oldTableExistance == "1" ]]
then 
	newtablecnttmp=${ipname}${IPIDserver}new_cnt.tmp
	${XLSUTILS}/reccount.sh ${IPIDserver} ${IPIDlogin} ${IPIDpassword} ipid_${ipname}_new ${newtablecnttmp}
	if [ $? -ne 0 ]
	then
		echo "error in reccount.sh, exiting ..."
		exit 1
	fi
	typeset -i newtablecount=`cat ${newtablecnttmp}`


	oldtablecnttmp=${ipname}${IPIDserver}old_cnt.tmp
	${XLSUTILS}/reccount.sh ${IPIDserver} ${IPIDlogin} ${IPIDpassword} ipid_${ipname} ${oldtablecnttmp}
	if [ $? -ne 0 ]
	then
		echo "error in reccount.sh, exiting ..."
		exit 1
	fi
	typeset -i oldtablecount=`cat ${oldtablecnttmp}`

	echo "Counts: old - ${oldtablecount}, new - ${newtablecount}"

	typeset -i diff=${deviation}
	typeset -i max=`expr 100 + $diff`
	typeset -i min=`expr 100 - $diff`
	#don't check if counts are the same
	if [ ${newtablecount} -ne ${oldtablecount} ]
	then
	  #be careful with very small numbers, as "expr" drops the remainder (i.e. `expr 4 * 120 / 100` = 4 NOT 5)
	  if [ ${newtablecount} -gt `expr ${oldtablecount} \* $max \/ 100` ] || [ ${newtablecount} -lt `expr ${oldtablecount} \* $min \/ 100` ]
	  then
		echo "Counts between new and old $ipname are different by ${diff} %. old rec count=${oldtablecount}, new rec count=${newtablecount}"
		blat - -body "Counts between new and old $ipname are different by ${diff} %. old rec count=${oldtablecount}, new rec count=${newtablecount}" -s "FAILURE! $ipname counts are too different after update on ${IPIDserver}" -t ipidalerts@alacra.com
		exit 1
	  fi

	fi
fi

if [[ $bakTableExistance == "1" ]]
then 
	echo "Dropping bak table"
	isql /n /U${IPIDlogin} /P${IPIDpassword} /S${IPIDserver} /b /Q "DROP TABLE ipid_${ipname}_bak"
	if [ $? != 0 ]
	then

		blat - -body "DROP TABLE ipid_${ipname}_bak on ${IPIDlogin} ${IPIDpassword} ${IPIDserver} $?" -s "Error dropping backup table ${ipname}" -t ipidalerts@alacra.com

	fi  
fi

if [[ $oldTableExistance == "1" ]]
then 
	echo "renaming original table to bak"
	isql /n /U${IPIDlogin} /P${IPIDpassword} /S${IPIDserver} /b /Q "exec sp_rename 'ipid_${ipname}', 'ipid_${ipname}_bak'"
	if [ $? != 0 ]
	then
		blat - -body "exec sp_rename 'ipid_${ipname}', 'ipid_${ipname}_bak' on ${IPIDlogin} ${IPIDpassword} ${IPIDserver} $?" -s "Error renaming current ipid to backup ${ipname}" -t ipidalerts@alacra.com
		exit 1
	fi
fi

echo "Renaming new table to original table"
isql /n /U${IPIDlogin} /P${IPIDpassword} /S${IPIDserver} /b /Q "exec sp_rename 'ipid_${ipname}_new', 'ipid_${ipname}'"
if [ $? != 0 ]
then

	blat - -body "exec sp_rename 'ipid_${ipname}', 'ipid_${ipname}_bak' on ${IPIDlogin} ${IPIDpassword} ${IPIDserver} $?" -s "Error renaming new ipid to current ${ipname}" -t ipidalerts@alacra.com
	exit 1

fi

#calling more generic ipidpostupdate.sh instead of dmo_events.sh
echo "Starting Postupdate"
$XLS/src/scripts/loading/concordance/ipidpostupdate.sh -r -s ${IPIDserver} -n ${ipname}
echo "End of Postupdate"
return $?
}

mkdir -p $XLSDATA/regulators
cd $XLSDATA/regulators
logfile=${ipname}_${IPIDserver}_`date +%y%m%d`.log
(main) > ${logfile} 2>&1
if [ $? -ne 0 ]
then
  blat $logfile -t administrators@alacra.com -s "regulator update failed on ${IPIDserver} for ${ipname}"
  exit 1
fi
exit 0


