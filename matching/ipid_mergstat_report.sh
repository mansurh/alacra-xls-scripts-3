# Script file to generate the IP matching reports for mergstat
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_mergstat_report.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=mergstat

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in mergstat"
select distinct i.xlsid, i.name, i.cusip, i.ticker, i.exchange, i.buyerOrseller, i.percent_sought into #temp1 from ipid_mergstat i
delete #temp1 from #temp1 t, company_map m where t.xlsid=m.xlsid and m.source=1
select name, cusip, ticker, exchange, buyerOrseller, percent_sought from #temp1 order by name
drop table #temp1

/* Required Report - Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from mergstat"
select distinct c.id, c.name as name into #temp2 from company_map m, company c where m.source=1 and c.id=m.xlsid
delete #temp2 from #temp2 t, ipid_mergstat i where t.id=i.xlsid
select * from #temp2 order by name
drop table #temp2

/* Required Report - Closed deals for public company in last 7 days */
PRINT "Closed deals for public company in last 7 days"
select distinct convert(char(9),d.announce_date,1)'announce_date',
convert(char(9),d.close_date,1)'close_date',s.seller_name,
d.unit_sold,b.buyer_name,c.country_name,b.buyer_cusip,
b.buyer_exchange,b.buyer_ticker,c1.country_name,s.seller_ticker,
s.seller_cusip,d.u_exchange,d.deal_desc

from buyer b,seller s,deal d,country c,country c1
where b.entry_no=s.entry_no and
s.entry_no=d.entry_no 
and b.buyer_country=c.country_init
and s.seller_country=c1.country_init
and s.s_ownership_type='p'
and (s.s_ownership_type='p' or b.buyer_ownership='p')
and d.close_date>=convert(char(9),dateadd(d,-7,getdate()),1)
and d.close_date<=convert(char(9),getdate(),1)
and d.close_date is not null
and ((d.competing_bid is null) or (d.competing_bid < 2)) 
order by close_date

HERE
