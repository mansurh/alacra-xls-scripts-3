# Script file to generate the IP matching reports for Media General
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_mediagen.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=mediagen

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in Media General database */
PRINT "Potentially new companies in Media General"
select matchkey, name, NULL, ticker, exchange, hoovers_id 
from ipid_mediagen
where matchkey not in (select sourcekey from company_map where source=2)


/* Required Report - Companies which have disappeared from Media General database */
PRINT "Matched companies that have disappeared from Media General"
select x.id, x.name, "Country"=c.name, "Exchange"=y.name
from company x, company_map m, country c, exchange y, security s
where
x.id = m.xlsid and m.source=2
and
m.sourcekey not in (select matchkey from ipid_mediagen)
and
x.country=c.id
and x.id=s.issuer and s.type = 3 and s.exchange*=y.id


/* Optional Reports */



HERE
