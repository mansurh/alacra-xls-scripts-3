--IPID_ONESOURCE

if Exists(SELECT * FROM sys.objects WHERE  object_id = OBJECT_ID(N'dump_ipid_data') AND type IN ( N'P', N'PC' ))
BEGIN
	drop procedure dump_ipid_data
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE dump_ipid_data 

AS
BEGIN
	SET NOCOUNT ON;

	select matchkey=KeyId, entity_type=e.type, name=CompanyName,RegAddress1=RegisteredAddress1,City,state_prov=StateDistrict,Country,ISIN,Ticker 
	from onesource_data_load d      
	left outer join Ownershiptype_lookup o on (o.id=d.Ownershiptypeid)        
	left outer join EntityType_lookup e on (e.id=entitytypeid)

END


