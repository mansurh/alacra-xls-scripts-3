--IPID_DNB and IPID_DNBUM

if Exists(SELECT * FROM sys.objects WHERE  object_id = OBJECT_ID(N'dump_ipid_data') AND type IN ( N'P', N'PC' ))
BEGIN
	drop procedure dump_ipid_data
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dump_ipid_data     
@UMTable as Varchar(2) = 'NO'    
AS    
BEGIN    
 SET NOCOUNT ON;    
 if @UMTable = 'NO'    
 BEGIN    
  select dunsNo, name, tradestyle, addr1, addr2, city, state, country, stateAbbr, countryCode, postalCode, telephone, busDesc, SIC1, SIC2, SIC3, salesAnnUSD, hqParentDunsNo, domesticUltimateDunsNo,  globalUltimateDunsNo, globalUltimateName, typeOfEstablishment, strDunsNo from worldbase  
 END    
    
 ELSE    
 BEGIN    
  select dunsNo, name,  tradestyle, addr1, addr2, city, state, country, stateAbbr, countryCode, postalCode, telephone, busDesc, SIC1, SIC2, SIC3, salesAnnUSD, hqParentDunsNo, domesticUltimateDunsNo,  globalUltimateDunsNo, globalUltimateName, typeOfEstablishment, strDunsNo from worldbaseUM  
 END    
    
     
END  