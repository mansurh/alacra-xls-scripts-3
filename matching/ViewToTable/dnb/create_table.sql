SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
				 WHERE
                 TABLE_NAME = 'ipid_dnb'  and TABLE_TYPE = 'BASE TABLE'))
BEGIN
	drop table ipid_dnb
END
CREATE TABLE [dbo].[ipid_dnb](
	[dunsNo] [int] NOT NULL,
	[name] [varchar](90) NULL,
	[tradestyle] [varchar](90) NULL,
	[addr1] [varchar](150) NULL,
	[addr2] [varchar](64) NULL,
	[city] [varchar](64) NULL,
	[state] [varchar](30) NULL,
	[country] [varchar](20) NULL,
	[stateAbbr] [char](4) NULL,
	[countryCode] [smallint] NULL,
	[postalCode] [varchar](9) NULL,
	[telephone] [varchar](16) NULL,
	[busDesc] [varchar](41) NULL,
	[SIC1] [varchar](8) NULL,
	[SIC2] [varchar](8) NULL,
	[SIC3] [varchar](8) NULL,
	[salesAnnUSD] [numeric](20, 0) NULL,
	[employeesTotal] [int] NULL,
	[dunsNoPrev] [int] NULL,
	[hqParentDunsNo] [int] NULL,
	[domesticUltimateDunsNo] [int] NULL,
	[globalUltimateDunsNo] [int] NULL,
	[globalUltimateName] [varchar](90) NULL,
	[typeOfEstablishment] [tinyint] NULL,
	[oobInd] [char](1) NULL,
	[strDunsNo] [varchar](9) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


