 IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
				 WHERE
                 TABLE_NAME = 'ipid_onesource_kwic' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
	drop table ipid_onesource_kwic
END
Create Table [dbo].[ipid_onesource_kwic](
       [matchkey] varchar(20) null,
       [name] [varchar](30) null,
 
) ON [PRIMARY]      