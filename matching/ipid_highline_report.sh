# Script file to generate the IP table and matching reports for highline
# 1 = Output filename

if [ $# -lt 1 ]
then
	echo "Usage: ipid_highline_report.sh reportfile"
	exit 1
fi


# Save the runstring parameters in local variables
XLSUTILS=$XLS/src/scripts/loading/dba/
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn


blatfile=$1

server=`get_xls_registry_value highline server`
user=`get_xls_registry_value highline user`
password=`get_xls_registry_value highline password`

SCRIPTDIR=${XLS}/src/scripts/loading/highline

# Now build an email to send to security matching
blatfiletmp1=${blatfile}.tmp1
blatfiletmp2=${blatfile}.tmp2
rm -f ${blatfile}
rm -f ${blatfiletmp1}
rm -f ${blatfiletmp2}


echo "*** ipid_highline update info: *** " > ${blatfile}
echo "" >> ${blatfile}
echo "*** Companies added: ***" >> ${blatfile}
echo "" >> ${blatfile}

isql -w 255 -x 255 -U ${user} -P ${password} -S ${server} -r -Q "drop table ipid_highline_old"
isql -w 255 -x 255 -U ${user} -P ${password} -S ${server} -r -Q "select * into ipid_highline_old from ipid_highline"
isql -w 255 -x 255 -U ${user} -P ${password} -S ${server} < ${SCRIPTDIR}/create_ipid_highline.sql
isql -w 255 -x 255 -U ${user} -P ${password} -S ${server} < ${SCRIPTDIR}/populate_ipid_highline.sql
isql -w 255 -x 255 -U ${user} -P ${password} -S ${server} -r -Q "set nocount on select * from ipid_highline where matchkey not in (select matchkey from ipid_highline_old)" > ${blatfiletmp1}

#tail +55 < ${blatfiletmp1} | sed -f ${SCRIPTDIR}/ipid_highline.sed > ${blatfiletmp2}
#
#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        print "${line}"
#
#    fi
#
#done < ${blatfiletmp2} >> ${blatfile}

bcp "select * from ipid_highline where matchkey not in (select matchkey from ipid_highline_old)" queryout ${blatfiletmp1} -U ${xls_logon} -P ${xls_password} -S ${xls_server} -c -t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp out ADDED companies, exiting..."
	exit 1
fi
cat ${blatfiletmp1} >> ${blatfile}

echo "" >> ${blatfile}

echo "*** Companies deleted: ***" >> ${blatfile}
echo "" >> ${blatfile}
#isql -w 255 -x 255 -U ${user} -P ${password} -S ${server} -r -Q "set nocount on select * from ipid_highline_old where matchkey not in (select matchkey from ipid_highline)" > ${blatfiletmp1}
#tail +55 < ${blatfiletmp1} | sed -f ${SCRIPTDIR}/ipid_highline.sed > ${blatfiletmp2}

#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        print "${line}"
#
#    fi
#
#done < ${blatfiletmp2} >> ${blatfile}

bcp "select * from ipid_highline_old where matchkey not in (select matchkey from ipid_highline)" queryout ${blatfiletmp2} -U ${xls_logon} -P ${xls_password} -S ${xls_server} -c -t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp out DELETED companies, exiting..."
	exit 1
fi
cat ${blatfiletmp2} >> ${blatfile}


exit 0
