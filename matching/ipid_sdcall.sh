# Script file to build the IP matching table for SDC M&A
# 1 = IPID Server
# 2 = IPID Server
# 3 = IPID Login
# 4 = IPID Password
# 5 = SDC M&A Server
# 6 = SDC M&A Login
# 7 = SDC M&A Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_sdc.sh IPID_server IPID_database IPID_login IPID_password sdc_server sdc_login sdc_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
sdcserver=$5
sdclogin=$6
sdcpassword=$7

ipname=sdc

# Name of the temp file to use
TMPFILE1=ipid_sdc1.tmp
TMPFILE2=ipid_sdc2.tmp

# Name of the table to use
TABLENAME=ipid_sdcall

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the sdc data into a temporary file
# Get the most recent version of the company name for the matching
# process
isql -S${sdcserver} -U${sdclogin} -P${sdcpassword} -s"|" -n -w500 -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON

select  distinct   c1.cusip, c1.cidgen, c2.MANAMES, c2.notes, c2.city , c1.state, nl.NATION, c1.TICKER, c1.ZIP, el.EXCHANGE , c1.SICP, replace(c1.duns, '-','') duns,
c1.up, create_stamp , update_stamp, c2.internet_address url
	from 
	company1 c1, company2 c2, nation_lookup nl, exchange_lookup el  
where
c1.cidgen = c2.cidgen
and c1.NATION *= nl.CODE
and c1.EXCH *= el.CODE
	and
 	(CASE WHEN update_stamp is not null then update_stamp ELSE 'January 1, 1900' END) = (select max(CASE WHEN update_stamp is not null THEN update_stamp ELSE 'Jan 1, 1900' END) from company1 where CIDGEN=c1.CIDGEN )
and c1.cusip is not null
order by c1.cusip, c1.cidgen
HERE

# Step 3 - post-process the temp file
sed -f match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	cusip varchar(6) NULL,
        cidgen float NULL,
	name varchar(30) NULL,
	notes varchar(175) NULL,
        city varchar(30) NULL,
        state varchar(2) NULL,
	country varchar(14) NULL,
	ticker varchar(6) NULL,
	zip varchar(10) NULL,
	exchange varchar(10) NULL,
	sicp    varchar(4) NULL,
	duns varchar(12) NULL,
        up varchar(6) NULL,
        create_stamp datetime NULL,
        update_stamp datetime NULL,
        url varchar(50) NULL
  
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(cusip)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(state)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(country)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(zip)
GO
create index ${TABLENAME}_07 on ${TABLENAME}
	(exchange)
GO
create index ${TABLENAME}_08 on ${TABLENAME}
	(sicp)
GO
create index ${TABLENAME}_09 on ${TABLENAME}
	(duns)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /c /t"|" /b100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# step 7 - cross reference the exchange names
#isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} < $XLS/src/scripts/matching/ipid_${ipname}_exchange.sql

# step 8 - cross reference the country names
#isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} < $XLS/src/scripts/matching/ipid_${ipname}_country.sql

# Step 8 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
