
#icc
echo data8
isql /Uicc /Picc /Sdata8 -i DirectMappingSP_icc.sql
echo data17
isql /Uicc /Picc /Sdata17 -i DirectMappingSP_icc.sql

#experian
echo data12
isql /Uexperian /Pexperian /Sdata12 -i DirectMappingSP_experian.sql
echo data23
isql /Uexperian /Pexperian /Sdata23 -i DirectMappingSP_experian.sql

#onesource 
echo data6
isql /Uonesource /Ponesource /Sdata6 -i DirectMappingSP_onesource.sql
echo data13
isql /Uonesource /Ponesource /Sdata13 -i DirectMappingSP_onesource.sql

#amadeus 
echo data8
isql /Uamadeus /Pamadeus /Sdata8 -i DirectMappingSP_amadeus.sql
echo data21
isql /Uamadeus /Pamadeus /Sdata21 -i DirectMappingSP_amadeus.sql

#BvD Fame & Zephyr
echo data8
isql /Ubvd /Pbvd /Sdata8 -i DirectMappingSP_BvD.sql
echo data15
isql /Ubvd /Pbvd /Sdata15 -i DirectMappingSP_BvD.sql

#equifax
echo data18
isql /Uequifax /Pequifax /Sdata18 -i DirectMappingSP_Equifax.sql
echo data21
isql /Uequifax /Pequifax /Sdata21 -i DirectMappingSP_Equifax.sql

