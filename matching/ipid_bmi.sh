# Script file to build the IP matching table for SDC M&A
# 1 = IPID Server
# 2 = IPID Server
# 3 = IPID Login
# 4 = IPID Password
# 5 = bmi Server
# 6 = bmi Login
# 7 = bmi Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_bmi.sh IPID_server IPID_database IPID_login IPID_password bmi_server bmi_login bmi_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
bmiserver=$5
bmilogin=$6
bmipassword=$7

ipname=bmi

SCRIPTDIR=$XLS/src/scripts/matching
DATADIR=$XLSDATA/matching/bmi
mkdir -p ${DATADIR}
# Name of the temp file to use
TMPFILE1=${DATADIR}/ipid_bmi1.tmp
TMPFILE2=${DATADIR}/ipid_bmi2.tmp

# Name of the table to use
TABLENAME=ipid_bmi

# Name of the format file to use
FORMAT_FILE=${SCRIPTDIR}/ipid_bmi.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the bmi data into a temporary file
# Get the most recent version of the company name for the matching
# process
isql -S${bmiserver} -U${bmilogin} -P${bmipassword} -s"|" -n -w500 -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON

select  companyid, company, address1, city1, state1, zip1, country, sic6code, exchange, website, ticker  from company

HERE

# Step 3 - post-process the temp file
sed -f match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	companyid int NULL,
	name varchar(100) NULL,
	address varchar(100) NULL,
        city varchar(100) NULL,
        state varchar(100) NULL,
        zip varchar(100) NULL,
        country varchar( 100 ) NULL,
        sic6code varchar(40) NULL, 
	exchange varchar(100) NULL,
        website varchar( 100) NULL, 
        ticker varchar(6) NULL
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(companyid)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(address)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(city)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(state)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(zip)
GO
create index ${TABLENAME}_07 on ${TABLENAME}
	(country)
GO
create index ${TABLENAME}_08 on ${TABLENAME}
	(sic6code)
GO
create index ${TABLENAME}_09 on ${TABLENAME}
	(sic6code)
GO
create index ${TABLENAME}_10 on ${TABLENAME}
	(exchange)
GO
create index ${TABLENAME}_11 on ${TABLENAME}
	(website)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /f${FORMAT_FILE} /b1000000 /e ${DATADIR}/${TABLENAME}.err
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi


