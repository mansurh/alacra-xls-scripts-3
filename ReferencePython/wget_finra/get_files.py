import argparse
import sys
import os
import time
from datetime import date

def main():
	# Command line parsing.
	def msg(name='get_files.py'):
		return "python " + name + ".py TODO msg()"
	# Parser for user argument input.
	parser = argparse.ArgumentParser(prog='get_files', usage=msg())
	
	# The destination argument is used to define the directory where files will be downloaded to.
	# $XLSDATA/destination
	parser.add_argument('--destination', type=str, metavar='',nargs='?')

	args = parser.parse_args()
	
	# If the user provided a destination argument it is set here.
	destination = args.destination 
	# If the user did not define a destination argument it is set to 'FINRA'.
	if destination == None: destination = 'FINRA' 
	
	
	# ---------- VARIABLES ---------- #
	XLSDATA = os.environ.get('XLSDATA') + '/'
	XLS = os.environ.get('XLS') + '/'
	today = time.strftime('%Y-%m-%d')
	year = date.today().year
	# ---------- VARIABLES ---------- #
	
	# Create destination directory.
	unix_command = 'mkdir -p ' + XLSDATA + destination + '/data_files/' + today
	os.popen(unix_command)
	unix_command = 'mkdir -p ' + XLSDATA + destination + '/bcp_files/' + today 
	os.popen(unix_command)
	
	# Clean up the destination directory from previous times the script has been run.
	unix_command = "cd " + XLSDATA + destination + "; rm -f *.zip"
	os.popen(unix_command)
	unix_command = "cd " + XLSDATA + destination + "; rm -f *.csv"
	os.popen(unix_command)
	unix_command = "cd " + XLSDATA + destination + "; rm -f *.txt"
	os.popen(unix_command)

	# Fetch the zip files from Latest F_5500 and F_5500_SF for the past 7 years.
	base_url = 'http://askebsa.dol.gov/FOIA%20Files/'
	amount_of_years = 7 # This is the number of years the files will go back to.
	urls = []
	while amount_of_years > 0:
		this_year = year - amount_of_years
		urls.append(base_url + str(this_year) + '/Latest/F_5500_' + str(this_year) + '_Latest.zip')
		urls.append(base_url + str(this_year) + '/Latest/F_5500_SF_' + str(this_year) + '_Latest.zip')
		amount_of_years = amount_of_years - 1
	
	for url in urls:
		unix_command = 'cd ' + XLSDATA + destination + '; wget "' + url + '"'
		os.popen(unix_command).read()
		
	
	for file in os.listdir(XLSDATA+destination):
		if file.endswith(".zip"):
			unix_command = 'cd ' + XLSDATA + destination + '; unzip -o ' + file
			os.popen(unix_command).read()
			
	
	# Move the csv files to the data_files_$today directory in the destination.
	unix_command = 'cd '+XLSDATA+destination+'; mv *.csv ' + XLSDATA + destination + '/data_files/' + today
	os.popen(unix_command).read()
	
	# Fields to look for in all F_5500 csv files.
	fields = ['ACK_ID','FORM_PLAN_YEAR_BEGIN_DATE','FORM_TAX_PRD',
				'INITIAL_FILING_IND','AMENDED_IND','FINAL_FILING_IND',
				'SHORT_PLAN_YR_IND','PLAN_NAME','SPONS_DFE_PN','PLAN_EFF_DATE',
				'SPONSOR_DFE_NAME','SPONS_DFE_MAIL_US_ADDRESS1','SPONS_DFE_MAIL_US_ADDRESS2',
				'SPONS_DFE_MAIL_US_CITY','SPONS_DFE_MAIL_US_STATE','SPONS_DFE_MAIL_US_ZIP',
				'SPONS_DFE_EIN','TOT_ACTIVE_PARTCP_CNT','FILING_STATUS','DATE_RECEIVED']
				
	# Fields to look for in all F_5500_SF csv files.		
	sf_fields = ['ACK_ID','SF_PLAN_YEAR_BEGIN_DATE','SF_TAX_PRD',
					'SF_INITIAL_FILING_IND','SF_AMENDED_IND','SF_FINAL_FILING_IND',
					'SF_SHORT_PLAN_YR_IND','SF_PLAN_NAME','SF_PLAN_NUM','SF_PLAN_EFF_DATE',
					'SF_SPONSOR_NAME','SF_SPONS_US_ADDRESS1','SF_SPONS_US_ADDRESS2',
					'SF_SPONS_US_CITY','SF_SPONS_US_STATE','SF_SPONS_US_ZIP','SF_SPONS_EIN',
					'SF_TOT_ACT_PARTCP_EOY_CNT','FILING_STATUS','DATE_RECEIVED']
	
	# Dictionary of dictionaries for the F_5500 csv files. This dictionary maps ['Year']:['Column Name':'Col#'].
	# i.e. If you want to know what Col# the ACK_ID column is in for year 2013 you could access it by calling... 
	# 		ACK_ID_ROW = non_sf['2013']['ACK_ID']
	non_sf = {} 
	# Dictionary of dictionaries for the F_5500_SF csv files. This dictionary maps ['Year']:['Column Name':'Col#'].	
	# Example above.
	sf = {}
	
	# Goes through the layout files and populates the non_sf and sf dictionaries for parsing purposes.
	for file in os.listdir(XLSDATA+destination):
		file_mapping = {}
		if file.endswith(".txt"):
			curr_file = open(XLSDATA+destination+'/'+file)
			lines = curr_file.readlines()[2:]
			curr_file_split = curr_file.name.split('/')
			curr_file_name = curr_file_split[len(curr_file_split)-1]
			file_type = None
			if 'f_5500_sf' in curr_file_name:
				curr_fields = sf_fields
				file_type = 'sf'
			else:
				curr_fields = fields
				file_type = 'non-sf'
			for line in lines:
				delimited_line = line.strip('\n').split(',')
				col_name = delimited_line[1]
				if col_name in curr_fields:
					file_mapping[col_name] = int(delimited_line[0]) - 1
			if file_type == 'sf':
				index = curr_file_name.index('f_5500_sf_')
				year = curr_file_name[index+10:index+14]
				sf[year] = file_mapping
			else:
				index = curr_file_name.index('f_5500_')
				year = curr_file_name[index+7:index+11]
				non_sf[year] = file_mapping
	
	for key in non_sf:
		curr_file = XLSDATA+destination+'/data_files/'+today+'/f_5500_'+key+'_latest.csv'
		txt_file = XLSDATA+destination+'/data_files/'+today+'/f_5500_'+key+'_latest.txt'
		unix_command = 'qcd2pipe.exe  -f ' + curr_file + ' > tmp'+key+'.txt;cp tmp'+key +'.txt '+ txt_file
		os.popen(unix_command).read()
	for key in sf:
		curr_file = XLSDATA+destination+'/data_files/'+today+'/f_5500_sf_'+key+'_latest.csv'
		txt_file = XLSDATA+destination+'/data_files/'+today+'/f_5500_sf_'+key+'_latest.txt'
		unix_command = 'qcd2pipe.exe  -f ' + curr_file + ' > tmp'+key+'.txt;cp tmp'+key +'.txt '+ txt_file
		os.popen(unix_command).read()

	# Creates an empty bcp file.
	bcp_file = open(XLSDATA+destination+'/bcp_files/'+today+'/FINRA_wget.txt','w')
		
		
	for key in non_sf:
		curr_file = None
		try:
			curr_file = open(XLSDATA+destination+'/data_files/'+today+'/f_5500_'+key+'_latest.txt')
		except FileNotFoundError:
			time.sleep(5)
			curr_file = open(XLSDATA+destination+'/data_files/'+today+'/f_5500_'+key+'_latest.txt')
		curr_file.readline()
		curr_file.readline()
		for line in curr_file:
			split_line = line.strip('\n').split('|')
			if len(split_line) > 1:
				bcp_string = key + '|'
				for field in fields:
					try:
						bcp_string += split_line[non_sf[key][field]] + '|'
					except KeyError:
						bcp_string += '|'
				bcp_string = bcp_string[:-1]
				bcp_string += '\r\n'
				bcp_file.write(bcp_string)		
			
			
	for key in sf:
		curr_file = None
		try:
			curr_file = open(XLSDATA+destination+'/data_files/'+today+'/f_5500_sf_'+key+'_latest.txt')
		except FileNotFoundError:
			time.sleep(5)
			curr_file = open(XLSDATA+destination+'/data_files/'+today+'/f_5500_sf_'+key+'_latest.txt')
		curr_file.readline()
		curr_file.readline()
		for line in curr_file:
			split_line = line.strip('\n').split('|')
			if len(split_line) > 1:
				bcp_string = key + '|'
				for field in sf_fields:
					try:
						bcp_string += split_line[sf[key][field]] + '|'
					except KeyError:
						bcp_string += '|'
				bcp_string = bcp_string[:-1]
				bcp_string += '\r\n'
				bcp_file.write(bcp_string)	
			
			
	'''	
	# Loops through the non_sf dictionary and adds data to the bcp_file using the layout from the dictionary.
	# In order to get the correct data a lot of parsing has to take place...
	# Example csv data - "value1","value2","value,three",,"5",6,,,,12,,"v4"
	for key in non_sf:
		curr_file = None
		try:
			curr_file = open(XLSDATA+destination+'/data_files/'+today+'/f_5500_'+key+'_latest.csv')
		except FileNotFoundError:
			time.sleep(5)
			curr_file = open(XLSDATA+destination+'/data_files/'+today+'/f_5500_'+key+'_latest.csv')
		lines = curr_file.readlines()[1:]
		for line in lines:
			split_line = line.strip('\n').split(',')
			delimited_line = []
			index = 0
			skip_value = False;
			stop_skipping_at = -1
			for value in split_line:
				if value == '':
					if not skip_value:
						delimited_line.append(value)
				elif value[0] == '"':
					if value[-1] == '"':
						if len(value) > 1:
							delimited_line.append(value)
					else:
						last_char = ''
						tmp_index = index+1
						while last_char != '"':
							if len(split_line[tmp_index]) < 1:
								last_char = ''
							else:
								last_char = split_line[tmp_index][-1]
							tmp_index += 1
						skip_value = True
						stop_skipping_at = tmp_index 
						line_string = ''
						while tmp_index > index:
							line_string = split_line[tmp_index-1] + line_string
							tmp_index -= 1
						delimited_line.append(line_string)
				elif not skip_value:
					delimited_line.append(value)
				index += 1
				if stop_skipping_at == index:
					skip_value = False
				
			bcp_string = key + ','
			count = 0
			for val in delimited_line:
				count = count + 1
			for field in fields:
				try:
					index = non_sf[key][field]
					bcp_string += delimited_line[index] + ','
				except KeyError:
					bcp_string += ','
			bcp_string = bcp_string[:-1]
			bcp_string += '\r\n'
			bcp_string = bcp_string.replace('"','')
			bcp_file.write(bcp_string)
			
	# Loops through the non_sf dictionary and adds data to the bcp_file using the layout from the dictionary.	
	for key in sf:
		curr_file = None
		try:
			curr_file = open(XLSDATA+destination+'/data_files/'+today+'/f_5500_sf_'+key+'_latest.csv')
		except FileNotFoundError:
			time.sleep(5)
			curr_file = open(XLSDATA+destination+'/data_files/'+today+'/f_5500_sf_'+key+'_latest.csv')
		lines = curr_file.readlines()[1:]
		for line in lines:
			split_line = line.strip('\n').split(',')
			delimited_line = []
			index = 0
			skip_value = False;
			stop_skipping_at = -1
			for value in split_line:
				if value == '':
					if not skip_value:
						delimited_line.append(value)
				elif value[0] == '"':
					if value[-1] == '"':
						if len(value) > 1:
							delimited_line.append(value)
					else:
						last_char = ''
						tmp_index = index+1
						while last_char != '"':
							if len(split_line[tmp_index]) < 1:
								last_char = ''
							else:
								last_char = split_line[tmp_index][-1]
							tmp_index += 1
						skip_value = True
						stop_skipping_at = tmp_index 
						line_string = ''
						while tmp_index > index:
							line_string = split_line[tmp_index-1] + line_string
							tmp_index -= 1
						delimited_line.append(line_string)
				elif not skip_value:
					delimited_line.append(value)
				index += 1
				if stop_skipping_at == index:
					skip_value = False
				
			bcp_string = key + ','
			for field in sf_fields:
				try:
					index = sf[key][field]
					bcp_string += delimited_line[index] + ','
				except KeyError:
					bcp_string += ','
			bcp_string = bcp_string[:-1]
			bcp_string += '\r\n'
			bcp_string = bcp_string.replace('"','')
			bcp_file.write(bcp_string)
	'''
	
	
	
if __name__ == '__main__':
	main()
	