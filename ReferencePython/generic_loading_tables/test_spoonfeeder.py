import pickle
import codecs
import re



re_ids = re.compile(r"<UniqueID>(C[0-9]+|[0-9]+)</UniqueID>",re.UNICODE|re.DOTALL)


p_chunk=''
def all_ids(data_files):
    global re_ids
    global p_chunk
    for path in data_files:
        with codecs.open(path,'r',encoding='utf16') as f:
            remainder=''
            while True:
                chunk = f.read(40000000)
                if (len(chunk) == 0):
                    break

                search_string = remainder + chunk
                matches = re_ids.finditer(search_string)
                
                lastindex=0
                for m in matches:
                    #print(m)
                    id = m.group(1)
                    #print(m.group(0))
                    lastindex = m.end(0)
                    #if lastindex > -1:
                    #    remainder = search_string[lastindex:]
                    #else:
                    #    remainder = search_string
                    #print('--------------------------')
                    #print(m.group(0))
                    #print('Rem',remainder)
                    #print('Chunk',search_string)
                    yield id
                #print(matches)
                if lastindex > -1:
                    remainder = search_string[lastindex:]
                else:
                    remainder = search_string

#load the processed ids list
with open('my_ids','rb') as f:
    my_ids = pickle.load(f)

'''
data_files =['/cygdrive/c/Users/acortina/Documents/C6/dataloading/Individual_1.xml',
             '/cygdrive/c/Users/acortina/Documents/C6/dataloading/Individual_2.xml',
             '/cygdrive/c/Users/acortina/Documents/C6/dataloading/Individual_3.xml',
             '/cygdrive/c/Users/acortina/Documents/C6/dataloading/Individual_4.xml',
             '/cygdrive/c/Users/acortina/Documents/C6/dataloading/Individual_5.xml',
             '/cygdrive/c/Users/acortina/Documents/C6/dataloading/Individual_6.xml']
             '''

data_files =['/cygdrive/c/Users/acortina/Documents/C6/dataloading/Business.xml']
        
test_ids = []

'''
#populate ids list
for id in all_ids(data_files):
    test_ids.append(id)


if len(my_ids) != len(test_ids):
    raise Exception("lists are different leghts",len(my_ids),len(test_ids))
print('Print passed the cardinality test')

my_ids = sorted(my_ids)
test_ids = sorted(test_ids)


for a,b in zip(my_ids,test_ids):
    if a != b:
        raise Exception('Id mismatch',a,b)
print('Passed the equality test')        

'''

itr=0
for id in all_ids(data_files):
    #compare ids
    poped_id = my_ids.pop(0)
    itr += 1
    if itr % 10000 == 0:
        print('Record:',itr,'id:',id)
    if id != poped_id:
        raise Exception(id,'did not match ',poped_id)
        
        
