exec moodys_issuer_file_proc

----------------------------
Create PROCEDURE [dbo].[moodys_issuer_file_proc]        
AS          
BEGIN          
      
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[moodys_issuer_file_python]') AND type in (N'U'))      
DROP TABLE [dbo].[moodys_issuer_file_python]      
      
           
CREATE TABLE moodys_issuer_file_python(        
 [org_id] [bigint] NULL,        
 [org_name] [varchar](255) NULL,        
 [ticker] [varchar](16) NULL,        
 [ltr] [varchar](16) NULL,        
 [ltr_date] [datetime] NULL,        
 [ltr_class] [varchar](80) NULL,        
 [ltr_ind] [varchar](255) NULL,        
 [issuer_rating] [varchar](16) NULL,        
 [issuer_rating_date] [datetime] NULL,        
 [issuer_rating_endors_ind] [varchar](59) NULL,        
 [issuer_rating_unsolicit_ind] [varchar](255) NULL,        
 [issuer_rating_off_code] [int] NULL,        
 [issuer_rating_withdrawn_reason] [varchar](255) NULL,        
 [issuer_rating_for_c] [varchar](16) NULL,        
 [issuer_rating_for_c_date] [datetime] NULL,        
 [issuer_rating_for_c_endors_ind] [varchar](255) NULL,        
 [issuer_rating_for_c_unsolicit_ind] [varchar](255) NULL,        
 [issuer_rating_for_c_off_code] [int] NULL,        
 [issuer_rating_for_c_withdrawn_reason] [varchar](255) NULL,        
 [issuer_rating_dom_c] [varchar](16) NULL,        
 [issuer_rating_dom_c_date] [datetime] NULL,        
 [issuer_rating_dom_c_endors_ind] [varchar](255) NULL,        
 [issuer_rating_dom_c_unsolicit_ind] [varchar](255) NULL,        
 [issuer_rating_dom_c_off_code] [int] NULL,        
 [issuer_rating_dom_c_withdrawn_reason] [varchar](255) NULL,        
 [str_most_recent] [varchar](16) NULL,        
 [str_most_recent_date] [datetime] NULL,        
 [str_most_recent_class] [varchar](80) NULL,        
 [str_ind] [varchar](5) NULL,        
 [corp_fam_rating] [varchar](16) NULL,        
 [corp_fam_rating_date] [datetime] NULL,        
 [corp_fam_rating_endors_ind] [varchar](255) NULL,        
 [corp_fam_rating_unsolicit_ind] [varchar](255) NULL,        
 [corp_fam_rating_off_code] [int] NULL,        
 [corp_fam_rating_withdrawn_reason] [varchar](60) NULL,        
 [est_senior_rating] [varchar](16) NULL,        
 [est_senior_rating_date] [datetime] NULL,        
 [outlook] [varchar](16) NULL,        
 [outlook_date] [datetime] NULL,        
 [watchlist_indicator] [varchar](4) NULL,        
 [watchlist_indicator_date] [datetime] NULL,        
 [watchlist_reason] [varchar](32) NULL,        
 [reserved_1] [varchar](65) NULL  
) ON [PRIMARY]        
        
         
insert into moodys_issuer_file_python(org_id)        
select distinct Organization_ID       
from CFG_moodysload_organization_ratings       
where (Rating_Class_Number in (-1,-2) or (Evaluation_Type_Code=25649 and Evaluation_Type_Text='Ratings Outlook'))        
       
       
update moodys_issuer_file_python    
set org_name = Moodys_Legal_Name    
from CFG_moodysload_organization_rating_root    
where org_id = Organization_ID    
      
        
update moodys_issuer_file_python        
set ticker = Organization_ID_Value        
from CFG_moodysload_organization_identifiers        
where Organization_ID = org_id        
and ID_Type_Code = 24901        
        
        
        
update moodys_issuer_file_python        
set ltr = rating_text, ltr_date = Rating_Local_Date,  ltr_class = rating_class_text        
from CFG_moodysload_organization_ratings        
where Organization_ID = org_id        
and Rating_Class_Number = -1        
        
        
        
        
update moodys_issuer_file_python        
set issuer_rating = rating_text , issuer_rating_date =Rating_Local_Date        
from CFG_moodysload_organization_ratings        
where Organization_ID = org_id        
and Security_Class_Code = 18690        
and Currency_Capd_Code = 19140        
and Duration_Code = 25636        
and Shadow_Code in (19139,2258)        
        
        
        
        
update moodys_issuer_file_python        
set issuer_rating_endors_ind = Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where        
 r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code= 5156627        
and Security_Class_Code = 18690        
and Currency_Capd_Code = 19140        
and Duration_Code = 25636        
and Shadow_Code in (19139,2258)        
        
        
update moodys_issuer_file_python        
set issuer_rating_unsolicit_ind = Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where         
r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code in (5156628,5159693,999999)        
and Security_Class_Code = 18690        
and Currency_Capd_Code = 19140        
and Duration_Code = 25636        
and Shadow_Code in (19139,2258)                   
        
        
update moodys_issuer_file_python        
set  issuer_rating_withdrawn_reason =Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where         
r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code = 5156692        
and Security_Class_Code = 18690        
and Currency_Capd_Code = 19140        
and Duration_Code = 25636        
and Shadow_Code in (19139,2258)        
        
        
        
        
        
update moodys_issuer_file_python        
set issuer_rating_for_c = Rating_Text,  issuer_rating_for_c_date = CFG_moodysload_organization_ratings.Rating_Local_Date        
from CFG_moodysload_organization_ratings        
where         
Organization_ID = org_id        
and CFG_moodysload_organization_ratings.Security_Class_Code = 18690        
and CFG_moodysload_organization_ratings.Currency_Capd_Code = 19141        
and CFG_moodysload_organization_ratings.Duration_Code = 25636        
and CFG_moodysload_organization_ratings.Shadow_Code in (19139,2258)        
        
        
        
        
        
update moodys_issuer_file_python        
set issuer_rating_for_c_endors_ind = Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where         
r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code = 5156627        
and Security_Class_Code = 18690        
and Currency_Capd_Code = 19141        
and Duration_Code = 25636        
and Shadow_Code in (19139,2258)        
        
        
update moodys_issuer_file_python        
set issuer_rating_for_c_unsolicit_ind = Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where         
r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code  in ( 5156628, 5159693,999999)        
and Security_Class_Code = 18690        
and Currency_Capd_Code = 19141        
and Duration_Code = 25636        
and Shadow_Code in (19139,2258)             
        
        
update moodys_issuer_file_python        
set issuer_rating_for_c_withdrawn_reason = Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where         
r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code = 5156692        
and Security_Class_Code = 18690        
and Currency_Capd_Code = 19141        
and Duration_Code = 25636        
and Shadow_Code in (19139,2258)        
        
        
        
        
update moodys_issuer_file_python        
set issuer_rating_dom_c = Rating_Text, issuer_rating_dom_c_date = Rating_Local_Date        
from CFG_moodysload_organization_ratings        
where        
Organization_ID = org_id        
and Security_Class_Code = 18690        
and Currency_Capd_Code = 19142        
and Duration_Code = 25636        
and Shadow_Code in (19139,2258)        
        
        
update moodys_issuer_file_python        
set issuer_rating_dom_c_endors_ind =Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where         
r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code in (5156627)        
and Security_Class_Code = 18690        
and Currency_Capd_Code = 19142        
and Duration_Code = 25636        
and Shadow_Code in (19139,2258)        
        
        
        
        
update moodys_issuer_file_python        
set issuer_rating_dom_c_unsolicit_ind = Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where         
r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code in (5156628,5159693,999999)        
and Security_Class_Code = 18690        
and Currency_Capd_Code = 19142        
and Duration_Code = 25636        
and Shadow_Code in (19139,2258)                 
        
        
update moodys_issuer_file_python        
set issuer_rating_dom_c_withdrawn_reason = Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where         
r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code = 5156692        
and Security_Class_Code = 18690        
and Currency_Capd_Code = 19142        
and Duration_Code = 25636        
and Shadow_Code in (19139,2258)        
        
        
        
update moodys_issuer_file_python        
set str_most_recent = Rating_Text,  str_most_recent_date = Rating_Local_Date,         
str_most_recent_class = Rating_Class_Text        
from CFG_moodysload_organization_ratings        
where Organization_ID = org_id        
and Rating_Class_Number = -2        
        
        
        
        
update moodys_issuer_file_python        
set corp_fam_rating = Rating_Text, corp_fam_rating_date = Rating_Local_Date        
from CFG_moodysload_organization_ratings        
where Organization_ID = org_id        
and Security_Class_Code = 18715        
and Duration_Code = 25636        
        
        
        
        
        
update moodys_issuer_file_python        
set corp_fam_rating_endors_ind = Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where         
r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code = 5156627        
and Security_Class_Code = 18715        
and Currency_Capd_Code =  129968        
and Duration_Code = 25636        
        
        
        
update moodys_issuer_file_python        
set  corp_fam_rating_unsolicit_ind = Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where         
r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code  in (  5156628,519693,999999)        
and Security_Class_Code = 18715        
and Duration_Code = 25636        
        
update moodys_issuer_file_python        
set  corp_fam_rating_withdrawn_reason = Rating_Attribute_Text        
from CFG_moodysload_organization_ratings_attribute r1        
join  CFG_moodysload_organization_ratings r on  r.organization_id = r1.organization_id        
where         
r1.Organization_ID = org_id        
and Rating_Attribute_Type_Code  in (  5156692)        
and Security_Class_Code = 18715        
and Duration_Code = 25636            
        
        
update moodys_issuer_file_python        
set outlook = Rating_Text, outlook_date =Rating_Local_Date        
from CFG_moodysload_organization_ratings        
where Organization_ID = org_id        
and Evaluation_Type_Code = 25649        
and Evaluation_Type_Text = 'Ratings Outlook'       
        
    
    select * from moodys_issuer_file_python
           
END     