origin|select if5.ack_id, 'us', if5.spons_dfe_mail_us_state,if5.plan_name,
if5.spons_dfe_mail_us_city, if5.DATE_RECEIVED, spons_dfe_ein+spons_dfe_pn 
from (
select spons_dfe_ein+spons_dfe_pn as concat, max(cast(form_tax_prd as date)) as maxdate 
from FINRA_wget 
where (final_filing_ind<>'1' or final_filing_ind is null)
and (short_plan_yr_ind <> '1' or short_plan_yr_ind is null)
and (tot_active_partcp_cnt<>'0' and tot_active_partcp_cnt is not null)
and DATE_RECEIVED > (DATEADD(year,-2,getdate()))
group by spons_dfe_ein+spons_dfe_pn) latest 
join FINRA_wget if5 
on latest.maxdate = if5.form_tax_prd 
and latest.concat = if5.spons_dfe_ein+spons_dfe_pn 
and if5.spons_dfe_ein+spons_dfe_pn not in (select spons_dfe_ein+spons_dfe_pn from FINRA_wget where final_filing_ind=1) 
and (final_filing_ind<>'1' or final_filing_ind is null) 

