import sys
import os
import pyodbc
from ftplib import FTP
import os.path
import subprocess as sproc
from log_me import logger
import time
from shutil import copyfile
import traceback
from os import listdir
from os.path import isfile, join, basename, splitext

"""
"""
class File_Hand(object):
	client = ''
	scriptdir = 'c:/users/xls/src/scripts/ReferencePython/generic_loading_tables/'
	#loaddir = os.environ.get('XLSDATA') + '/' + client + '/'
	loaddir = os.environ.get('XLSDATA') + '/' 
	today = time.strftime('%Y%m%d')
	now = time.strftime('%Y-%m-%d-%H')
	files = None
	logging = None
	
	"""
	"""
	def __init__(self, client_dir, logger):		
		### logging ###
		self.logging = logger
		### logging ###
		self.client = client_dir

	"""
	"""		
	def parser(self, col_num, delim, delim_indices, data):
		parsed = []
		#columns = [col[int(col_num)].split(delim) for col in data]
		columns = []
		for col in data:
			if col[int(col_num)] != None:
				columns.append(col[int(col_num)].split(delim))
			else:
				columns.append('')
		for col in columns:
			if delim_indices != None:
				parsed_column = []
				for index in delim_indices:
					if len(col) > int(index):
						parsed_column.append(col[int(index)])
					else:
						parsed_column.append('')
				parsed.append(parsed_column)
			else:
				parsed.append(col)
		return parsed

	"""
	"""		
	def update_data_with_parsed(self, col_num, data, parsed): 
		row_index = 0
		row_length = len(data[0])
		parsed_row_length = len(parsed[0])
		for pyodbc_row in data:
			row = [x for x in pyodbc_row]
			col_index = 0
			while col_index < row_length + parsed_row_length:
				if col_index == int(col_num):
					row.pop(col_index)
					for p in reversed(parsed[row_index]):
						row.insert(col_index, p)
				col_index += 1
			data[row_index] = row
			row_index += 1
		return data

	"""
	"""		
	def get_parser_param(self, parse_file):
		file = open(parse_file,'r')
		file_text = file.read()
		if file_text.strip('\n') == 'None' or len(file_text) == 0:
			return None
		else:
			paramaters = []
			for line in file_text.split('\n'):
				paramaters.append(line)
			return paramaters
			
	"""
	"""			
	def get_table_columns(self, create_file):
		file = open(create_file)
		columns = []
		for line in file:
			col = line.split('|')
			col[1] = col[1].strip('\n')
			columns.append(col)
		return columns

	"""
	"""		
	def get_sql_command(self, load_file):
		file = open(load_file).readlines()
		sql_command = ''
		for line in file:
			sql_command += line
		return sql_command

	def get_procedure(self, procedure_file):
		file = open(procedure_file,'r')
		file_text = file.readline()
		if file_text.strip('\n') == 'None' or len(file_text) == 0:
			return None
		else:
			#procedure = ''
			#for line in file_text.split('\n'):
			#	procedure += line
			#return procedure
			return file_text

	"""
	"""		
	def get_arg_files(self, client_dir, table_name):
		my_path = os.environ.get('XLS')+'/src/scripts/referencepython/generic_loading_tables/arg_files/' + client_dir + '/' # change this to a more permanent path
		arg_files = [f for f in listdir(my_path) if isfile(join(my_path,f))]
		if table_name != None:
			update_arg_files = []
			for f in arg_files:
				if splitext(basename(f))[1] == '.txt' and f.split("_",1)[1] == table_name + '.txt':
					update_arg_files.append(f)
			arg_files = update_arg_files
		files = {}
		for f in arg_files:
			if splitext(basename(f))[1] == '.txt':
				f_name = splitext(basename(f))[0]
				f_name = f_name.split("_",1)[1]
				if f_name in files:
					files[f_name].append(f)
				else: 
					files[f_name] = [f]
		return files	

	"""
	"""		
	def put_data_in_file(self, file_name, data):
		delimiter = '|'
		file = open(self.loaddir+self.client+'/'+file_name, 'w')
		for row in data:
			col_index = 0
			for col in row:
				if col == None:
					pass
				else:
					file.write(str(col))
				if col_index < len(row)-1: file.write(delimiter)
				col_index += 1
			file.write('\r\n')
		file.close()

	"""
	"""		
	def check_file_size(self, file_name, threshold):
		passed_test = False
		if '.' not in file_name:
			file_name = file_name + '.txt'
		unix_command = '''wc -l ''' + self.loaddir + self.client + '/' + file_name + ''' |cut -f1 -d" "'''
		row_count = os.popen(unix_command).read()
		row_count.strip('\n')
		try:
			if int(row_count) < threshold:
				print(file_name + " does not meet the row threshold of " + str(threshold) + ".")
				self.logging.error(file_name + " does not meet the row threshold of " + str(threshold) + ".")
			elif int(row_count) > threshold:
				passed_test = True
			else:
				print('The else statement in check_file_size() was reached and should not ever be reached.') # change this into some sort of error 
		except ValueError:
			traceback.print_exc()
			self.logging.error('ValueError: check_file_size: '+file_name+' row_count was unable to be converted to an int')
		return passed_test

	"""
	"""		
	def create_load_directory(self):
		unix_command = 'mkdir -p ' + self.loaddir + self.client + '/log'
		print(os.popen(unix_command).read())
		unix_command = 'mkdir -p ' + self.loaddir + self.client + '/back/' + self.today
		print(os.popen(unix_command).read())


	"""
	"""		
	def move_files(self, file_name):
		unix_command = 'touch ' + self.loaddir + self.client + '/' + file_name
		print(os.popen(unix_command).read())
		time.sleep(1)
		
		unix_command = 'mv ' + self.loaddir + self.client + '/' + file_name + ' ' + self.loaddir + self.client + '/back/' + self.today + '/' + splitext(file_name)[0] + self.now + splitext(file_name)[1]
		os.popen(unix_command).read()
		time.sleep(1)
		
		#unix_command = 'mv ' + file_name + ' ' + self.loaddir + self.client + '/' + file_name
		#os.popen(unix_command)

	"""
	"""
	def move_arg_files(self, origin, files):
		for file_name in files:
			unix_command = 'touch ' + self.scriptdir + 'arg_files/' + origin + '/' + file_name
			os.popen(unix_command).read()
			time.sleep(1)
			
			unix_command = 'mv ' + self.scriptdir + 'arg_files/' + origin + '/' + file_name + ' ' + self.loaddir + self.client + '/back/' + self.today + '/' + splitext(file_name)[0] + self.now + splitext(file_name)[1]
			os.popen(unix_command).read()
			time.sleep(1)
			
	"""
	not currently in use
	"""
	#def move_log(self, file_name):
	#	unix_command = 'touch ' + self.scriptdir + file_name ##need to change some loddir to scriptdir and self.client needs to be changed so it always is client
	#	os.popen(unix_command)
	#	time.sleep(1)
	#	
	#	unix_command = 'mv ' + self.scriptdir + file_name + ' ' + self.loaddir + self.client + '/log/' + splitext(file_name)[0] + self.now + splitext(file_name)[1]
	#	os.popen(unix_command)
	#	time.sleep(1)

	
	
		