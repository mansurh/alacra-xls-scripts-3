XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_return_code.fn
. $XLSUTILS/get_registry_value.fn

echo "Beginning the monthly SMBC Reports"

# Determine who we are emailing to
stage="dev"
inputArguments=$#
if [ $inputArguments -ge 0 ]
then stage=$1
fi

case ${stage} in
	"prod") emaillist=""
	;;  
	*) emaillist="colin.wong@opus.com,david.davenport@opus.com"
	;;
esac

updatedate=`date +%Y%m%d`
datadir=$XLSDATA/smbc/auditreport/${updatedate}
logfile=${datadir}/auditreport_log.log

mkdir -p ${datadir}
check_return_code $? "Failed to make directory ${datadir}" 1
cd $datadir
check_return_code $? "Failed to navigate to ${datadir}" 1

$XLS/src/scripts/emailupdates/smbc/auditreport/smbcaudit.exe

fileuploadcsv="FileUploadHistory.xlsx"
mofalistcsv="SMBCMOFAList.xlsx"
shortlistcsv="SMBCShortList.xlsx"

echo "Emailing SMBC Audit files" >> ${logfile}
blat -body "Reports attached" -attach ${fileuploadcsv},${mofalistcsv},${shortlistcsv} -s "SMBC Audit Reports ${updatedate}" -t colin.wong@opus.com

#if [ $? != 0 ]
#then
#  	#send email to admin
#  	blat ${logfile} -s "Emaling SMBC Audit Reports ${updatedate} failed" -t administrators@alacra.com
#  	exit 1
#fi

rm -rf *.xlsx

