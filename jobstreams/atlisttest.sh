echo "DO NOT RUN WITHOUT PERMISSION FROM Information Systems personnel!!"
# Define the scheduling server
schedserver="768288-UASVC07S"
if [ $COMPUTERNAME != $schedserver ]
then
    echo "Not $schedserver, exiting..."
    exit 1
fi

#exit

# Define the scheduling server
#schedserver=datastage

# Clear all existing jobs
#SCHTASKS /Delete /TN "*" /F

# Set up the daily alerts in dev
SCHTASKS /Create /F /SC HOURLY /MO 1 /st 00:10 /et 18:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/testalerts" /TN "testalerts" /RU ""

# Set up the reindexing process of major tables in alerts databse
#SCHTASKS /Create /F /SC DAILY /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/alerts_reindex_test" /TN "alerts_reindex_test" /RU ""


# NEX Send 
SCHTASKS /Create /F /SC DAILY /ST 16:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/NEX_send" /TN "Nex Send test" /RU ""

# bmi 
#SCHTASKS /Create /F /SC Weekly /D TUE,WED,THU,FRI,SAT /ST 06:30  /TR "loadjob c:/users/xls/src/scripts/jobstreams/bmi_test"  /TN "bmi_test" /RU ""

# FinCEN 
SCHTASKS /Create /F /SC DAILY /ST 03:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/FinCEN_test" /TN "FinCEN_test" /RU ""

# S&P V3 incremental 
#SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 16:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ipid_spcred_inc_test" /TN "S&P V3 inc" /RU ""

# S&P V3 full 
#SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 09:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ipid_spcred_full_test" /TN "S&P V3 full" /RU ""

#djwatchliststage               
SCHTASKS /Create /F /SC DAILY /ST 01:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/djwatchliststage" /TN "djwatchliststage" /RU ""

#djwatchlisttest
SCHTASKS /Create /F /SC DAILY /ST 01:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/djwatchlistTest" /TN "djwatchlistTest" /RU ""

SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 08:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/djwatchlistFullTest" /TN "djwatchlistFull" /RU ""

#world-check daily load
SCHTASKS /Create /F /SC DAILY /ST 15:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wcheckTest" /TN "wcheckTest" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 15:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wcheckStage" /TN "wcheckstage" /RU ""

# Load sanctions lists
SCHTASKS /Create /F /SC DAILY /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/sanctionsTest" /TN "sanctionsTest" /RU ""

#RDC on test, testing new PM feed
#The incremental on day 1 at 7am will fail, incremental at 2pm will probably have file
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rdcFullTest" /TN "rdcFullTest" /RU ""
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rdcFullStage" /TN "rdcFullStage" /RU ""

#RDC incremental
SCHTASKS /Create /F /SC DAILY /ST 07:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rdcIncrementalTest" /TN "rdcIncrementalTest" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 07:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rdcIncrementalStage" /TN "rdcIncrementalStage" /RU ""
#SCHTASKS /Create /F /SC DAILY /ST 08:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rdcNMcompare" /TN "rdcNMcompare" /RU ""
#SCHTASKS /Create /F /SC DAILY /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/monitoringWFTest" /TN "monitoringWFTest" /RU ""

# Concordance copy to test
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 20:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/copy_concordance_test" /TN "copy_concordance_test" /RU ""

#Compliance batch alerts
SCHTASKS /Create /F /SC MINUTE /MO 15 /st 00:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/compbatch_alerts_test" /TN "compbatch_alerts_test" /RU ""

#ESMA Load
SCHTASKS /Create /F /SC DAILY /ST 09:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ipid_ESMA" /TN "esma load" /RU ""

# Nomura RR placement
#SCHTASKS /Create /F /SC DAILY /ST 00:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/acre_rr_stage" /TN "acre_rr_stage" /RU ""

# ACE RR placement
#SCHTASKS /Create /F /SC DAILY /ST 00:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ace_rr_stage" /TN "ace_rr_stage" /RU ""


#CPL load
#SCHTASKS /Create /F /SC WEEKLY /D FRI /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/cpl_stage" /TN "cpl_stage" /RU ""


# UBS LEM Test Daily Delivery at 12:00 - Incremental 
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 12:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ubs_test_send_incr " /TN "UBS LEM test daily reports - 1nd run" /RU ""

# UBS LEM Test Daily Delivery at 18:00 - Full Refresh 
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SUN /ST 18:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ubs_test_send_full " /TN "UBS LEM test daily reports - 2nd run" /RU ""

#AAF Base File Creation at 15:45
SCHTASKS /Create /F /SC DAILY /ST 15:45 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/aaf_base_files " /TN "AAF Base Files" /RU ""

# AAF BOFA Test Daily Delivery at 16:00 - Full Refresh and incremental
SCHTASKS /Create /F /SC DAILY /ST 16:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/bofaresolve_test " /TN "Bofa Test Send" /RU ""


# AAF BOFA Vendor Attributes 19:00
SCHTASKS /Create /F /SC Weekly /D SUN /ST 19:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/bofa_extract_vendor_attributes_test " /TN "Bofa Vendor Extract Attributes Test Send" /RU ""

# MIC
SCHTASKS /Create /F /SC DAILY /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ipid_MIC_Mods"  /TN "Alacra MIC Mods" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 14:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ipid_MIC_Deletes"  /TN "Alacra MIC Deletes" /RU ""




# Moodys new API test
#schtasks /create /tn "New Moodys API testing" /sc hourly /st 00:05:00 /tr "loadjob c:/users/xls/src/scripts/jobstreams/moodys-new-api-test"

# nomura Files OBI stage
#SCHTASKS /Create /F /SC Weekly /D SUN,MON,TUE,WED,THU /ST 18:00 /TR "loadjob c:/users/xls/src/scripts/jobstreams/nomuraFilesLongStage"  /TN "nomuraFilesLongStage" /RU ""


# Run Factiva Select feed name matching table creation
SCHTASKS /Create /F /SC DAILY /ST 00:40 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/factiva_ace_monitoringTest" /TN "factiva_ace_monitoringTest" /RU ""


# "WF Factiva Daily Load
#SCHTASKS /Create /F /SC DAILY /ST 01:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wellsfargo_factiva_loadTest"  /TN "wellsfargofactivatest" /RU ""

# "WF4210 Daily Load 
#SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 21:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wells4210_load_test"  /TN "Alacra Wellsfinra 4210 Load" /RU ""

# "WF4210 Daily Update 
#SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 03:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wells4210_update_test"  /TN "Alacra Wellsfinra 4210 Update" /RU ""

# "WF4210 Daily Send
#SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 08:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wells4210_send_test"  /TN "Alacra Wellsfinra 4210 Send" /RU ""


#SCHTASKS /Create /F /SC DAILY /ST 01:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wellsfargo_factiva_loadStage"  /TN "wellsfargofactivastage" /RU ""

# Wells Fargo Monitoring Summary ACK XML
SCHTASKS /Create /F /SC DAILY /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wellsfargo_monitoring_summary_ack_report_test" /TN "wellsfargo_monitoring_summary_ack_report_test" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 04:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wellsfargo_monitoring_summary_ack_report_test" /TN "wellsfargo_monitoring_summary_ack_report_test" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 15:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wellsfargo_monitoring_summary_ack_report_test" /TN "wellsfargo_monitoring_summary_ack_report_test" /RU ""

# Concordance ACE aaf events
SCHTASKS /Create /F /SC DAILY /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/concordance_ace_aaf_eventsTest" /TN "concordance_ace_aaf_eventsTest" /RU ""

# Extract data from ACE and put it in concordance
#SCHTASKS /Create /F /SC DAILY /ST 12:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ace_extractToConcordanceTest" /TN "ace_extractToConcordanceTest" /RU ""

#C6 Weekly Full Refresh Sundays Sundays Only
SCHTASKS /Create /F /SC Weekly /D SUN /ST 14:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/c6weeklytest" /TN "Alacra C6FSundaysOnlytest" /RU ""

#C6 Daily Incremental Update 
SCHTASKS /Create /F /SC Weekly /D TUE,WED,THU,FRI,SAT,SUN /ST 02:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/c6dailytest" /TN "Alacra C6IDailytest" /RU ""



#KYC FILE IMPORT
SCHTASKS /Create /F /SC Weekly /D SUN /ST 18:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/kyc_test"  /TN "load KYC Data" /RU ""

# AOD invoice accounts invoice mailing
SCHTASKS /Create /F /SC DAILY /st 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/aod_invoice_account_mail_test" /TN "AOD invioce accounts invoice mailing (TEST)" /RU ""

# Alacra enhanced monitoring emails in test
SCHTASKS /Create /F /SC DAILY /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ace_inbox_monitoring_emails_test" /TN "ACE Enhanced Monitoring Emails in test" /RU ""

# Alacra enhanced monitoring emails in stage
SCHTASKS /Create /F /SC DAILY /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ace_inbox_monitoring_emails_stage" /TN "ACE Enhanced Monitoring Emails in stage" /RU ""

# update Prime database
#SCHTASKS /Create /F /SC DAILY /ST 03:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/primeTest" /TN "prime" /RU ""

# send weekly STAGE sales reports
SCHTASKS /Create /F /SC Weekly /D FRI /ST 03:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/salesreportsSTAGE" /TN "salesreports STAGE" /RU ""

# DISABLED - There is only 1 subscription list, and it must come from PROD
# ANZ_CHARM - send subscription list to BvD. TEST instance for now.
# SCHTASKS /Create /F /SC WEEKLY /D TUE /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ANZ_CHARM_BVD_Subscription_List_TEST" /TN "Alacra ANZ_CHARM BvD Subscription List TEST" /RU ""

# ANZ_CHARM - wait for BvD files to be produced, then fetch them.
# TEST instance.
SCHTASKS /Create /F /SC WEEKLY /D WED /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ANZ_CHARM_BVD_Wait_And_Fetch_TEST" /TN "Alacra ANZ_CHARM await and fetch BVD files TEST" /RU ""

# ANZ_CHARM - wait for BvD files to be produced, then fetch them.
# STAGE instance.
SCHTASKS /Create /F /SC WEEKLY /D WED /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ANZ_CHARM_BVD_Wait_And_Fetch_STAGE" /TN "Alacra ANZ_CHARM await and fetch BVD files STAGE" /RU ""

# SQL Unit Tests - execute and distribute results
SCHTASKS /Create /F /SC DAILY /ST 01:45 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/sqlunittests" /TN "Alacra SQL Server unit tests" /RU ""