set transaction isolation level read uncommitted

DECLARE @month datetime
select @month='December 2008'

select  u1.userid, p.company, u1.description, u1.price, u1.access_time, u1.usageid from usage u1, usage u2, users p
where
p.id = u1.userid
and
p.id = u2.userid
and
u1.access_time >= @month and u1.access_time < DATEADD(month,1,@month)
and
u2.access_time >= @month and u2.access_time < DATEADD(month,1,@month)
and
u1.userid = u2.userid
and
u1.description = u2.description
and
u1.list_price = u2.list_price
and
DATEDIFF (second, u1.access_time, u2.access_time) < 60
and
DATEDIFF (second, u1.access_time, u2.access_time) > -60
and
u1.usageid <> u2.usageid
and
u1.price >= 1.00
and
u2.price >= 1.00
and
(u1.project is NULL or u1.project=u2.project)
and
u1.no_charge_flag is NULL
and
u2.no_charge_flag is NULL
and
p.demo_flag is NULL

UNION

select  u1.userid, p.company, u1.description, u1.price, u1.access_time, u1.usageid from usage u1, usage u2, users p
where
p.id = u1.userid
and
p.id = u2.userid
and
u1.access_time >= @month and u1.access_time < DATEADD(month,1,@month)
and
u2.access_time >= @month and u2.access_time < DATEADD(month,1,@month)
and
u1.userid = u2.userid
and
u1.description = u2.description
and
u1.list_price = u2.list_price
and
DATEDIFF (second, u1.access_time, u2.access_time) < 86400
and
DATEDIFF (second, u1.access_time, u2.access_time) > -86400
and
u1.usageid <> u2.usageid
and
u1.price >= 1.00
and
u2.price >= 1.00
and
(u1.project is NULL or u1.project=u2.project)
and
u1.no_charge_flag is NULL
and
u2.no_charge_flag is NULL
and
p.demo_flag is NULL
and
p.id in (select id from users where account in (2791,3246))

order by u1.userid, p.company, u1.description, u1.access_time asc

