set transaction isolation level read uncommitted
set nocount on

DECLARE @month datetime
select @month='12/1/2010'
select
	distinct 
	i.id 'Database', 
	i.name 'Database Name', 

	case
	  when a.name like '%Merrill Lynch%' and u.project4 in ('88Q085', '88Q824', '736HZ8') 
	  then 5745
	  else a.id
	end 'Account Id',

	case
	  when a.name like '%Merrill Lynch%' and u.project4 in ('88Q085', '88Q824', '736HZ8') 
	  then 'Merrill Lynch India'
	  else a.name
	end 'Account Name',

	p.login 'Login',
	REPLACE(COALESCE(u.project,''),'|','^') 'Project',
	REPLACE(COALESCE(u.project2,''),'|','^') 'Project 2',
	REPLACE(COALESCE(u.project3,''),'|','^') 'Project 3',
	REPLACE(COALESCE(u.project4,''),'|','^') 'Project 4',
	REPLACE(COALESCE(u.project5,''),'|','^') 'Project 5',

	CONVERT(VARCHAR(8),u.access_time,1)+' '+CONVERT(VARCHAR(8),u.access_time,8) 'Access Time',
	REPLACE(u.description,'|','^') 'Content Description',
	u.list_price 'List Price',
	u.price 'Alacra Billed Price',
	'' 'Opt1', /* Required by Thomson Reuters for backwards compatibility */
	'' 'Opt2', /* Required by Thomson Reuters for backwards compatibility */
	'' 'Opt3', /* Required by Thomson Reuters for backwards compatibility */
    '' 'Opt4', /* Required by Thomson Reuters for backwards compatibility */

	case
	   when i.ipcode in ('ITEXT','MARKI','ITPPV')
	   then
			case
			   when CHARINDEX('|',s.ipinfo) > 0
			   then
				SUBSTRING(s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1)+1)+1, 
			      CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1)+1)+1) - CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1)+1) - 1)
			   else
				''
			   end   
	   else
			''
	   end 'Source Code',
	case
	   when i.ipcode in ('ITEXT','MARKI','ITPPV')
	   then
			case 
			   when CHARINDEX('|',s.ipinfo) > 0
			   then
				SUBSTRING(s.ipinfo,1,CHARINDEX('|',s.ipinfo)-1) 
			   else
				''
			   end
	   else
			''
	   end 'Report Number',
	case
	   when i.ipcode in ('ITEXT','MARKI','ITPPV')
	   then
			case
			   when CHARINDEX('|',s.ipinfo) > 0
			   then
				SUBSTRING(s.ipinfo, CHARINDEX('|',s.ipinfo)+1, 
			       CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1) - CHARINDEX('|',s.ipinfo) - 1)
			   else
				''
			   end   
	   else
			''
	   end 'Pages Sold',


	CONVERT(MONEY,u.price * i.royalty_rate) 'Invoice Revenue', /* Alacra Billed Price * Split */
	CONVERT(VARCHAR(8),u.access_time,1)+' '+CONVERT(VARCHAR(8),u.access_time,8) 'Access Time', /* Duplicated for backwards compatibility at Thomson Reuters */
	'USD' 'Currency Code', /* Alacra always reports in U.S. Dollars */

	COALESCE(a.address1,'') 'Company Address 1',
	COALESCE(a.address2,'') 'Company Address 2',
	COALESCE(a.address3,'') 'Company Address 3',
	COALESCE(a.city,'') 'City',
	COALESCE(sp.abbreviation,'') 'State/Prov', /* Abbreviation */
	COALESCE(c.name,'') 'Country',
	COALESCE(a.postal_code,'') 'Company Zip Code',
	COALESCE(a.contact,'') 'Company Contact Name',
	COALESCE(a.email,'') 'Company Contact Email',
	a.tel_country+' '+a.tel_area+' '+a.tel_number 'Company Contact Telephone',
	p.first_name+' '+p.last_name 'User Name',
	p.tel_country+' '+p.tel_area+' '+p.tel_number 'User Telephone',
	COALESCE(p.email,'') 'User E-mail',
	COALESCE(p.department,'') 'User Department',
	sv.name 'Platform', /* Service - Premium, Book, etc */
	'' 'User Status (contracted/trial)', /* T for trial - not used now */
	CONVERT(VARCHAR(8),p.commence,1)+' '+CONVERT(VARCHAR(8),p.commence,8) 'Start Date',
	u.usageid 'PK',
	CASE WHEN a.id = 6915 THEN 'Y' ELSE 'N' END 'ePib'
from 
	usage u JOIN users p ON u.userid = p.id LEFT JOIN usagespecial s
	ON u.usageid = s.usage JOIN ip i ON u.ip = i.id
	JOIN account a ON p.account = a.id JOIN service sv ON sv.id = p.service
	LEFT JOIN state_prov sp ON a.state_prov = sp.id LEFT JOIN country c ON
	a.country = c.id
where 
	u.ip in 	(
		select 
			id 
		from 
			ip 
		where 
			ipcode in (
'MARKI',
'ITEXT',
'ITPPV')
		)
	and 
	u.no_charge_flag is null
	and
	u.access_time >= @month and u.access_time < DATEADD(month,1,@month)
	and
	p.demo_flag is NULL
UNION

select 
	distinct
	i.id 'Database', 
	i.name 'Database Name', 

	case
	  when a.name like '%Merrill Lynch%' and u.project4 in ('88Q085', '88Q824', '736HZ8') 
	  then 5745
	  else a.id
	end 'Account Id',

	case
	  when a.name like '%Merrill Lynch%' and u.project4 in ('88Q085', '88Q824', '736HZ8') 
	  then 'Merrill Lynch India'
	  else a.name
	end 'Account Name',

	p.login 'Login',
	REPLACE(COALESCE(u.project,''),'|','^') 'Project',
	REPLACE(COALESCE(u.project2,''),'|','^') 'Project 2',
	REPLACE(COALESCE(u.project3,''),'|','^') 'Project 3',
	REPLACE(COALESCE(u.project4,''),'|','^') 'Project 4',
	REPLACE(COALESCE(u.project5,''),'|','^') 'Project 5',

	CONVERT(VARCHAR(8),u.access_time,1)+' '+CONVERT(VARCHAR(8),u.access_time,8) 'Access Time',
	REPLACE(u.description,'|','^') 'Content Description',
	u.list_price 'List Price',
	u.price 'Alacra Billed Price',
	'' 'Opt1', /* Required by Thomson Reuters for backwards compatibility */
	'' 'Opt2', /* Required by Thomson Reuters for backwards compatibility */
	'' 'Opt3', /* Required by Thomson Reuters for backwards compatibility */
    '' 'Opt4', /* Required by Thomson Reuters for backwards compatibility */

	'' 'Source Code', /* NA for these databases */
	'' 'Report Number', /* NA for these databases */
	'' 'Pages Sold', /* NA for these databases */

	CONVERT(MONEY,u.price * i.royalty_rate) 'Invoice Revenue', /* Alacra Billed Price * Split */
	CONVERT(VARCHAR(8),u.access_time,1)+' '+CONVERT(VARCHAR(8),u.access_time,8) 'Access Time', /* Duplicated for backwards compatibility at Thomson Reuters */
	'USD' 'Currency Code', /* Alacra always reports in U.S. Dollars */

	COALESCE(a.address1,'') 'Company Address 1',
	COALESCE(a.address2,'') 'Company Address 2',
	COALESCE(a.address3,'') 'Company Address 3',
	COALESCE(a.city,'') 'City',
	COALESCE(sp.abbreviation,'') 'State/Prov', /* Abbreviation */
	COALESCE(c.name,'') 'Country',
	COALESCE(a.postal_code,'') 'Company Zip Code',
	COALESCE(a.contact,'') 'Company Contact Name',
	COALESCE(a.email,'') 'Company Contact Email',
	a.tel_country+' '+a.tel_area+' '+a.tel_number 'Company Contact Telephone',
	p.first_name+' '+p.last_name 'User Name',
	p.tel_country+' '+p.tel_area+' '+p.tel_number 'User Telephone',
	COALESCE(p.email,'') 'User E-mail',
	COALESCE(p.department,'') 'User Department',
	sv.name 'Platform', /* Service - Premium, Book, etc */
	'' 'User Status (contracted/trial)', /* T for trial - not used now */
	CONVERT(VARCHAR(8),p.commence,1)+' '+CONVERT(VARCHAR(8),p.commence,8) 'Start Date',
	u.usageid 'PK',
	CASE WHEN a.id = 6915 THEN 'Y' ELSE 'N' END 'ePib'
from 
	usage u JOIN users p ON u.userid = p.id JOIN ip i ON u.ip = i.id
	JOIN account a ON p.account = a.id JOIN service sv ON sv.id = p.service
	LEFT JOIN state_prov sp ON a.state_prov = sp.id LEFT JOIN country c ON
	a.country = c.id
where 
	u.ip in 	(
		select 
			id 
		from 
			ip 
		where 

			ipcode in (
'CAPA',
'CCBN',
'CDAW',
'DGA',
'CITYW',
'DSTRM',
'EXCARD',
'EXTEL',
'FIRSTC',
'FCRSH', 
'IBES',
'INSID',
'MGE',
'MULFUN',
'MXAPI',
'NI',
'RTRFIL',
'SDC', 
'SDCJV',
'SDCVC',
'TECH',
'TED',
'TFSHRE',
'WORLDS',
'538', '539', '540'
)
		)
	and 
	u.no_charge_flag is null
	and 
	u.access_time >= @month and u.access_time < DATEADD(month,1,@month)
	and
	p.demo_flag is NULL
