set transaction isolation level read uncommitted

DECLARE @periodStart datetime
DECLARE @periodEnd datetime
select @periodStart='April 2013'
select @periodEnd=DATEADD(month,1,@periodStart)

select  distinct u1.userid, p.company, u1.description, u1.price, u1.access_time, u1.usageid, us1.ipinfo, u2.access_time 'Duplicate Time', DATEDIFF(minute, u1.access_time, u2.access_time) 'Time Difference (min)', u2.usageid 'Duplicate ID', us2.ipinfo 'Duplicate Ipinfo'
from
usage u1 left outer join usagespecial us1 on (u1.usageid = us1.usage) join users p on (u1.userid=p.id), 
usage u2 left outer join usagespecial us2 on (u2.usageid = us2.usage) 
where
u1.userid = u2.userid
and
u1.access_time >= @periodStart and u1.access_time < @periodEnd
and
u2.access_time >= @periodStart and u2.access_time < @periodEnd
and
u1.description = u2.description
and
u1.list_price = u2.list_price
and
DATEDIFF (second, u1.access_time, u2.access_time) < 60
and
DATEDIFF (second, u1.access_time, u2.access_time) > -60
and
u1.price > 1.00
and
u2.price > 1.00
and
(((u1.project is NULL) and (u2.project is NULL)) or (u1.project=u2.project))
and
(((u1.project2 is NULL) and (u2.project2 is NULL)) or (u1.project2=u2.project2))
and
(((u1.project3 is NULL) and (u2.project3 is NULL)) or (u1.project3=u2.project3))
and
(((u1.project4 is NULL) and (u2.project4 is NULL)) or (u1.project4=u2.project4))
and
(((u1.project5 is NULL) and (u2.project5 is NULL)) or (u1.project5=u2.project5))
and
u1.no_charge_flag is NULL
and
u2.no_charge_flag is NULL
and
p.demo_flag is NULL
and
(((us1.ipinfo is NULL) and (us2.ipinfo is NULL)) or (us1.ipinfo = us2.ipinfo))
and
u1.usageid < u2.usageid
and p.account not in (4163, 7126,6935)

UNION

/* BCG has a broader time range and subscription usage is de-duped as well */
select  distinct u1.userid, p.company, u1.description, u1.price, u1.access_time, u1.usageid, us1.ipinfo, u2.access_time 'Duplicate Time', DATEDIFF(minute, u1.access_time, u2.access_time) 'Time Difference (min)', u2.usageid 'Duplicate ID', us2.ipinfo 'Duplicate Ipinfo'
from 
usage u1 left outer join usagespecial us1 on (u1.usageid = us1.usage) join users p on (u1.userid=p.id), 
usage u2 left outer join usagespecial us2 on (u2.usageid = us2.usage)
where
u1.userid = u2.userid
and
u1.access_time >= @periodStart and u1.access_time < @periodEnd
and
u2.access_time >= @periodStart and u2.access_time < @periodEnd
and
u1.description = u2.description
and
u1.list_price = u2.list_price
and
DATEDIFF (second, u1.access_time, u2.access_time) < 86400
and
DATEDIFF (second, u1.access_time, u2.access_time) > -86400
and
(((u1.project is NULL) and (u2.project is NULL)) or (u1.project=u2.project))
and
(((u1.project2 is NULL) and (u2.project2 is NULL)) or (u1.project2=u2.project2))
and
(((u1.project3 is NULL) and (u2.project3 is NULL)) or (u1.project3=u2.project3))
and
(((u1.project4 is NULL) and (u2.project4 is NULL)) or (u1.project4=u2.project4))
and
(((u1.project5 is NULL) and (u2.project5 is NULL)) or (u1.project5=u2.project5))
and
u1.no_charge_flag is NULL
and
u2.no_charge_flag is NULL
and
p.demo_flag is NULL
and
(((us1.ipinfo is NULL) and (us2.ipinfo is NULL)) or (us1.ipinfo = us2.ipinfo))
and
u1.usageid < u2.usageid
and
p.id in (select id from users where account in (6788,6789,6793,6791,6792, 6811, 6744,7442, 6832, 7333, 7356))

order by 
u1.price desc, u1.userid, p.company, u1.description, us1.ipinfo asc, u1.usageid asc

select count(*) 'Duplicate Downloads', sum(price) 'Total Alacra Price' from usage where usageid in (
select  u2.usageid 
from
usage u1 left outer join usagespecial us1 on (u1.usageid = us1.usage) join users p on (u1.userid=p.id), 
usage u2 left outer join usagespecial us2 on (u2.usageid = us2.usage) 
where
u1.userid = u2.userid
and
u1.access_time >= @periodStart and u1.access_time < @periodEnd
and
u2.access_time >= @periodStart and u2.access_time < @periodEnd
and
u1.description = u2.description
and
u1.list_price = u2.list_price
and
DATEDIFF (second, u1.access_time, u2.access_time) < 60
and
DATEDIFF (second, u1.access_time, u2.access_time) > -60
and
u1.price > 1.00
and
u2.price > 1.00
and
(((u1.project is NULL) and (u2.project is NULL)) or (u1.project=u2.project))
and
(((u1.project2 is NULL) and (u2.project2 is NULL)) or (u1.project2=u2.project2))
and
(((u1.project3 is NULL) and (u2.project3 is NULL)) or (u1.project3=u2.project3))
and
(((u1.project4 is NULL) and (u2.project4 is NULL)) or (u1.project4=u2.project4))
and
(((u1.project5 is NULL) and (u2.project5 is NULL)) or (u1.project5=u2.project5))
and
u1.no_charge_flag is NULL
and
u2.no_charge_flag is NULL
and
p.demo_flag is NULL
and
(((us1.ipinfo is NULL) and (us2.ipinfo is NULL)) or (us1.ipinfo = us2.ipinfo))
and
u1.usageid < u2.usageid
and p.account not in (4163, 7126,6935)

UNION

/* BCG has a broader time range and subscription usage is de-duped as well */
select  u2.usageid 
from 
usage u1 left outer join usagespecial us1 on (u1.usageid = us1.usage) join users p on (u1.userid=p.id), 
usage u2 left outer join usagespecial us2 on (u2.usageid = us2.usage)
where
u1.userid = u2.userid
and
u1.access_time >= @periodStart and u1.access_time < @periodEnd
and
u2.access_time >= @periodStart and u2.access_time < @periodEnd
and
u1.description = u2.description
and
u1.list_price = u2.list_price
and
DATEDIFF (second, u1.access_time, u2.access_time) < 86400
and
DATEDIFF (second, u1.access_time, u2.access_time) > -86400
and
(((u1.project is NULL) and (u2.project is NULL)) or (u1.project=u2.project))
and
(((u1.project2 is NULL) and (u2.project2 is NULL)) or (u1.project2=u2.project2))
and
(((u1.project3 is NULL) and (u2.project3 is NULL)) or (u1.project3=u2.project3))
and
(((u1.project4 is NULL) and (u2.project4 is NULL)) or (u1.project4=u2.project4))
and
(((u1.project5 is NULL) and (u2.project5 is NULL)) or (u1.project5=u2.project5))
and
u1.no_charge_flag is NULL
and
u2.no_charge_flag is NULL
and
p.demo_flag is NULL
and
(((us1.ipinfo is NULL) and (us2.ipinfo is NULL)) or (us1.ipinfo = us2.ipinfo))
and
u1.usageid < u2.usageid
and
p.id in (select id from users where account in (6788,6789,6793,6791,6792, 6811, 6744,7442, 6832, 7333, 7356))
)






