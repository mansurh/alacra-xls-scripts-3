# Split the command line arguments.
#	1 = month 
#	2 = year
#	3 = server
#	4 = user
#	5 = password
set arglist [split $argv]
if {[llength $arglist] != 5} {
	puts "Usage: non_billable_accounts.tcl month year server user password"
	exit
}
set month [lindex $arglist 0]
set year [lindex $arglist 1]
set server [lindex $arglist 2]
set user [lindex $arglist 3]
set password [lindex $arglist 4]

# Open the connection to SQL Server
set handle [sybconnect $user $password $server]

# Select the non-active accounts
sybsql $handle "select id,name from account where id not in (select a.id from account a, charge c where c.account = a.id and c.start_date < DATEADD (month, 1, \"$month 1, $year\") and c.end_date >= \"$month 1, $year\") order by id"

puts "Non Billable accounts for $month $year"
sybnext $handle {
	puts " @2 (@1)"
}

# Select the valid users doing downloads against inactive accounts
sybsql $handle "select distinct p.id from users p, usage u where u.userid = p.id and u.access_time >= \"$month 1, $year\" and u.access_time <= DATEADD(month,1,\"$month 1, $year\") and p.demo_flag is null and (p.service is null or p.service=0) and p.account in (select id from account where id not in (select a.id from account a, charge c where c.account = a.id and c.start_date < DATEADD (month, 1, \"$month 1, $year\") and c.end_date >= \"$month 1, $year\") )"

puts "!!Usage for userids not billed for $month $year!!"
sybnext $handle {
	puts "@1"
}

# Close the connection to SQL Server
sybclose $handle
