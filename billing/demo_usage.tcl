# Split the command line arguments.
#	1 = month 
#	2 = year
#	3 = server
#	4 = user
#	5 = password
set arglist [split $argv]
if {[llength $arglist] != 5} {
	puts "Usage: demo_usage.tcl month year server user password"
	exit
}
set month [lindex $arglist 0]
set year [lindex $arglist 1]
set server [lindex $arglist 2]
set user [lindex $arglist 3]
set password [lindex $arglist 4]

# Open the connection to SQL Server
set handle [sybconnect $user $password $server]

# Select the active accounts
sybsql $handle "select u.usageid, p.account, COALESCE(a.name,p.company) \"Company\", u.userid, p.login, p.last_name, u.description, u.price, u.access_time, u.project, u.remote_address, i.name from usage u JOIN users p ON u.userid = p.id and u.access_time >= p.commence JOIN ip i ON u.ip = i.id LEFT JOIN account a ON p.account = a.id where u.access_time >= \"$month 1, $year\" and u.access_time < DATEADD(month, 1, \"$month 1, $year\") and p.demo_flag is not null and (p.service is null or p.service = 0) order by p.account, u.userid, u.access_time "

puts "Demo usage by account, $month, $year"
puts "Usage ID|Account|Company|UserID|Login|Last Name|Description|Price|Access Time|Project|Address|Content Provider"
sybnext $handle {
	puts "@1|@2|[join @3 " "]|@4|@5|@6|[join @7 " "]|@8|[join @9 " "]|[join @10 " "]|@11|[join @12 " "]"
}

# Close the connection to SQL Server
sybclose $handle
