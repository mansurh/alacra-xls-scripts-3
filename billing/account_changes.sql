declare @enddate smalldatetime

/* Move to beginning of the month */
select @startdate = DATEADD(day, -1 * (DATEPART(day, @startdate) -1), @startdate)
select @startdate = DATEADD(hour, -1 * DATEPART(hour, @startdate), @startdate)
select @startdate = DATEADD(minute, -1 * DATEPART(minute, @startdate), @startdate)

/* Calculate the end of the monthly period */
select @enddate = DATEADD (month, 1, @startdate)

select "Month Starting", @startdate

print "Charges starting in month"
print "========================="
select 
	a.id "Id",
	CONVERT (varchar(32),a.name) "Account", 
	CONVERT (varchar(40),b.name) "Billed", 
	CONVERT (varchar(12),c.start_date,1) "Start",
	CONVERT (varchar(12),c.end_date,1) "End",
	CONVERT (varchar(10),c.amount) "Amount",
	CONVERT (varchar(60),c.description) "Description",

	CONVERT (varchar(16),s.last_name) "DDL Rep",
	CONVERT (varchar(16),s2.last_name) "Outside Rep",
	CONVERT (varchar(16),s3.last_name) "Account Manager",
	CONVERT (varchar(16),c.currency) "Currency"

from 
	charge c INNER JOIN account a ON c.account = a.id
	INNER JOIN billable_type b ON c.how_billed = b.id
	LEFT JOIN salesperson s ON c.ddl_salesperson = s.id
	LEFT JOIN salesperson s2 ON c.outside_salesperson = s2.id
	LEFT JOIN salesperson s3 ON a.account_manager = s3.id
where 
	c.start_date >= @startdate and c.start_date < @enddate
order by
	a.name

print "Charges ending in month"
print "======================="
select 
	a.id "Id",
	CONVERT (varchar(32),a.name) "Account", 
	CONVERT (varchar(40),b.name) "Billed", 
	CONVERT (varchar(12),c.start_date,1) "Start",
	CONVERT (varchar(12),c.end_date,1) "End",
	CONVERT (varchar(10),c.amount) "Amount",
	CONVERT (varchar(60),c.description) "Description",

	CONVERT (varchar(16),s.last_name) "DDL Rep",
	CONVERT (varchar(16),s2.last_name) "Outside Rep",
	CONVERT (varchar(16),s3.last_name) "Account Manager",
	CONVERT (varchar(16),c.currency) "Currency"

from 
	charge c INNER JOIN account a ON c.account = a.id
	INNER JOIN billable_type b ON c.how_billed = b.id
	LEFT JOIN salesperson s ON c.ddl_salesperson = s.id
	LEFT JOIN salesperson s2 ON c.outside_salesperson = s2.id
	LEFT JOIN salesperson s3 ON a.account_manager = s3.id
where 
	(c.end_date >= @startdate and c.end_date < @enddate)
	and 
	c.start_date < @startdate
order by
	a.name


print "Accounts that appear to have canceled"
print "====================================="

select 
	a.id, CONVERT (varchar(32),a.name) "Account", max(c.end_date) "Cancel As Of"

from
	charge c, account a
where
	c.account = a.id
group by
	a.id, a.name
having
	max(c.end_date) >= @startdate
	and
	max(c.end_date) < @enddate

