/* Send monthly to Laura McHugh at LauraM@perfectinfo.com */
select 
	i.name 'Database Name', 
	a.id 'Account Id', 
	a.name 'Account Name',
	a.city 'Account City',
	c.name 'Account Country',
 	p.login 'Login',
	u.access_time 'Access Time',
	u.description 'Content Description',
	u.price 'Price',
	u.list_price 'List Price',
	s.ipinfo 'Docid|Report Type|Pages '
from 
	usage u JOIN users p ON u.userid=p.id LEFT JOIN usagespecial s ON
	u.usageid = s.usage JOIN ip i ON u.ip = i.id JOIN account a ON
	p.account = a.id JOIN country c ON c.id = a.country
where 
	ip in 	(120, 303)
	and 
	no_charge_flag is null
	and 
	access_time >= 'January 1, 2008' and access_time < 'February 1, 2008'
	and
	p.demo_flag is NULL
order by 
	i.id, a.id, p.login, u.access_time asc
