set transaction isolation level read uncommitted
set nocount on

DECLARE @month datetime
/* select @month='$(month)/1/$(year)' */
select @month='December 1, 2008'

select 

 

	case
	  when a.name like '%Merrill Lynch%' and u.project4 in ('88Q085', '88Q824', '736HZ8') 
	  then 5745
	  else a.id
	end 'Account_Id_Number',

	case
	  when a.name like '%Merrill Lynch%' and u.project4 in ('88Q085', '88Q824', '736HZ8') 
	  then 'Merrill Lynch India'
	  else a.name
	end 'Client_Name',


	COALESCE(a.address1,'')+' '+COALESCE(a.address2,'')+' '+COALESCE(a.address3,'') 'Address',
	COALESCE(a.city,'') 'City',
	COALESCE(sp.abbreviation,'') 'State', /* Abbreviation */
	COALESCE(a.postal_code,'') 'Zip Code',
	COALESCE(c.name,'') 'Country',
	COALESCE(a.contact,'') 'Contact_Name',
	COALESCE(a.email,'') 'Contact_Email',
	a.tel_country+' '+a.tel_area+' '+a.tel_number 'Contact_Phone',
	
	
	/* CASE WHEN a.id = 6915 THEN 'Y' ELSE 'N' END 'ePib', */
	
	'' 'Total_Trial_Users',
		
count(	distinct CASE 
		WHEN f.value='${project}'
		THEN u.project
		WHEN f.value='${project2}'
		THEN u.project2
		WHEN f.value='${project3}'
		THEN u.project3
		WHEN f.value='${project4}'
		THEN u.project4
		WHEN f.value='${project5}'
		THEN u.project5
		ELSE CONVERT(varchar(255),u.userid)
		END) 'Total_Contracted_Users',
		
			i.name 'Content Service Level 1',
			
		CASE 
		WHEN i.id=52
		THEN CASE
				WHEN u.description like '%Detail Historical%'
				THEN 'Reuters Estimates - Historical Detail'
				WHEN u.description like '%Consensus Historical%'
				THEN 'Reuters Estimates - Historical Consensus'
				WHEN u.description like '%Consensus Report%'
				THEN 'Reuters Estimates - Current Consensus'
				WHEN u.description like '%Broker Recommendation%'
				THEN 'Reuters Estimates - Current Detail'
				WHEN u.description like 'Reuters Estimates - Detail report%' /* old version */
				THEN 'Reuters Estimates - Current Detail'
				ELSE 'Reuters Estimates - Unclassified Content Type'
			 END
		WHEN i.id=29
		THEN CASE
				WHEN u.description like 'I/B/E/S Consensus%'
				THEN 'IBES Current Consensus Estimates'
				WHEN u.description like 'I/B/E/S Detail%'
				THEN 'IBES Current Detail Estimates'
				WHEN u.description like 'I/B/E/S Aggregate History%'
				THEN 'IBES Historical Aggregates'
				WHEN u.description like 'I/B/E/S Aggregate Snapshot%'
				THEN 'IBES Current Aggregates'
				WHEN u.description like 'ibes:%Keys%'
				THEN 'IBES Current Consensus Estimates'
				WHEN u.description like 'I/B/E/S Historical Summary%'
				THEN 'IBES Historical Consensus Estimates'
				ELSE 'IBES Unclassified Content Type'
		END
		ELSE i.name
	END 'Contract Service Level 2', 		
			
			
			
	i.id 'Database Number',
	
	CASE WHEN sv.name='Alacra'
	     THEN 'Alacra Premium'
	     ELSE sv.name
	END 'Delivery_Platform', /* Service - Premium, Book, etc */	
	
	@month 'Rpt_Start_Date',
	DATEADD(day, -1, DATEADD(month,1,@month)) 'Rpt_End_Date',

	

	
	CASE 
		WHEN f.value='${project}'
		THEN 'Yes'
		WHEN f.value='${project2}'
		THEN 'Yes'
		WHEN f.value='${project3}'
		THEN 'Yes'
		WHEN f.value='${project4}'
		THEN 'Yes'
		WHEN f.value='${project5}'
		THEN 'Yes'
		ELSE 'No'
		END 'Lightweight IDs',
		
		'' 'Comments'
	


from 
	usage u JOIN users p ON u.userid = p.id JOIN ip i ON u.ip = i.id
	JOIN account a ON p.account = a.id JOIN service sv ON sv.id = p.service
	LEFT JOIN state_prov sp ON a.state_prov = sp.id LEFT JOIN country c ON
	a.country = c.id LEFT JOIN accounts_flag f ON a.id = f.accountId
where 
	u.ip in 	(
		select 
			id 
		from 
			ip 
		where 

			ipcode in (
			
'MARKI',
'ITEXT',
'ITPPV',			
			
'CAPA',
'CCBN',
'CDAW',
'DGA',
'CITYW',
'DSTRM',
'EXCARD',
'EXTEL',
'FIRSTC',
'FCRSH', 
'IBES',
'INSID',
'MGE',
'MULFUN',
'MXAPI',
'NI',
'RTRFIL',
'SDC', 
'SDCJV',
'SDCVC',
'TECH',
'TED',
'TFSHRE',
'WORLDS'
)
		)
	and 
	u.no_charge_flag is null
	and 
	u.access_time >= @month and u.access_time < DATEADD(month,1,@month)
	and
	p.demo_flag is NULL
	and
	f.name='LightweightUserId'
	and
	u.price < 0.01
	and
	a.id <> 6915 /* Specifically exclude ePib usage */

group by
	
	i.name,
	i.id,

	CASE 
		WHEN i.id=52
		THEN CASE
				WHEN u.description like '%Detail Historical%'
				THEN 'Reuters Estimates - Historical Detail'
				WHEN u.description like '%Consensus Historical%'
				THEN 'Reuters Estimates - Historical Consensus'
				WHEN u.description like '%Consensus Report%'
				THEN 'Reuters Estimates - Current Consensus'
				WHEN u.description like '%Broker Recommendation%'
				THEN 'Reuters Estimates - Current Detail'
				WHEN u.description like 'Reuters Estimates - Detail report%'
				THEN 'Reuters Estimates - Current Detail'
				ELSE 'Reuters Estimates - Unclassified Content Type'
			 END
		WHEN i.id=29
		THEN CASE
				WHEN u.description like 'I/B/E/S Consensus%'
				THEN 'IBES Current Consensus Estimates'
				WHEN u.description like 'I/B/E/S Detail%'
				THEN 'IBES Current Detail Estimates'
				WHEN u.description like 'I/B/E/S Aggregate History%'
				THEN 'IBES Historical Aggregates'
				WHEN u.description like 'I/B/E/S Aggregate Snapshot%'
				THEN 'IBES Current Aggregates'
				WHEN u.description like 'ibes:%Keys%'
				THEN 'IBES Current Consensus Estimates'
				WHEN u.description like 'I/B/E/S Historical Summary%'
				THEN 'IBES Historical Consensus Estimates'
				ELSE 'IBES Unclassified Content Type'
		END
		ELSE i.name
	END, 

	case
	  when a.name like '%Merrill Lynch%' and u.project4 in ('88Q085', '88Q824', '736HZ8') 
	  then 5745
	  else a.id
	end,

	case
	  when a.name like '%Merrill Lynch%' and u.project4 in ('88Q085', '88Q824', '736HZ8') 
	  then 'Merrill Lynch India'
	  else a.name
	end,


	COALESCE(a.address1,'')+' '+COALESCE(a.address2,'')+' '+COALESCE(a.address3,''),
	COALESCE(a.city,''),
	COALESCE(sp.abbreviation,''), /* Abbreviation */
	COALESCE(c.name,''),
	COALESCE(a.postal_code,''),
	COALESCE(a.contact,''),
	COALESCE(a.email,''),
	a.tel_country+' '+a.tel_area+' '+a.tel_number,
	sv.name, /* Service - Premium, Book, etc */
	
	/* CASE WHEN a.id = 6915 THEN 'Y' ELSE 'N' END, */
	
	CASE 
		WHEN f.value='${project}'
		THEN 'Yes'
		WHEN f.value='${project2}'
		THEN 'Yes'
		WHEN f.value='${project3}'
		THEN 'Yes'
		WHEN f.value='${project4}'
		THEN 'Yes'
		WHEN f.value='${project5}'
		THEN 'Yes'
		ELSE 'No'
		END




/* select * from usage where ip=52 and access_time >='January 1, 2009' and access_time < 'February 1, 2009' */