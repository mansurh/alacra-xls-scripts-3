

XLSUTILS=$XLS/src/scripts/loading/dba
loaddir=${XLSDATA}/wellsfargo
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 = database

# Save the runstring parameters in local variables
database=xls
if [ $# -gt 0 ]
then
	database=$1
fi
server=`get_xls_registry_value $database Server`
user=`get_xls_registry_value $database User`
password=`get_xls_registry_value $database Password`

check_files_size()
{		
	printf "\n\nChecking file size ..."
	filecount1=`wc -l ${loaddir}/wellsfargo_usage.csv |cut -f1 -d" "`
	if [ $filecount1 -lt 1 ]
	then
        printf "\nWellsfargo_usage.csv - no usage rows found, exiting ..."
        exit 1
	fi
	printf "\nWellsfargo_usage.csv contains $filecount1 records"
}

main()
{
    rm -f ${loaddir}/*.csv
	filename=${loaddir}/wellsfargo_usage.csv
	query="select replace(project, ',', ' '), replace(description, ',', ';'), access_time from usage where ip=414 and access_time > DATEADD(month, DATEDIFF(month, 0, (GETDATE() - 30)), 0) and access_time < DATEADD(month, DATEDIFF(month, 0, (GETDATE())), 0) and userid in (select id from users where account=7025) order by access_time"
	bcp "$query" queryout $filename /U${user} /P${password} /S${server}  /c /t,
    printf "\nBcp command : $query"     

	if [ $? -ne 0 ]
	then
	  echo "error in getting account list"
	  exit 1
	fi

    check_files_size

    blat -attach wellsfargo_usage.csv -body "Monthly usage attached" -s "Wells Fargo Monthly Account Usage" -t albert.tamayev@alacra.com
    mv ${loaddir}/*.* ${loaddir}/bak
}

# make sure our load directory exist
mkdir -p ${loaddir}
mkdir -p ${loaddir}/log
mkdir -p ${loaddir}/bak
logfile=${loaddir}/log/wellsfargo_usage`date +%y%m`.log

(main) > ${logfile} 2>&1 
if [ $? -ne 0 ]
then
   blat $logfile -t administrators@alacra.com -s "Error running Wells Fargo Monthly Usage Reports"
   exit 1
fi
