use Win32::OLE;
use OLE;
use Date::Manip;

if (@ARGV < 1){
	die "Usage: perl database_usage_1yr.pl <last date + 1>\n";
}

my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!"; 
my $connect="DRIVER={SQL SERVER};PWD=xls;UID=xls;SERVER=data3;DATABASE=xls"; 
$conn->open($connect);

my $conn2=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!"; 
my $connect2="DRIVER={SQL SERVER};PWD=xls;UID=xls;SERVER=data3;DATABASE=xls"; 
$conn2->open($connect2);

my $conn3=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!"; 
my $connect3="DRIVER={SQL SERVER};PWD=xls;UID=xls;SERVER=data3;DATABASE=xls"; 
$conn3->open($connect3);

$main::TZ = "EST5EDT";	#Need this to datemanip
$currentDate = &ParseDate($ARGV[0]);
for ($i=0; $i<=12; $i++){
	$monthsback = " -" . $i . " month";
	$cutoffDate  = &DateCalc($currentDate, $monthsback); 
	#print "${cutoffDate}\n";  
	$daterange[$i] = &UnixDate($cutoffDate, "%m/%d/%Y");
	#print &UnixDate($cutoffDate, "%m/%d/%Y\n");
}

#Start by getting all ip databases
my $sql="Select * from ip order by name";
my $rs=$conn->Execute($sql) || die "Execute Error: $!";
my $fcnt = $rs->Fields->Count;
#Loop thru all ips
while ( !$rs->EOF){
	#Clear each months sum total holder
	for ($i=1; $i<=12; $i++){
		$sumprice[$i] = 0;
		$sumprice2[$i] = 0;
	}
	$ip = $rs->Fields('id')->value;
	print $ip;
	print "|";
	print $rs->Fields('name')->value;

	#Get all the usage for this ip
	for ($i=1; $i<=12; $i++){
		#Get sum(price) for this user ip each month
		my $sql3 = " Select sum(price) as price, sum(list_price) as listprice from usage where access_time <= '$daterange[$i - 1]' and access_time > '$daterange[$i]' and ip = " . $ip;
		my $rs3=$conn3->Execute($sql3) || die "Execute Error(${sql3}): $!";
		while ( !$rs3->EOF ){
			$oneprice = $rs3->Fields('price')->value;
			#print "!${oneprice}!";
			$sumprice[$i] += $oneprice;	
			$oneprice = $rs3->Fields('listprice')->value;
			$sumprice2[$i] += $oneprice;	
			$rs3->MoveNext;
		}
		$rs3->Close;
	}

	#Print the sum for all users of this account for each month
	for ($i=12; $i>=1; $i--){
		print "|";
		print $sumprice[$i];
		print "|";
		print $sumprice2[$i];
	}


	$rs->MoveNext;
	print "\n";
}
$rs->Close;

$conn->Close;
$conn2->Close;
$conn3->Close;




                    