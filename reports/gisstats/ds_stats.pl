
use strict;

my @sep_process_indices = (	
			'ccbn',
			'moodys',
			'eiuftxml',
			'markintel',
		       	'co',
			'newstex',
			'rbi',
			'dnb2',
			'dnb2store',
			'itext',
			'tblbase',
			'nn',
			'bamp',

			'gale',
			'gsight',
			'insider',
			'nikkei',
			  );

use Win32::TieRegistry;
sub getXlsRandomizedRegistryValue {
	my $keyname = shift;
	my $valname = shift;
	$Registry->Delimiter("/");
	my $HKLMKey = $Registry->{"LMachine/"};
	my $RequestedKey = $HKLMKey->{$keyname};
	my @xlist = split /;/, $RequestedKey->{"/$valname"};
	my $x = $xlist[int(rand(scalar(@xlist)))];
	$x;
}
sub getXlsRegistryValue {
	my $keyname = shift;
	my $valname = shift;
	$Registry->Delimiter("/");
	my $HKLMKey = $Registry->{"LMachine/"};
	my $RequestedKey = $HKLMKey->{$keyname};
	my $x = $RequestedKey->{"/$valname"};
	$x;
}

sub readInOrSplit {
	my $val = shift;
	my $sep = shift;
	my @retlist = ();
	if( ! -e $val ) { @retlist = split /$sep/, $val; }
	else {
		open( INPUTFIL, '<', $val );
		while( <INPUTFIL> ) { chomp; push @retlist, $_; }
		close( INPUTFIL );
	}
	@retlist;
}

# Setup input parameters
my $location = getXlsRegistryValue("SOFTWARE/XLS", "ServerLocation");
if( $#ARGV ge 0 ) {
	$location = $ARGV[0];
}
print "Logging GIS stats for $location... ($#ARGV)\n";

my $rerequest_nostats = 0;
if( $#ARGV ge 1 ) {
	$rerequest_nostats = int($ARGV[1]);
}
my @server_filter = ();
if( $#ARGV ge 2 ) {
	if( $ARGV[2] !~ /\%/ ) {
		@server_filter = readInOrSplit($ARGV[2], ",");
	}
}
#print "<!-- serverfilter: ".join(",", @server_filter)."-->\n";
my @regkey_filter = ();
if( $#ARGV gt 3 ) {
	if( $ARGV[3] !~ /\%/ ) {
		@regkey_filter = readInOrSplit($ARGV[3], ",");
	}
}
#print "<!-- regkeyfilter: ".join(",", @regkey_filter)."-->\n";

my $dba2server = getXlsRandomizedRegistryValue("SOFTWARE/XLS/Data Servers/dba2", "Server");
my $dba2uid = getXlsRegistryValue("SOFTWARE/XLS/Data Servers/dba2", "User");
my $dba2pwd = getXlsRegistryValue("SOFTWARE/XLS/Data Servers/dba2", "Password");
print "<!-- using dba2 server:$dba2server,uid:$dba2uid,pwd:$dba2pwd-->\n";

# Read regkey location/status from db
## Should use DBI/DBD wrapper for ADO so in case we ever migrate away from sql server.... (yeah right!)
##  or DBI/DBD for ODBC if we really are aiming for portability
my @regkey_input_list = ();
my %regkey_data_name = ();
use Win32::OLE;
my $xlsserver = getXlsRandomizedRegistryValue("SOFTWARE/XLS/Data Servers/xls", "Server");
my $xlsuid = getXlsRegistryValue("SOFTWARE/XLS/Data Servers/xls", "User");
my $xlspwd = getXlsRegistryValue("SOFTWARE/XLS/Data Servers/xls", "Password");
my $cn = Win32::OLE->new("ADODB.Connection");
my $dsn = "PROVIDER=SQLOLEDB;SERVER=$xlsserver;DATABASE=xls;UID=$xlsuid;PWD=$xlspwd";
print "<!-- using xls server:$xlsserver,uid:$xlsuid,pwd:$xlspwd-->\n";
$cn->open($dsn);
if( !$cn ) {
	die "Could not open connection to DSN because of [$!]";
}

# build db list from xls resource list instead of from list below
my $rsdbs = $cn->Execute("
			  select distinct inv_regkey,data_regkey from searchentry order by inv_regkey
			 ");
if( !$rsdbs ) {
	print "rsdbs failure";
	my $Err = $cn->Errors();
	print "Errors: \n";
	foreach my $e (keys %$Err) {
		print $e->{Description} . "\n";
	}
	die "Could not execute SQL.";
}
while( !$rsdbs->EOF ) {
	my $regkey_val = $rsdbs->Fields(0)->{Value};
	push @regkey_input_list, lc($regkey_val);
	$regkey_data_name{lc($regkey_val)} = lc($rsdbs->Fields(1)->{Value});
	$rsdbs->MoveNext;
}
$cn->Close;

my %regkeys = ();
my %server_process_count = ();
$dsn = "PROVIDER=SQLOLEDB;SERVER=$dba2server;DATABASE=dba2;UID=$dba2uid;PWD=$dba2pwd";
$cn->open($dsn);
if( !$cn ) {
	die "Could not open connection to DSN because of [$!]";
}
foreach my $rk (@regkey_input_list) {
	my $rk_to_whereis = $regkey_data_name{$rk};
	my $rs = $cn->Execute("whereis '$rk_to_whereis'");
	if( !$rs ) {
		print "rs failure";
		my $Err = $cn->Errors();
		print "Errors: \n";
		foreach my $e (keys %$Err) {
			print $e->{Description} . "\n";
		}
		die "Could not execute SQL.";
	}
	while( !$rs->EOF ) {
		my $server = $rs->Fields(2)->{Value};
		my $status = $rs->Fields(3)->{Value};
		my $loc = $rs->Fields(6)->{Value};
		#print "<!-- $rk on $server is ";
		if( $status eq 'online' && (length($location) eq 0 || $loc =~ /$location/i) ) {
			#print "*online* and \@ $location";
			if( grep( /$rk/, @sep_process_indices ) ) {
				$server_process_count{"$server"} = $server_process_count{"$server"} + 1;
				my $x = $server_process_count{"$server"};
				$regkeys{"${server}_${x}"} = $rk;
			} else {
				$regkeys{"${server}_0"} = $regkeys{"${server}_0"} . "$rk,";
			}
		}
		#print "-->\n";
		$rs->MoveNext;
	}
	$rs->Close;
}
$cn->Close;

# Now loop over the constructed hash of potential servers and regkeys and retrieve statistics from the GIS running on the appropriate server
SERVER: foreach my $server_key (sort keys %regkeys) {
	my $server = substr $server_key, 0, length($server_key) -2;
	my $uc_server = uc($server_key);
	my @inv_indices = split(/,/, $regkeys{$server_key});

	my $pipe_delimed = join( '|', @regkey_filter );
	if (scalar(@regkey_filter) ne 0 && !grep( /$pipe_delimed/, @inv_indices )) { print "<!-- skipping $server entry because of regkeys -->\n"; }
	if (scalar(@server_filter) ne 0 && !grep( /$server$/, @server_filter )) { print "<!-- skipping $server entry because of servers -->\n"; }
	next SERVER if (scalar(@regkey_filter) ne 0 && !grep( /$pipe_delimed/, @inv_indices ))
		|| (scalar(@server_filter) ne 0 && !grep( /$server$/, @server_filter ) )
		|| (@inv_indices eq 0);

	my $comma_delimed = join( ',', @inv_indices );
	my @shell_args = ("perl", "ds_stat.pl", "$server", "$comma_delimed", "$rerequest_nostats");
	system( @shell_args ); # or die "ERROR!  Shell execute failed: $?";
}

#my ($sec2,$min2,$hour2,$mday2,$mon2,$year2,$wday2,$yday2) = gmtime(time);
#$year2+=1900; $mon2++;
#$to_print_at_end_buffer .= "<I>--Page Generation Completed: $mday2/$mon2/$year2 $hour2:$min2.$sec2--</I>";

print "Done.\n";

