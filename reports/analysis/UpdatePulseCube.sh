XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

if [ $# -ne 1 ]
then
	echo "Usage: UpdatePulseCube.sh as_server"
	exit 1
fi

as_server=$1
domainuser="ALACRA.NET\\prodbuild"
domainpass="!R)Nmountain"

scriptdir=$XLS/src/scripts/reports/analysis

run() {
	retval=0

	echo "Processing Dimensions (ProcessUpdate) in PulseEventStats cube in Analysis Services database Pulse on $as_server..."
	ascmd -S ${as_server} -U ${domainuser} -P ${domainpass} -d Pulse -i ${scriptdir}/processDim.xmla -o processDimout.xml -T processDim.trace
	ssaserrcheck.exe processDimout.xml
	if [ $? != 0 ]
	then
		echo -e "Error processing Dimensions (processDim.xmla)"
		retval=1
	fi

	echo "Processing Cube (ProcessFull) PulseEventStats in Analysis Services database Pulse on $as_server..."
	ascmd -S ${as_server} -U ${domainuser} -P ${domainpass} -d Pulse -i ${scriptdir}/processCube.xmla -o processCubeout.xml -T processCube.trace
	ssaserrcheck.exe processCubeout.xml
	if [ $? != 0 ]
	then
		echo -e "Error processing Cube (processCube.xmla)"
		retval=1
	fi



	echo "Processing Dimensions (ProcessUpdate) of AlacraStore cube in Analysis Services database AlacraStore on $as_server..."
	ascmd -S ${as_server} -U ${domainuser} -P ${domainpass} -d AlacraStore -i ${scriptdir}/processDim_Alacrastore.xmla -o processDimout_Alacrastore.xml -T processDim_Alacrastore.trace
	ssaserrcheck.exe processDimout_Alacrastore.xml
	if [ $? != 0 ]
	then
		echo -e "Error processing Dimensions (processDim_Alacrastore.xmla)"
		retval=1
	fi

	echo "Processing Cube (ProcessFull) AlacraStore in Analysis Services database AlacraStore on $as_server..."
	ascmd -S ${as_server} -U ${domainuser} -P ${domainpass} -d AlacraStore -i ${scriptdir}/processCube_Alacrastore.xmla -o processCubeout_Alacrastore.xml -T processCube_Alacrastore.trace
	ssaserrcheck.exe processCubeout_Alacrastore.xml
	if [ $? != 0 ]
	then
		echo -e "Error processing Cube (processCube_Alacrastore.xmla)"
		retval=1
	fi



	echo "Processing Dimensions (ProcessUpdate) of DMOmonitorStats cube in Analysis Services database DMOmonitor on $as_server..."
	ascmd -S ${as_server} -U ${domainuser} -P ${domainpass} -d DMOmonitor -i ${scriptdir}/processDim_DMOmonitor.xmla -o processDimout_DMOmonitor.xml -T processDim_DMOmonitor.trace
	ssaserrcheck.exe processDimout_DMOmonitor.xml
	if [ $? != 0 ]
	then
		echo -e "Error processing Dimensions (processDim_DMOmonitor.xmla)"
		retval=1
	fi

	echo "Processing Cube (ProcessFull) DMOmonitorStats in Analysis Services database DMOmonitor on $as_server..."
	ascmd -S ${as_server} -U ${domainuser} -P ${domainpass} -d DMOmonitor -i ${scriptdir}/processCube_DMOmonitor.xmla -o processCubeout_DMOmonitor.xml -T processCube_DMOmonitor.trace
	ssaserrcheck.exe processCubeout_DMOmonitor.xml
	if [ $? != 0 ]
	then
		echo -e "Error processing Cube (processCube_DMOmonitor.xmla)"
		retval=1
	fi



	echo "Processing Dimensions (ProcessUpdate) of Usage cube in Analysis Services database AlacraPremium on $as_server..."
	ascmd -S ${as_server} -U ${domainuser} -P ${domainpass} -d AlacraPremium -i ${scriptdir}/processDim_AlacraPremium.xmla -o processDimout_AlacraPremium.xml -T processDim_AlacraPremium.trace
	ssaserrcheck.exe processDimout_AlacraPremium.xml
	if [ $? != 0 ]
	then
		echo -e "Error processing Dimensions (processDim_AlacraPremium.xmla)"
		retval=1
	fi

	echo "Processing Cube (ProcessFull) Usage in Analysis Services database AlacraPremium on $as_server..."
	ascmd -S ${as_server} -U ${domainuser} -P ${domainpass} -d AlacraPremium -i ${scriptdir}/processCube_AlacraPremium.xmla -o processCubeout_AlacraPremium.xml -T processCube_AlacraPremium.trace
	ssaserrcheck.exe processCubeout_AlacraPremium.xml
	if [ $? != 0 ]
	then
		echo -e "Error processing Cube (processCube_AlacraPremium.xmla)"
		retval=1
	fi


	return $retval
}


if [ "${XLSDATA}" = "" ]
then
	echo "Warning, XLSDATA not defined, using f:/users/default"
	XLSDATA=f:/users/default
	if [ ! -d "f:/users/default" ]
	then
		mkdir -p f:/users/default
		if [ $? != 0 ]
		then
		    echo "XLSDATA is not set up, exiting"
		    exit 1
		fi
	fi
fi

if [ ! -d $XLSDATA/AnalysisServices/log ]
then
	mkdir -p $XLSDATA/AnalysisServices/log
	if [ $? != 0 ]
	then
	    echo "XLSDATA/AnalysisServices/log is not set up, exiting"
	    exit 1
	fi
fi

cd $XLSDATA/AnalysisServices

logfile=log/UpdatePulseCube`date +%y%m%d`.log
run > ${logfile}
if [ $? -ne 0 ]
then
	blat ${logfile} -t administrators@alacra.com -s "Update of Cubes on Analysis Services Server $as_server failed."
	exit 1
else
	successmsg="Update of Cubes on Analysis Services Server $as_server was successful."
	echo "${successmsg}" > success_email.txt
	echo "Log file at http://${COMPUTERNAME}/xlsdata/AnalysisServices/${logfile}" >> success_email.txt

#	blat success_email.txt -t administrators@alacra.com -s "${successmsg}"
	rm success_email.txt
fi

exit 0
