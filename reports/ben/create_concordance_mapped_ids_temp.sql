if exists (select * from dbo.sysobjects where id = object_id(N'[concordance_mapped_ids_temp]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [concordance_mapped_ids_temp]
go

CREATE TABLE concordance_mapped_ids_temp
(
	local_id	varchar(255) NULL,
	latest_doc_id	varchar(255) NULL,
	latest_doc_date	varchar(255) NULL,
	url	varchar(512) NULL,
)
GO

CREATE INDEX concordance_mapped_ids_temp_01 ON concordance_mapped_ids_temp(local_id)
GO
