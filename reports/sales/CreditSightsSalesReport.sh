XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 - database
if [ $# -lt 1 ]
then
	echo "Usage: $0 database"
	exit 1
fi

database=$1
# Save the runstring parameters
logon=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`
dataserver=`get_xls_registry_value ${database} Server`


isql /U${logon} /P${password} /S${dataserver} /n /w 500 < $XLS/src/scripts/reports/sales/CreditSightsSalesReport.sql | sed -e "s/--------------------------------*/------------------/" | sed -e "s/  *$//" | blat - -t cssales@alacra.com -s "Daily CreditSights Sales Report"
