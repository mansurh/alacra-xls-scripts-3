set nocount on
set tran isolation level read uncommitted
PRINT 'Data Downlink Daily Sales Report'
PRINT ''
PRINT ''
select CONVERT(varchar(16),GETDATE(),107) 'Today''s Date'
PRINT ''
PRINT ''


PRINT 'Subscribers expiring in the next 30 days'
PRINT '========================================'
select
	convert (varchar(12),v.name) 'Service',
	convert (varchar(24),COALESCE(p.company,a.name)) 'Company',
	convert (varchar(16),p.last_name) 'Last Name',
	convert (varchar(12),p.first_name) 'First Name',
	convert (varchar(12),p.login) 'User-ID',
	convert (varchar(12),a.id) 'Account-ID',
	convert (varchar(12),p.terminate,107) 'Expiration',
	convert (varchar(12),COALESCE(s.last_name,' ')) 'Account Manager',
	COALESCE (convert (varchar(3),sp.abbreviation),''),
	COALESCE (convert (varchar(8),c.name),'')
from
	users p, account a left outer join state_prov sp on(a.state_prov = sp.id) left outer join country c on (a.country = c.id)
    left outer join salesperson s on (a.account_manager = s.id) ,  service v
where
	p.terminate >= GETDATE()
	and
	p.terminate <= DATEADD(day,30,GETDATE())
	and
	p.demo_flag is null
	and
	p.account = a.id
	and
	p.service = v.id
	
order by
	convert (varchar(12), v.name), convert (varchar(24),COALESCE(p.company,a.name)), p.last_name

	
PRINT ''
PRINT ''

PRINT 'Demo User IDs expiring in the next 7 days'
PRINT '========================================='
select
	convert (varchar(12),v.name) 'Service',
	convert (varchar(24),COALESCE(p.company,a.name)) 'Company',
	convert (varchar(16),p.last_name) 'Last Name',
	convert (varchar(12),p.first_name) 'First Name',
	convert (varchar(12),p.login) 'User-ID',
	convert (varchar(12),a.id) 'Account-ID',
	convert (varchar(12),p.terminate,107) 'Expiration',
	convert (varchar(12),COALESCE(s.last_name,' ')) 'Acct Manager'
from
	users p, account a left outer join salesperson s on (a.account_manager = s.id), service v
where
	p.terminate >= GETDATE()
	and
	p.terminate <= DATEADD(day,7,GETDATE())
	and
	p.demo_flag is not null
        and
        p.account=a.id
	and
	p.service = v.id
order by 
	convert (varchar(12), v.name), convert (varchar(24),COALESCE(p.company,a.name)), p.last_name


PRINT ''
PRINT ''

PRINT 'Demo User Downloads by User, Content Provider in the last 7 days'
PRINT '================================================================'
select 
	convert (varchar(24),COALESCE(p.company,a.name)) 'Company',
	convert (varchar(16),p.last_name) 'Last Name',
	convert (varchar(12),p.first_name) 'First Name',
	convert (varchar(12),p.login) 'User-ID',
	convert (varchar(12),a.id) 'Account-ID',
	convert (varchar(50),i.name) 'Content Provider', 
	convert (varchar(4), count(u.price)) 'Count', 
	convert (varchar(10),sum(u.price)) 'Total $',
	substring (convert (varchar(8),max(u.access_time),112),5,4) 'Last',
	convert (varchar(12),max(COALESCE(s.last_name,' '))) 'Acct Manager'
from 
	usage u, users p, ip i, account a left outer join salesperson s on (a.account_manager = s.id)
where
	u.access_time >= DATEADD(day,-7,CONVERT(varchar(9),GETDATE(),1))
and
	u.access_time < CONVERT(varchar(9),GETDATE(),1)
and
	u.userid = p.id
and
	u.ip = i.id
and
	p.demo_flag is not null
and
	p.account = a.id
	
group by 
	convert (varchar(24),COALESCE(p.company,a.name)),p.last_name, p.first_name, p.login, a.id, i.name
order by
	convert (varchar(24),COALESCE(p.company,a.name)),p.last_name, p.first_name, p.login, a.id, i.name

go

PRINT ''
PRINT ''

PRINT 'Demo User Downloads by User, Content Provider in the last 1 day'
PRINT '==============================================================='
select 
	convert (varchar(24),COALESCE(p.company,a.name)) 'Company',
	convert (varchar(16),p.last_name) 'Last Name',
	convert (varchar(12),p.first_name) 'First Name',
	convert (varchar(12),p.login) 'User-ID',
	convert (varchar(12),a.id) 'Account-ID',
	convert (varchar(50),i.name) 'Content Provider', 
	convert (varchar(4), count(u.price)) 'Count', 
	convert (varchar(10),sum(u.price)) 'Total $',
	substring (convert (varchar(8),max(u.access_time),112),5,4) 'Last',
	convert (varchar(12),max(COALESCE(s.last_name,' '))) 'Acct Manager'
from 
	usage u, users p, ip i, account a left outer join salesperson s on (a.account_manager = s.id)
where
	u.access_time >= DATEADD(day,-1,CONVERT(varchar(9),GETDATE(),1))
and
	u.access_time < CONVERT(varchar(9),GETDATE(),1)
and
	u.userid = p.id
and
	u.ip = i.id
and
	p.demo_flag is not null
and
	p.account = a.id

group by 
	convert (varchar(24),COALESCE(p.company,a.name)),p.last_name, p.first_name, p.login, a.id, i.name
order by
	convert (varchar(24),COALESCE(p.company,a.name)),p.last_name, p.first_name, p.login, a.id, i.name

go

PRINT ''
PRINT ''

PRINT 'Demo Prepbooks by User in the last 1 day'
PRINT '========================================'
select 
	convert (varchar(24),COALESCE(u.company,a.name)) 'Company', convert(varchar(24),u.login) 'User', count(*) 'Count'
from 
	prepbook p, users u, account a
where 
	p.pbcreatetime > DATEADD(day,-1,GETDATE())
	and
	p.userid = u.id
	and
	u.account = a.id
	and
	u.demo_flag is not null
group by
	convert (varchar(24),COALESCE(u.company,a.name)), convert(varchar(24),u.login)
order by
	convert (varchar(24),COALESCE(u.company,a.name)), convert(varchar(24),u.login) asc

go

PRINT ''
PRINT ''

PRINT 'Potential Duplicate Downloads in the last 24 hours'
PRINT '=================================================='
select  
	distinct u1.userid, p.company, CONVERT(varchar(256),u1.description), u1.price, CONVERT (varchar(20), u1.access_time,113), u1.usageid
from usage u1, usage u2, users p
where
p.id = u1.userid
and
p.id = u2.userid
and
u1.access_time > DATEADD(minute,-(60*24+5),CONVERT(varchar(9),GETDATE(),1))
and
u1.access_time < DATEADD(minute, 5, CONVERT(varchar(9),GETDATE(),1))
and
u2.access_time > DATEADD(minute,-(60*24+5),CONVERT(varchar(9),GETDATE(),1))
and
u2.access_time < DATEADD(minute, 5, CONVERT(varchar(9),GETDATE(),1))
and
u1.userid = u2.userid
and
u1.description = u2.description
and
u1.list_price = u2.list_price
and
DATEDIFF (second, u1.access_time, u2.access_time) < 60
and
DATEDIFF (second, u1.access_time, u2.access_time) > -60
and
u1.usageid <> u2.usageid
and
u1.price >= 1.00
and
u2.price >= 1.00
and
(u1.project is NULL or u1.project=u2.project)
and
u1.no_charge_flag is NULL
and
u2.no_charge_flag is NULL
and
p.demo_flag is NULL
order by u1.userid, p.company, CONVERT(varchar(256),u1.description), CONVERT (varchar(20), u1.access_time,113) asc

go