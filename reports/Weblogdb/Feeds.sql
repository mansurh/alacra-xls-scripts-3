set nocount on
set transaction isolation level read uncommitted

select t.descr, count(distinct f.id), count(distinct e.id) from
feeds f, weblogs w, event e, event_type et, source_type t
where 
w.feed_id = f.id
and
w.id = e.doc_id
and
e.event_type = et.id
and
et.name='AnalystQuote'
and
e.date >= DATEADD(day,-30,GETDATE())
and
f.source_type = t.id
group by t.descr

order by count(distinct e.id) desc
