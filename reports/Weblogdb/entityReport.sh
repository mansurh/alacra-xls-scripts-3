XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

database="weblogdb"
emailDestination="entity_extract@alacra.com"
scriptdir=$XLS/src/scripts/reports/weblogdb

# Get the SQL Server to connect to
dbserver=`get_xls_registry_value ${database} Server`
dbuser=`get_xls_registry_value ${database} User`
dbpassword=`get_xls_registry_value ${database} Password`

echo "Weblogdb Stats for ${database} on ${dbserver}"
mkdir -p $XLSDATA/weblogdb
cd $XLSDATA/weblogdb

reportfile=$XLSDATA/weblogdb/report.xml
rm -f ${reportfile}
rm -f report.zip

# Start the report output file
cat ${scriptdir}/preamble.xml > ${reportfile}
if [ $? != 0 ]
then
	echo "Error creating report.xml file, terminating" >&2
	exit 1
fi

cd ${scriptdir}
for Sheet in Summary Feeds ByCompany ByCountry ByFirm ByAnalyst ActiveAnalysts ActiveFirms ActiveFeeds ActiveBlogs
do
	# Run the queries for the individual sheets
	sqlcmd -S ${dbserver} -U ${dbuser} -P ${dbpassword}  -o ${Sheet}.rpt -W -s"|" -h -1 -i ${Sheet}.sql
	if [ $? != 0 ]
	then
		echo "Error running SQLQuery ${Sheet}.sql, terminating" >&2
		exit 1
	fi 

	# Convert The Rows of Data into XML
	if [ "${Sheet}" = "ByCompany" ]
	then
		sed -f ${scriptdir}/companies.sed ${Sheet}.rpt > ${Sheet}.xml
	elif [ "${Sheet}" = "Summary" ]
	then
		sed -f ${scriptdir}/summary.sed ${Sheet}.rpt > ${Sheet}.xml
	elif [ "${Sheet}" = "ActiveAnalysts" ]
	then
		sed -f ${scriptdir}/ActiveAnalysts.sed ${Sheet}.rpt > ${Sheet}.xml
	elif [ "${Sheet}" = "ActiveFirms" ]
	then
		sed -f ${scriptdir}/ActiveFirms.sed ${Sheet}.rpt > ${Sheet}.xml
	elif [ "${Sheet}" = "ActiveFeeds" ]
	then
		sed -f ${scriptdir}/ActiveFeeds.sed ${Sheet}.rpt > ${Sheet}.xml
	elif [ "${Sheet}" = "ActiveBlogs" ]
	then
		sed -f ${scriptdir}/ActiveBlogs.sed ${Sheet}.rpt > ${Sheet}.xml

	else 
		sed -f ${scriptdir}/aggregates.sed ${Sheet}.rpt > ${Sheet}.xml
	fi

	# Must be in the UTF-8 Character Set
	iconv -f ISO-8859-1 -t UTF-8 ${Sheet}.xml > ${Sheet}_utf.xml
	if [ $? != 0 ]
	then
		echo "Error converting output to UTF-8, terminating" >&2
		exit 1
	fi 


	# Append together the Excel bits
	for bit in ${Sheet}Start.xml ${Sheet}_utf.xml WorksheetEnd.xml
	do
		cat ${bit}  >> ${reportfile}
		if [ $? != 0 ]
		then
			echo "Error concatenating ${bit} to report file, terminating" >&2
			exit 1
		fi 
	done

	# Clean Up
	rm -f ${Sheet}.rpt
	rm -f ${Sheet}.xml
	rm -f ${Sheet}_utf.xml
done

cd $XLSDATA/weblogdb

# End the report output file
cat ${scriptdir}/postamble.xml >> ${reportfile}
if [ $? != 0 ]
then
	echo "Error wrapping up report.xml file, terminating" >&2
	exit 1
fi

# Zip the report file
zip report.zip ${reportfile}
if [ $? != 0 ]
then
	echo "Error zipping report.xml file, terminating" >&2
	exit 1
fi



# Mail it out.  Must be a binary attachment as UTF-8 uses the high bit
blat -to ${emailDestination} -subject "Entity Extraction Statistics (last 30 days)" -body "See Attached" -uuencode -attach report.zip
if [ $? != 0 ]
then
	echo "Error sending report file via Blat, terminating" >&2
	exit 1
fi


# Clean up final file
# rm -f report.xml
# rm -f report.zip

exit 0