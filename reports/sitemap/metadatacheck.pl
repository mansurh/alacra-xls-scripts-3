#!c:/perl/bin/perl
use LWP::Simple;
use XML::Parser;

die "Usage: metadatacheck.pl mdurl"
    unless (@ARGV == 1);

my $loc="";
my $publication="";
my $publisher="";
my $date="";
my $provider="";
my $ppv="";
my $curr_tag="";

my $metadataurl=$ARGV[0];
my $metadatafile="metadata.xml";
my $errstr="";

my $savefile=$metadatafile;
if ( $metadataurl =~ /\.gz$/ ) {
  $savefile .= ".gz";
}

system("rm -f $metadatafile $savefile");

print ("$metadataurl ");

$rc=getstore($metadataurl, $savefile);

if ( ! -s $savefile ) {
  print "Error downloading file (0 size)!\n";
  exit 1;
}

if ( $rc != 200 ) {
  print "$rc HTTP err on retrieve\n";
  exit (1);
}

if ( $metadataurl =~ /\.gz$/ ) {
  system("gzip -d $savefile");
}

my $parser = new XML::Parser(ErrorContext => 2,
                             ProtocolEncoding => "UTF-8",
                             Handlers => {Start => \&start_handler,
                                          End => \&end_handler,
                                          Char => \&char_handler}
                             );

$parser->parsefile($metadatafile);

if ( $errstr eq "" ) {
  print "OK\n";
  exit 0;
} else {
  print "\n$errstr\n";
  exit 1;
}


sub start_handler
{
  my $p = shift;
  my $el = shift;

  $curr_tag = $el;

  if ($el eq "record") {
    $loc="";
    $publication="";
    $publisher="";
    $date="";
    $provider="";
    $ppv="";
  } 
}

sub end_handler
{
  my $p = shift;
  my $el = shift;

  if ($el eq "record") {
    # print "$loc $lastmod $hint\n";
    # do tests
    $item = $loc;
    $item =~ s/.+\/([^\/]+)$/$1/g;

    if ($loc eq "") {
      $errstr .= "  $item: empty loc in metadataindex\n";
    } elsif ($loc !~ /http:\/\/[^\/]+\/storecontent\/.+/ ) {
      $errstr .= "  $item: item must have /storecontent/ in URL\n";
    } 

    if ($publisher eq "" || $publication eq "") {
      $errstr .= "  $item: publisher and publication must not be blank\n";
    }

    if ($provider ne "Alacra Store") {
      $errstr .= "  $item: provider must be 'Alacra Store'\n";
    }

    if ($ppv ne "yes") {
      $errstr .= "  $item: PPV must be yes\n";
    }

    if ($date eq "") {
      $errstr .= "  $item: date must not be blank\n";
    } elsif ($date !~ /^[12][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]$/ ) {
      $errstr .= "  $item: date must by YYYY-MM-DD format\n";
    }     
  } 
}

sub char_handler
{
  my ($p, $data) = @_;

  if ($curr_tag eq "loc") {
    $loc .= $data;
  } elsif ($curr_tag eq "publisher") {
    $publisher .= $data;
  } elsif ($curr_tag eq "publication") {
    $publication .= $data;
  } elsif ($curr_tag eq "date") {
    $date .= $data;
  } elsif ($curr_tag eq "provider") {
    $provider .= $data;
  } elsif ($curr_tag eq "ppv") {
    $ppv .= $data;
  }
}
