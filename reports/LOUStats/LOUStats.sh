DATADIR=${XLSDATA}/lou
XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_arguments.fn
. $XLSUTILS/get_registry_value.fn

database="concordance"
emailDestination="cicistats@alacra.com"

check_arguments $# 1 "Usage: LOUStats.sh sourceid" 1

sourceid=$1

# Get the SQL Server to connect to
dbserver=`get_xls_registry_value ${database} Server`
dbuser=`get_xls_registry_value ${database} User`
dbpassword=`get_xls_registry_value ${database} Password`

dba2database=dba2
dba2server=`get_xls_registry_value ${dba2database} Server`
dba2login=`get_xls_registry_value ${dba2database} User`
dba2password=`get_xls_registry_value ${dba2database} Password`

thisServer=$(echo $dbserver | tr [A-Z] [a-z]) #lower case version of the server name
masterServer=""

# if this is production master server then generate statistics report
bcp "exec whereis ${database}" queryout whereis.txt -c -S${dba2server} -U${dba2login} -P${dba2password} -t "|"
if [ $? -ne 0 ]; then echo "Error in getting concordance list from dba2"; exit 1; fi;

IFS="|"

while read junk env server junk4 junk5 junk6 junk7 master junkend
do
	currServer=$(echo $server | tr [A-Z] [a-z]) #lover case version of the server name
	currEnv=$(echo $env | tr [A-Z] [a-z])  #lover case version of the environment type
	if [ $currEnv = "production" ]
	then
		flag=$(echo $master|cut -c1| tr [A-Z] [a-z])
		echo "master=$flag"
		if [ "$flag" = "y" ]
		then
			masterServer=${currServer}
		fi;
	fi;
done < whereis.txt

rm -f whereis.txt

if [ "${masterServer}" != "${thisServer}" ]
then
	echo "LEI Stats on ${dbserver}: Not master server, exiting"
	exit 0
fi

lou=`isql -S ${dbserver} -U ${dbuser} -P ${dbpassword} -Q "exec spGetLOUbySourceID ${sourceid}" -h-1 | head -1`
if [ $? != 0 ]
then
	echo "Error running spGetLOUbySourceID, terminating"
	exit 1
fi

if [[ -z $lou ]]
then
	echo "Source ${sourceid} is not an active LOU. Exiting...";
	exit 0;
fi

filters=`isql -S ${dbserver} -U ${dbuser} -P ${dbpassword} -Q "exec spCountLOUFilters '${lou}'" -h-1 | head -1`
if [ $? != 0 ]
then
	echo "Error running spCountLOUFilters, terminating"
	exit 1
fi

if [ "$filters" -eq 0 ]
then
	echo "Source ${sourceid}. There is no metadata to generate LOU statistics report. Exiting...";
	exit 0;
fi

echo "LEI Stats for ${database} on ${dbserver}"

if [ "${dbserver}" = "datatest2" ]
then
	emailDestination="simon.vileshin@alacra.com"
fi


mkdir -p ${DATADIR}
cd ${DATADIR}

# Start the report output file
cat< $XLS/src/scripts/reports/LOUStats/preamble.xml >report.xml
if [ $? != 0 ]
then
	echo "Error creating report.xml file, terminating"
	exit 1
fi


#Replace sqlcmd with bcp, as writing out to the file caused problems for this script
#sqlcmd -S ${dbserver} -U ${dbuser} -P ${dbpassword} -Q "exec spGetLOUStatistics ${sourceid}"  -y0 >  lou.xml
bcp "exec spGetLOUStatistics ${sourceid}" queryout lou.xml  -S ${dbserver} -U ${dbuser} -P ${dbpassword} -c -CRAW
if [ $? != 0 ]
then
	echo "Error running spGetLOUStatistics, terminating"
	exit 1
fi 

# Must be in the UTF-8 Character Set
iconv -f ISO-8859-1 -t UTF-8 < lou.xml > lou_utf.xml
if [ $? != 0 ]
then
	echo "Error converting output to UTF-8, terminating"
	exit 1
fi 

# Add report body
cat< lou_utf.xml >>report.xml
if [ $? != 0 ]
then
	echo "Error creating report.xml file, terminating"
	exit 1
fi

# End the report output file
cat < $XLS/src/scripts/reports/LOUStats/postamble.xml >>report.xml
if [ $? != 0 ]
then
	echo "Error wrapping up report.xml file, terminating"
	exit 1
fi

rm -f lou.xml


# Zip the report file
zip report.zip report.xml
if [ $? != 0 ]
then
	echo "Error zipping report.xml file, terminating"
	exit 1
fi


# Mail it out.  Must be a binary attachment as UTF-8 uses the high bit
blat -to "${emailDestination}" -subject "${lou} Statistics" -body "See Attached" -uuencode -attach report.zip
if [ $? != 0 ]
then
	echo "Error sending report file via Blat, terminating"
	exit 1
fi

# Clean up final file
rm -f report.xml
rm -f report.zip
rm -f lou_utf.xml
exit 0
