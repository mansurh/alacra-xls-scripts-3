SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
GO

set transaction isolation level read uncommitted
set nocount on


PRINT 'Deal Titles where last_audit_date is in the past 24 hours'
PRINT ''
PRINT ''




declare @title varchar(255)
declare @gen_title varchar(255)
declare @dealno varchar(255)
declare @date datetime


declare pcursor cursor FAST_FORWARD FOR
	select dealno, last_audit_date, title, generic_title
	from pulse_deals
	where last_audit_date > DATEADD(hour, -24, getdate())
	order by dealno desc
open pcursor
fetch next from pcursor into @dealno, @date, @title, @gen_title
while (@@fetch_status <> -1)
begin
 if (@@fetch_status <> -2)
 begin
	PRINT 'Deal # ' + @dealno+ ': last audit ' + CONVERT(varchar, @date, 100)
	IF @title IS NOT NULL
		PRINT '  Title: ' + @title
	IF @gen_title IS NOT NULL
		PRINT '  Generic Title: ' + @gen_title
	PRINT ''
 end
 fetch next from pcursor into @dealno, @date, @title, @gen_title
end
close pcursor
deallocate pcursor
