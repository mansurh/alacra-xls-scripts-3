set nocount on
set transaction isolation level read uncommitted
set ansi_warnings off

select x.accountid, x.userid, x.[login], x.first_name, x.last_name, x.company, x.commence, x.terminate, x.email, x.login_count,
	count(distinct p.primaryid) as num_pageviews, count(distinct s.primaryid) as num_searches,
	coalesce(h.email_count,0) as num_alert_emails
from xls_logins_temp x
left outer join pulseimpressionlog_temp p on x.userid = p.userid
left outer join pulsesearchlog_temp s on x.userid = s.userid
left outer join alerts_emails_temp h on x.userid = h.userid
group by x.accountid, x.userid, x.[login], x.first_name, x.last_name, x.company, x.commence, x.terminate, x.email, x.login_count, x.most_recent, coalesce(h.email_count,0)
order by x.accountid, x.userid, x.[login]
