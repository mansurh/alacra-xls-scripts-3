
set spreadsheetName "extranet.xls"
set magicid "pwcextra"
set xmlName "extranet.xml"

# Open the Spreadsheet
set sheet [spreadsheet $spreadsheetName]

# Open the XML output file
set xmlfile [open $xmlName w]

# Set the Column Widths
$sheet column 0 0 [expr 16 * 256]
$sheet column 1 3 [expr 36 * 256]
$sheet column 4 4 [expr 36 * 256]
$sheet column 5 5 [expr 64 * 256]
$sheet column 6 6 [expr 16 * 256]

# Set some rudimentary styles
set titleFont [$sheet font bold color BLACK]
set titleStyle [$sheet style font $titleFont]

# Connect to the database
set dbhandle [sybconnect xls xls data8]

# Execute the usage select
sybsql $dbhandle "declare @startdate smalldatetime declare @enddate smalldatetime select @startdate=DATEADD(month, 0, GETDATE()) select @startdate=DATEADD(day,-1*(DATEPART(day,@startdate) -1),@startdate) select @startdate = DATEADD(hour,-1*DATEPART(hour,@startdate),@startdate) select @startdate=DATEADD(minute,-1*DATEPART(minute,@startdate),@startdate) select @enddate=DATEADD(month,1,@startdate) select @startdate, @enddate select p.login, u.project, u.project2, u.project3, u.access_time, u.description, u.price from users p, usage u where p.id=u.userid and p.login=\"$magicid\" and u.no_charge_flag is null and u.access_time >= p.commence and u.access_time >= \"January 1,1999\" and u.access_time <= \"January 1, 2003\" order by p.login asc, u.access_time asc"

# Add the labels to the spreadsheet
$sheet cell 0 0 $titleStyle label "DDL Usage through PWC Extranet"
$sheet cell 1 0 $titleStyle label "Starting Date:"
$sheet cell 2 0 $titleStyle label "Ending Date:"
set row 4
	$sheet cell $row 0 $titleStyle label "Login"
	$sheet cell $row 1 $titleStyle label "Project Code"
	$sheet cell $row 2 $titleStyle label "B2BScene ID"
	$sheet cell $row 3 $titleStyle label "E-Mail/Country"
	$sheet cell $row 4 $titleStyle label "Date/Time"
	$sheet cell $row 5 $titleStyle label "Description"
	$sheet cell $row 6 $titleStyle label "Price"
set row [incr row]

# Add the XML header stuff
puts $xmlfile "<DOCUMENT>"
puts $xmlfile "<RESULTSET NAME=\"usage\" xmlns=\"www.xls.com\" xmlns:us=\"www.xls.com/usage\">"

# Eat the null result sets
sybnext $dbhandle { }
sybnext $dbhandle { }
sybnext $dbhandle { }
sybnext $dbhandle { }
sybnext $dbhandle { }

# Get the start and end date
sybnext $dbhandle { 

	# Set the starting and ending dates in the spreadsheet
	$sheet cell 1 1 DEFAULT label @1
	$sheet cell 2 1 DEFAULT label @2

	# Add the starting and ending dates to the XML
	set startdate @1
	set enddate @2
	puts $xmlfile "<METADATA>"
	puts $xmlfile "<us:startdate>$startdate</us:startdate>"
	puts $xmlfile "<us:enddate>$enddate</us:enddate>"
	puts $xmlfile "</METADATA>"
	
}


# Start the XML Results Section
puts $xmlfile "<RESULTS>"

sybnext $dbhandle {

	# Output the Excel Spreadsheet Row
	$sheet cell $row 0 DEFAULT label @1
	$sheet cell $row 1 DEFAULT label @2
	$sheet cell $row 2 DEFAULT label @3
	$sheet cell $row 3 DEFAULT label @4
	$sheet cell $row 4 DEFAULT label @5
	$sheet cell $row 5 DEFAULT label @6
	$sheet cell $row 6 DEFAULT number @7
	set row [incr row]

	# Output the XML Data row
	puts $xmlfile "<us:usage>"

	puts $xmlfile "<us:login>" nonewline
	puts $xmlfile @1 nonewline
	puts $xmlfile "</us:login>"

	puts $xmlfile "<us:project1>" nonewline
	puts $xmlfile @2 nonewline
	puts $xmlfile "</us:project1>"

	puts $xmlfile "<us:project2>" nonewline
	puts $xmlfile @3 nonewline
	puts $xmlfile "</us:project2>"

	puts $xmlfile "<us:project3>" nonewline
	puts $xmlfile @4 nonewline
	puts $xmlfile "</us:project3>"

	puts $xmlfile "<us:time>" nonewline
	puts $xmlfile @5 nonewline
	puts $xmlfile "</us:time>"

	puts $xmlfile "<us:description>" nonewline
	puts $xmlfile @6 nonewline
	puts $xmlfile "</us:description>"

	puts $xmlfile "<us:price>" nonewline
	puts $xmlfile @7 nonewline
	puts $xmlfile "</us:price>"

	puts $xmlfile "</us:usage>"
	
}

# Close the database connection
sybclose $dbhandle

# Output the spreadsheet
$sheet write

# Close the Spreadsheet
$sheet close

# End the XML document
puts $xmlfile "</RESULTS>"
puts $xmlfile "</RESULTSET>"
puts $xmlfile "</DOCUMENT>"

# Close the XML File
close $xmlfile
