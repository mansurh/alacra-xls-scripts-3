SET NOCOUNT ON

declare @startdate smalldatetime
declare @enddate smalldatetime

/* Run for previous month */
select @startdate = DATEADD(month, -1, GETDATE())

/* Move to beginning of the month */
select @startdate = DATEADD(day, -1 * (DATEPART(day, @startdate) -1), @startdate)
select @startdate = DATEADD(hour, -1 * DATEPART(hour, @startdate), @startdate)
select @startdate = DATEADD(minute, -1 * DATEPART(minute, @startdate), @startdate)

/* Calculate the end of the monthly period */
select @enddate = DATEADD (month, 1, @startdate)

select CONVERT(char(8),y.accession_number) , CONVERT(char(8), u.access_time, 112)

from xls..usageSpecial s, xls..usage u, xls..users p, cwi..story y

where 
	u.access_time >= @startdate
and
	u.access_time < @enddate
and
	u.userid = p.id
and
	u.ip = 49
and
	u.access_time >= p.commence
and
	p.demo_flag is null
and
	u.usageid = s.usage
and
	y.id = CONVERT (int, s.ipinfo)
order by
	u.access_time
