PRINT 'Alacra DMO Daily Usage Report'
PRINT ''
PRINT ''
select CONVERT(varchar(16),DATEADD(day,-1,GETDATE()),107) 'Today''s Date'
PRINT ''
PRINT ''

PRINT '================================================================='
select 
	convert (varchar(10),userid)'userid',
	convert (varchar(20),remote_address)'IP Address',
--	convert (varchar(25),access_time)'Access Time',
--	convert (varchar(20),msg) 'MSG',
--	convert (varchar(20),topic) 'Topic'
	count (userid) 'Total Access Count'
from 
	dmo_usage
where
	access_time >= DATEADD(day,-1,GETDATE())
group by userid, remote_address
order by
	convert (varchar(10),userid) asc



