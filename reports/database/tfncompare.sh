# tfncompare.sh
# Script to compare record counts from database tables
#
# Usage:
#	tfncompare.sh"
#


tables="report reportsection reporttable reportgraph reportpage pagemap company industry geography product sic subject topic extfile"

# --------- Industry Insider ----------------

server1=data3
server2=data5
db=iinsider

echo "Checking Industry Insider"
for t in $tables
do
	recordcountcomp.sh ${server1} ${server2} ${db} ${db} ${t}
done


# --------- Markintel ----------------

server1=data3
server2=data5
db=markintel

echo ""
echo "Checking Markintel"
for t in $tables
do
	recordcountcomp.sh ${server1} ${server2} ${db} ${db} ${t}
done

recordcountcomp.sh ${server1} ${server2} ${db} ${db} pdfprices


# --------- Investext ----------------

server1=data8
server2=data7
db=itext

echo ""
echo "Checking Investext"
for t in $tables
do
	recordcountcomp.sh ${server1} ${server2} ${db} ${db} ${t}
done


exit 0
