#!/bin/bash
# backup script
cd /root/WIKI
currentYear=`date +%Y`
currentMonth=`date +%m`
currentDay=`date +%d`

datestring="${currentYear}${currentMonth}${currentDay}"

mysqldump --databases wikidb > ${datestring}.sql

if [ $? != 0 ]
then
	echo "Error dumping DB to backup file"
	exit 1
fi

rm -f ${datestring}.sql.gz

gzip ${datestring}.sql

# okay, let's ftp it out
rm -f command.ftp
echo "user administrator newpass" > command.ftp
echo "binary" >> command.ftp
echo "cd /f/mis/wiki" >> command.ftp
echo "put ${datestring}.sql.gz" >> command.ftp
echo "quit" >> command.ftp

echo "FTPing the file to the server"
ftp -i -n data11.alacra.com < command.ftp
if [ $? != 0 ]
then
	echo "Error FTPing file to data11"
	exit 1
fi

echo "Backup succeeded"
rm -f ${datestring}.sql.gz
rm -f command.ftp
