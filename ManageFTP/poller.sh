XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# script to poll the out ftp server to see if the days update
# files are available.

# Get the runtime arguments
if [ $# -lt 1 ]
then
	echo "Usage: poller.sh database [filename (date or partial filename)] [MinutesToWait] [numberOfIterations] [ftp command] [contact_group] [firstCutoff]"
	exit 1
fi
database=$1
ftpserver=`get_xls_registry_value ${database} Server 6`
logon=`get_xls_registry_value ${database} User 6`
password=`get_xls_registry_value ${database} Password 6`

today=`date "+%b %d"`
if [ $# -gt 1 ]
then
	today=$2
fi
minutesToWait="15"
if [ $# -gt 2 ]
then
	minutesToWait=$3
fi
numberOfIterations="20"
if [ $# -gt 3 ]
then
	numberOfIterations=$4
fi

ftp_command=""
if [ $# -gt 4 ]
then
	ftp_command=$5
fi

contactgroup="administrators@alacra.com"
if [ $# -gt 5 ]
then
	contactgroup=$6
fi

cutoff_notify=1
cutofftime=""
if [ $# -gt 6 ]
then
	cutoff_notify=0
	cutofftime=$7
fi


mkdir -p $XLSDATA/${database}/download
cd $XLSDATA/${database}/download
if [ $? -ne 0 ]
then
	echo "error cd to $XLSDATA/${database}/logs directory, exiting..."
	exit 5
fi

pollerlog=poller`date +%m%d`.log
cmdFile="poll.cmd"
lstFile="poll.lst"
rm -f ${cmdFile}

echo "Polling ${database} every $minutesToWait minutes for $numberOfIterations iterations" > ${pollerlog}

# Build a command file to retrieve a file listing of the Sdc server
echo "user ${logon}" > ${cmdFile}
echo "${password}" >> ${cmdFile}
echo "$ftp_command" >> ${cmdFile}
echo "mls -l ${lstFile}" >> ${cmdFile}
echo "quit" >> ${cmdFile}

echo "Looking for files with name or date of ${today} on ${ftpserver} server" >> ${pollerlog}

# Initialize the try count
tryCount=0

while [ 1 == 1 ]
do
	currtime=`date +%H%M`
	# Increment the try count
	tryCount=`expr ${tryCount} + 1`
	echo "${currtime}: Attempt number ${tryCount} to find file name/date of ${today} on ${ftpserver}" >> ${pollerlog}

	ftp -i -n -s:${cmdFile} ${ftpserver} >> ${pollerlog} 2>&1

	# Test the exit status of ftp
	if [ $? != 0 ]
	then
		echo "${currtime}: Error in ftp, Exiting" >> ${pollerlog}
		blat ${pollerlog} -t "$contactgroup" -s "Error: failed to retrieve files for ${database}."
		exit 2
	fi

	# Grep for the various detail files
	grep -q "${today}" ${lstFile}
	if [ $? == 0 ]
	then
		echo "${currtime}: Found file with ${today} name/date on ${ftpserver} Server, OK to proceed" >> ${pollerlog}
		#sleep for few minutes to make sure the file is not in the middle of the upload
		sleep ${minutesToWait}m
		exit 0
	fi

	echo "${currtime}: Unable to find file name/date of ${today} on ${ftpserver}" >> ${pollerlog}

	# See if we have exceeded the try limit
	if [ ${tryCount} -ge ${numberOfIterations} ]
	then
		echo "${currtime}: Retry limit exceeded" >> ${pollerlog}
		blat ${pollerlog} -t "$contactgroup" -s "Error: failed to retrieve files for ${database}."
		exit 3
	fi

	if [ $cutoff_notify -eq 0 -a `date +%H%M` -gt $cutofftime ]
	then
		cutoff_notify=1
		echo "${currtime}: past cutoff time of $cutofftime, send notification ..." >> ${pollerlog}
		blat ${pollerlog} -t "$contactgroup" -s "Warning: poller reached cutoff time ($cutofftime) for ${database}, continue running..."
	fi

	echo "${currtime}: Sleeping for ${minutesToWait} minutes until next try"  >> ${pollerlog}

	# Sleep until the next try
	sleep ${minutesToWait}m
done

exit 0