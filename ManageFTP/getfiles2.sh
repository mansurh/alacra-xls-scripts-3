XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 2 ]
then
    echo "usage: $0 projname regname"
    exit 1
fi

projname=$1
regname=$2

user=`get_xls_registry_value ${regname} User 6`
pass=`get_xls_registry_value ${regname} Password 6`
ftpserver=`get_xls_registry_value ${regname} Server 6`
sftpkey=`get_xls_registry_value ${regname} Key 6`
ftpfolder=`get_xls_registry_value ${regname} Outbound 6`
clienttype=`get_xls_registry_value ${regname} ClientType 6`

usecurl=0
if [ "$clienttype" = "curl" ]; then usecurl=1; fi;


main()
{
cd $XLSDATA/$projname/download
if [ $? -ne 0 ]
then
	echo "$0: error in cd to $XLSDATA/$projname/download, exiting ..." 
	exit 3
fi

#Go to the root directory
cd $XLSDATA/$projname/

ftpcmdfile=getfiles.ftp
rm -f ${ftpcmdfile}
if [ ${usecurl} -eq 1 ]
then
  curl -s -k -O "sftp://${user}:${pass}@${ftpserver}/${ftpfolder}/${filelist}"
  returncode=$?
else
  if [ "${ftpfolder}" != "" ]
  then
    echo "cd ${ftpfolder}" >> ${ftpcmdfile}
  fi
  echo "get ${filelist}" >> ${ftpcmdfile}
  echo "quit" >> ${ftpcmdfile}
  sftp -b ${ftpcmdfile} -oIdentityFile=$XLS/src/${sftpkey} ${user}@${ftpserver} 
  returncode=$?
fi

if [ $returncode -ne 0 ]
then
  echo "$0: eror in getting ${filelist} from ${ftpserver} " 
  exit 1
fi


if [ ! -s ${filelist} ]
then
  echo "$0: ${filelist} is empty on ${ftpserver} exiting ... " 
  rm -f ${filelist}
  exit 1
fi


cd $XLSDATA/$projname/download

if [ ${usecurl} -eq 1 ]
then
  while read filename
  do
	echo "getting $filename"
	curl -k -O "sftp://${user}:${pass}@${ftpserver}/${ftpfolder}/${filename}" 
	if [ $? -ne 0 ]; then echo "$0: error in getting $filename, exiting ..."; exit 1; fi;
  done < $XLSDATA/$projname/${filelist}

else
  awk -f $XLS/src/scripts/ManageFTP/get.awk -v PASS=$pass -v USER=$user -v DIR="${ftpfolder}" $XLSDATA/$projname/${filelist} | tail -n +4 > $XLSDATA/$projname/getfiles.sftp
  returncode=$?
  if [ $returncode -ne 0 ]
  then
	echo "$0: error in get.awk, exiting ... " 
	exit $returncode
  fi

  sftp -b $XLSDATA/$projname/getfiles.sftp -oIdentityFile=$XLS/src/${sftpkey} ${user}@${ftpserver} 
  if [ $? -ne 0 ]; then echo "$0: error in ftp, exiting ... "; exit 1; fi;
fi
}

logfile=$XLSDATA/$projname/ManageFtp`date +%m%d%H`.log
filelist=${ftpserver}.${projname}.filelist

mkdir -p $XLSDATA/$projname/download
(main) >> ${logfile} 2>&1
rc=$?
exit $rc