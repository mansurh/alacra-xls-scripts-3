rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\alacra2003survey"
registry -s -k "$rkey" -n "mode" -v "advanced"
registry -s -k "$rkey" -n "required_fields" -v "first,last,company,email"
registry -s -k "$rkey" -n "web_form" -v "c:\\usr\\netscape\\server\\docs\\survey\\alacra2003survey.htm"

rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\alacra2003survey\\internalemail"
registry -s -k "$rkey" -n "template_files" -v "c:\\users\\xls\\src\\scripts\\mailto\\alacra2003survey\\internalemail.txt"
registry -s -k "$rkey" -n "command" -v "blat \${_file0} -f \"\${email,c}\" -t \"carolann.thomas@alacra.com,barbara.koscs@alacra.com,colin.dusaire@alacra.com,rob.taylor@alacra.com,gemma.page@alacra.com,bob.delaney@alacra.com,steven.goldstein@alacra.com,michael.angle@alacra.com\" -s \"Alacra 2003 Survey\" -q"

rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\alacra2003survey\\sqlinsert"
registry -s -k "$rkey" -n "template_files" -v "c:\\users\\xls\\src\\scripts\\mailto\\alacra2003survey\\alacra2003survey.sql"
registry -s -k "$rkey" -n "command" -v "isql /S data9 /U mailto /P mailto < \${_file0} > nul"

rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\alacra2003survey\\webresponse"
registry -s -k "$rkey" -n "template_files" -v "c:\\usr\\netscape\\server\\docs\\survey\\alacrasurveythanks.htm"
registry -s -k "$rkey" -n "command" -v "web response"

