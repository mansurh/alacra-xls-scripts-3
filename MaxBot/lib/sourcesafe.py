import subprocess as sproc
import re
import os
from .globals import globals
from .print import cprint
from .colors import colors


class SourceSafe:
    def __init__(self):
        pass

    def label(self, sspath, from_label="", to_label="", label_suffix=""):
        cprint.info("\n  --> Labeling '{0}' from '{1}' to '{2}-{3}'".format(sspath, from_label, to_label, label_suffix))
        if len(from_label) == 0 and len(to_label) == 0:
            raise ValueError(colors.Red + "Labels from_label and to_label are both empty" + colors.END)
        # version = self.get_label_version(sspath, from_label)
        # sproc.call('ss label {file} -L{label} -V{version} -I-Y'.format(file=sspath, label=to_label, version=version), shell=True)
        if len(label_suffix) == 0:
            cmd = "ss label {file} -Vl'{from_label}' -L'{to_label}' -I-Y".format(
                file=sspath, from_label=from_label, to_label=to_label
            )
        elif len(from_label) == 0 :
            cmd = "ss label {file} -Vl'{from_label}' -L'{to_label}-{label_suffix}' -I-Y".format(
                file=sspath, from_label=from_label, to_label=to_label, label_suffix=label_suffix
            )
        else :
           cmd = "ss label {file} -Vl'{from_label}-{label_suffix}' -L'{to_label}-{label_suffix}' -I-Y".format(
                file=sspath, from_label=from_label, to_label=to_label, label_suffix=label_suffix
            ) 

        cprint.declare(cmd)
        # sproc.call(cmd, shell=True)
        lcall = sproc.Popen(cmd, stdout=sproc.PIPE, stderr=sproc.STDOUT, shell=True)
        cprint.tee(lcall, globals.logfile)
        lcall.communicate()
        if lcall.returncode != 0:
            raise sproc.CalledProcessError(lcall.returncode, cmd)
        cprint.print()
    
    def get_file_tmp(self, sspath, label, label_suffix, tmpdir):
        """Gets file from SourceSafe to tmpdir."""
        try: 
            os.makedirs(tmpdir)
        except OSError:
            if not os.path.isdir(tmpdir):
                raise
        
        if len (label_suffix) == 0 or len(label) == 0:
            cmd = "ss get {sspath} -Vl'{label}' -WR -GL. -I-Y".format( sspath=sspath, label=label )
        else :
            cmd = "ss get {sspath} -Vl'{label}-{label_suffix}' -WR -GL. -I-Y".format( sspath=sspath, label=label, label_suffix=label_suffix )

        cprint.declare(cmd, '\n(Working dir = {0})'.format(tmpdir))
        try:
            os.remove(os.path.join(tmpdir, os.path.basename(sspath)))
        except OSError:
            pass
        # val = sproc.call(cmd, shell=True, cwd=tmpdir)
        lcall = sproc.Popen(cmd, cwd=tmpdir, stdout=sproc.PIPE, stderr=sproc.STDOUT, shell=True)
        cprint.tee(lcall, globals.logfile)
        lcall.communicate()
        if lcall.returncode != 0:
            raise sproc.CalledProcessError(lcall.returncode, cmd)
        return os.path.isfile(os.path.join(tmpdir, os.path.basename(sspath)))

            
ss = SourceSafe()
