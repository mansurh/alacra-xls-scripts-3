#!/usr/bin/env python3

import os, sys, socket, datetime
import subprocess as sproc
import xml.etree.ElementTree as etree
import pyodbc
from .configs import creds


def add_document(filename):
    # server = sproc.check_output(["regtool", "get", '/machine/software/xls/Application Servers/LockerServer/Server'])
    # server = server.decode().strip().split(';')[0]
    connection_string = 'DRIVER={{SQL Server}};SERVER={s};DATABASE={db};UID={u};PWD={p}'.format(
        s=creds['dba_server'], db='dba2', u=creds['dba_user'], p=creds['dba_pass']
    )
    conn = pyodbc.connect(connection_string)
    c = conn.cursor()
    c.execute("exec whereisresource 'lockerserver'")
    myall = c.fetchall()
    servers = [row.server for row in myall
               if row.status == 'online' and row.serverstatus == 'online' 
               and row.resourceuse == 'production' and row.group == 1]
    server = servers[0]

    port = sproc.check_output(["regtool", "get", '/machine/software/xls/Application Servers/LockerServer/Port'])
    port = int(port.decode().strip().split(';')[0])

    f = open(filename, 'rb').read()
    flen = os.path.getsize(filename)

    tnow = datetime.datetime.now().strftime('%d %b %y %H:%M:%S:%f')[:-3]

    data = {
        'lockerfilenamein': filename,
        'lockerfilesize': flen,
        'machinename': socket.gethostbyname(socket.gethostname()),
        'command': 'adddocument',
        'dateadded': tnow,
        'userid': 220781,
        'lwid': 220781,
        'lockerproduct': 'maxbot',
        'lockerdocname': 'maxbot file',
        'contenttype': 'application/zip',
        # 'zipcontent': 'y',
        # 'contentziplevel': '6'
    }
    
    params = ''
    for k,v in data.items():
        kv = k + '=' + str(v) + '&'
        params += kv

    plen = len(params)

    part1 = """POST /cgi-bin/LockerService.exe HTTP/1.0
Content-Type: application/x-www-form-urlencoded
Content-Length: {clen}
Header-Length: {plen}
Metadata-Length: 0

{params}""".format(plen=plen, params=params, clen=flen + plen)

    part1 = part1.encode()
    print('Locker headers + query:', part1)

    print('Opening socket to {0}:{1}'.format(server, port))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((server, port))
    # s.connect(("data14", 7001))

    print('Sending headers + query')
    totalsent = 0
    lenpart1 = len(part1)
    while totalsent < lenpart1:
        sent = s.send(part1[totalsent:])
        if sent == 0:
            raise RuntimeError("socket connection broken")
        totalsent = totalsent + sent
        print('Sent {0} of {1}'.format(totalsent, lenpart1))

    print('Sending file')
    s.sendall(f)

    res = b''
    print('Waiting for response')
    while True:
        rec = s.recv(1024)
        res += rec
        if not rec: break

    print('Closing connection')
    s.close()

    res = res.decode('utf-8')
    print('Response:', res)

    print('Parsing response')
    idx = res.find('<DOCUMENT')
    xmlstr = res[idx:]
    xml = etree.fromstring(xmlstr)
    lockerfileid_node= xml.find('{ns}QUERY/{ns}CONSTRAINTS/{ns}lockerfileid/{ns}VALUE'.format(ns='{http://www.alacra.com/}'))
    if lockerfileid_node is not None:
        lockerfileid = lockerfileid_node.text
        print('Lockerfileid:', lockerfileid)
        return lockerfileid
    else:
        print('Error: Didn\'t find lockerfileid in response')
        raise Exception("Error adding document to locker")


if __name__ == '__main__':
    filename = sys.argv[1]
    add_document(filename)
