from .colors import colors
from .globals import globals

# import pprint
import sys


class CPrint():
    def print(self, *args, color=None, **kwargs):
        if color:
            # a = (color,) + args + (colors.END,)
            # print('color type:', type(color))
            # a = color + args + colors.END
            print(color, end='')
            print(*args, **kwargs)
            print(colors.END, end='')
        else:
            print(*args, **kwargs)

        if hasattr(globals, 'logfile') and globals.logfile:
            print(*args, file=globals.logfile, **kwargs)

    def green(self, *args, **kwargs):
        self.print(*args, color=colors.GREEN, **kwargs)

    def warning(self, *args, **kwargs):
        self.print(*args, color=colors.YELLOW, **kwargs)

    def error(self, *args, **kwargs):
        self.print(*args, color=colors.RED, **kwargs)

    def special(self, *args, **kwargs):
        self.print(*args, color=colors.BLUEBG, **kwargs)

    def info(self, *args, **kwargs):
        self.print(*args, color=colors.WHITE, **kwargs)

    def declare(self, *args, **kwargs):
        self.print(*args, color=colors.PURPLE, **kwargs)

    def tee(self, proc, logfile):
        """Takes subprocess proc whose stdout was to proc.PIPE and tee the output to
stdout and the input logfile"""
        while True:
            ch = proc.stdout.read(1)
            if not ch:
                sys.stdout.flush()
                break
            else:
                sys.stdout.write(ch.decode())
                sys.stdout.flush()
                logfile.write(ch.decode())

cprint = CPrint()
