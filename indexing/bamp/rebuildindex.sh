TITLEBAR="BAMP Index Update"

dataserver=`hostname`

save_backup()
{
  mkdir -p backup
  cp -p bamp* backup/
}


# where is the index located?
currentloc=`reginvidx get bamp`

scratch=${currentloc%/*}
indexdir=${scratch}
cd ${indexdir}
if [ $? -ne 0 ]
then
	echo "Index directory not found, exiting"
	exit 1
fi

# Mark the database as updating
$XLS/src/scripts/loading/dba/startupdate.sh bamp ${dataserver} bamp
if [ $? -ne 0 ]
then
	echo "error in startupdate.sh bamp ${dataserver} bamp"
	exit 1
fi

rm -r -f save
mkdir -p save
mv bamp* save/
cp -p $XLS/src/cfgfiles/bamp.cfg .

# Now rebuild the index - part 1
#newrdsindex.exe -d bamp -t 10000000 -m 200000000 -S "December 31, 1994"
if [ $? -ne 0 ]
then
	echo "Error updating news indices, Exiting"
	exit 1
fi

# Continue to rebuild the rest
for yeartoindex in `isql /S${dataserver} /Ubamp /Pbamp -h-1 -n /Q "set nocount on select distinct datepart(year, publication_date) from story where publication_date >= '2001-01-01' and publication_date <= getdate() order by datepart(year, publication_date)"`
do
#  save_backup
  echo "newrdsindex.exe -d bamp -t 10000000 -m 200000000 -s January 1, ${yeartoindex} -S December 31, ${yeartoindex}"
  newrdsindex.exe -d bamp -t 10000000 -m 200000000 -s "January 1, ${yeartoindex}" -S "December 31, ${yeartoindex}"
  if [ $? -ne 0 ]
  then
	echo "Error updating news indices for first half of $yeartoindex , Exiting"
	exit 1
  fi
done

rm -r -f backup
# Mark the database update as complete
$XLS/src/scripts/loading/dba/endupdate.sh bamp ${dataserver} bamp
